<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCopyFiledsToInsertionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insertion_orders', function (Blueprint $table) {
            $table->string('copy2', 20)->nullable()->after('copy');
            $table->string('copy3', 20)->nullable()->after('copy2');
            $table->string('copy4', 20)->nullable()->after('copy3');
            $table->string('copy5', 20)->nullable()->after('copy4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insertion_orders', function (Blueprint $table) {
            $table->dropColumn('copy2');
            $table->dropColumn('copy3');
            $table->dropColumn('copy4');
            $table->dropColumn('copy5');

        });
    }
}
