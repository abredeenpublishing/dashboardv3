<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClassifiedsAndFlyerFieldsToInsertionOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insertion_orders', function (Blueprint $table) {
            $table->string('classified_category', 40)->nullable()->after('web_ad');
            $table->longText('classified_word_ad')->nullable()->after('classified_category');
            $table->string('classified_number_of_weeks', 20)->nullable()->after('classified_word_ad');
            $table->date('classified_start_date')->nullable()->after('classified_number_of_weeks');
            $table->date('classified_end_date')->nullable()->after('classified_start_date');
            $table->string('classified_word_count', 40)->nullable()->after('classified_end_date');
            $table->float('classified_extra_word_cost')->nullable()->after('classified_word_count');

            $table->float('flyer_dollars_per_thousand')->nullable()->after('classified_extra_word_cost');
            $table->double('flyer_quanity')->nullable()->after('flyer_dollars_per_thousand');
            $table->string('flyer_discount', 40)->nullable()->after('flyer_quanity');
            $table->float('flyer_discount_amount')->nullable()->after('flyer_discount');
            $table->string('flyer_identification', 40)->nullable()->after('flyer_discount_amount');
            $table->longText('flyer_distribution')->nullable()->after('flyer_identification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insertion_orders', function (Blueprint $table) {
            
            $table->dropColumn('classified_category');
            $table->dropColumn('classified_word_ad');
            $table->dropColumn('classified_number_of_weeks');
            $table->dropColumn('classified_start_date');
            $table->dropColumn('classified_end_date');
            $table->dropColumn('classified_word_count');
            $table->dropColumn('classified_extra_word_cost');

            $table->dropColumn('flyer_dollars_per_thousand');
            $table->dropColumn('flyer_quanity');
            $table->dropColumn('flyer_discount');
            $table->dropColumn('flyer_discount_amount');
            $table->dropColumn('flyer_identification');
            $table->dropColumn('flyer_distribution');
        });
    }
}
