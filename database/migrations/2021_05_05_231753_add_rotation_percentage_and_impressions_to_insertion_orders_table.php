<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRotationPercentageAndImpressionsToInsertionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insertion_orders', function (Blueprint $table) {
            $table->string('rotation_percentage', 50)->nullable()->after('web_ad_space');
            $table->string('impressions', 50)->nullable()->after('rotation_percentage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insertion_orders', function (Blueprint $table) {
            //
            $table->dropColumn('rotation_percentage');
            $table->dropColumn('impressions');
        });
    }
}
