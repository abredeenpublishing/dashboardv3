<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClassifiedDefaultFieldsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('classified_word_count', 20)->nullable()->after('remember_token');
            $table->float('classified_extra_word_cost')->nullable()->after('classified_word_count');
            $table->string('classified_base', 20)->nullable()->after('classified_extra_word_cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('classified_word_count');
            $table->dropColumn('classified_extra_word_cost');
            $table->dropColumn('classified_base');
        });
    }
}
    