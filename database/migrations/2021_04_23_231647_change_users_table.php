<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('office_admin_email', 50)->nullable()->after('email');
            $table->string('production_email', 50)->nullable()->after('email');
            $table->string('publication', 50)->nullable()->after('email');
            $table->string('rep_number', 10)->nullable()->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('office_admin_email');
            $table->dropColumn('production_email');
            $table->dropColumn('publication');
            $table->dropColumn('rep_number');
        });
    }
}
