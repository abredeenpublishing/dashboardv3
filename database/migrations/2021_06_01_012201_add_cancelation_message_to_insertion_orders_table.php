<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCancelationMessageToInsertionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insertion_orders', function (Blueprint $table) {
            //
            $table->longtext('cancel_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insertion_orders', function (Blueprint $table) {
            //
            $table->dropColumn('cancel_message');
        });
    }
}
