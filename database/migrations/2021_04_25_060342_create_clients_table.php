<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string("dti_id")->nullable();
            $table->string('client_name')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string("postal_code")->nullable();
            $table->string("billing_contact_name")->nullable();
            $table->string('billing_contact_phone')->nullable();
            $table->string('billing_contact_email')->nullable();
            $table->string('client_contact_name')->nullable();
            $table->string('client_contact_phone')->nullable();
            $table->string('client_contact_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
