<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsertionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insertion_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('user_id')->unsigned();
            $table->Integer('client_id')->unsigned();
            $table->string('order_number', 20)->nullable();
            $table->integer('order_type')->nullable();
            $table->date('insertion_date')->nullable();
            $table->string('po_number', 50)->nullable();
            $table->string('modular_size', 50)->nullable();
            $table->float('columns_wide')->nullable();
            $table->float('inches_high')->nullable();
            $table->float('column_rate')->nullable();
            $table->float('colour_charge')->nullable();
            $table->float('subtotal_overide')->nullable();
            $table->float('subtotal')->nullable();
            $table->float('gst')->nullable();
            $table->float('total')->nullable();
            $table->string('payment_method', 50)->nullable();
            $table->string('position_requested', 50)->nullable();
            $table->string('feature', 50)->nullable();
            $table->string('copy', 25)->nullable();
            $table->string('caption_line', 50)->nullable();
            $table->string('ad_type', 50)->nullable();
            $table->boolean('tfn')->default(false);
            $table->string('print_frequency_0', 50)->nullable();
            $table->date('print_start_date_0')->nullable();
            $table->date('print_end_date_0')->nullable();
            $table->string('print_publication_0', 50)->nullable();
            $table->string('print_supplement_0', 50)->nullable();
            $table->integer('print_no_insertions_0')->nullable();
            $table->string('print_frequency_1', 50)->nullable();
            $table->date('print_start_date_1')->nullable();
            $table->date('print_end_date_1')->nullable();
            $table->string('print_publication_1', 50)->nullable();
            $table->string('print_supplement_1', 50)->nullable();
            $table->integer('print_no_insertions_1')->nullable();
            $table->string('print_frequency_2', 50)->nullable();
            $table->date('print_start_date_2')->nullable();
            $table->date('print_end_date_2')->nullable();
            $table->string('print_publication_2', 50)->nullable();
            $table->string('print_supplement_2', 50)->nullable();
            $table->integer('print_no_insertions_2')->nullable();
            $table->string('print_frequency_3', 50)->nullable();
            $table->date('print_start_date_3')->nullable();
            $table->date('print_end_date_3')->nullable();
            $table->string('print_publication_3', 50)->nullable();
            $table->string('print_supplement_3', 50)->nullable();
            $table->integer('print_no_insertions_3')->nullable();
            $table->string('print_frequency_4', 50)->nullable();
            $table->date('print_start_date_4')->nullable();
            $table->date('print_end_date_4')->nullable();
            $table->string('print_publication_4', 50)->nullable();
            $table->string('print_supplement_4', 50)->nullable();
            $table->integer('print_no_insertions_4')->nullable();
            $table->string('print_frequency_5', 50)->nullable();
            $table->date('print_start_date_5')->nullable();
            $table->date('print_end_date_5')->nullable();
            $table->string('print_publication_5', 50)->nullable();
            $table->string('print_supplement_5', 50)->nullable();
            $table->integer('print_no_insertions_5')->nullable();
            $table->string('print_frequency_6', 50)->nullable();
            $table->date('print_start_date_6')->nullable();
            $table->date('print_end_date_6')->nullable();
            $table->string('print_publication_6', 50)->nullable();
            $table->string('print_supplement_6', 50)->nullable();
            $table->integer('print_no_insertions_6')->nullable();
            $table->string('print_frequency_7', 50)->nullable();
            $table->date('print_start_date_7')->nullable();
            $table->date('print_end_date_7')->nullable();
            $table->string('print_publication_7', 50)->nullable();
            $table->string('print_supplement_7', 50)->nullable();
            $table->integer('print_no_insertions_7')->nullable();
            $table->string('print_frequency_8', 50)->nullable();
            $table->date('print_start_date_8')->nullable();
            $table->date('print_end_date_8')->nullable();
            $table->string('print_publication_8', 50)->nullable();
            $table->string('print_supplement_8', 50)->nullable();
            $table->integer('print_no_insertions_8')->nullable();
            $table->string('print_frequency_9', 50)->nullable();
            $table->date('print_start_date_9')->nullable();
            $table->date('print_end_date_9')->nullable();
            $table->string('print_publication_9', 50)->nullable();
            $table->string('print_supplement_9', 50)->nullable();
            $table->integer('print_no_insertions_9')->nullable();
            $table->string('print_frequency_10', 50)->nullable();
            $table->date('print_start_date_10')->nullable();
            $table->date('print_end_date_10')->nullable();
            $table->string('print_publication_10', 50)->nullable();
            $table->string('print_supplement_10', 50)->nullable();
            $table->integer('print_no_insertions_10')->nullable();
            $table->string('print_frequency_11', 50)->nullable();
            $table->date('print_start_date_11')->nullable();
            $table->date('print_end_date_11')->nullable();
            $table->string('print_publication_11', 50)->nullable();
            $table->string('print_supplement_11', 50)->nullable();
            $table->integer('print_no_insertions_11')->nullable();
            $table->string('print_frequency_12', 50)->nullable();
            $table->date('print_start_date_12')->nullable();
            $table->date('print_end_date_12')->nullable();
            $table->string('print_publication_12', 50)->nullable();
            $table->string('print_supplement_12', 50)->nullable();
            $table->integer('print_no_insertions_12')->nullable();
            $table->date('web_start_date')->nullable();
            $table->date('web_end_date')->nullabe();
            $table->string('web_url', 100)->nullable();
            $table->string('web_online_asset', 50)->nullable();
            $table->string('web_ad_space', 50)->nullable();
            $table->string('web_ad', 500)->nullable();
            $table->longtext('notes')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insertion_orders');
    }
}
