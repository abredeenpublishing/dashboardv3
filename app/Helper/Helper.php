<?php

namespace App\Helper;

use DateTime;
use App\Models\User;

class Helper
{

    public static function getioType($type)
    {
        if ($type == "1") {
            $io = "Print";
        } elseif ($type == "2") {
            $io = "Web";
        } elseif ($type == "3") {
            $io = "Flyer";
        } elseif ($type == "4") {
            $io = "Classified";
        } else {
            $io = "undefined";
        }

        return $io;

    }

    public static function cleanDate($date)
    {

        $clean_date = date('M d, Y', strtotime($date));
        return $clean_date;

    }

    public static function validateDate($date, $format = 'Y-m-d'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    public static function getFrequency($num)
    {
        $freq = "Not Found";
        if ($num == "1") {
            $freq = "Once a Week";
        } elseif ($num == "2") {
            $freq = "Every Other Week";
        } elseif ($num == "3") {
            $freq = "Every 3rd Week of the Month";
        } elseif ($num == "4") {
            $freq = "Once a Month";
        }

        return $freq;
    }

    public static function createIOEmailVars($insertionorder, $email_input, $what_changed, $subject)
    {
        // if the order has been entered onbehalf of anohter rep the other reps details needs to be in the Insertion Order. 
        if (!empty($email_input['sales_id'])) {
            $sales_rep = User::find($email_input['sales_id']);
            $sales_rep_name = $sales_rep->name;
            $sales_rep_number = $sales_rep->rep_number;

        } else {
            $sales_rep_name = $insertionorder->user->name;
            $sales_rep_number = $insertionorder->user->rep_number;
        }
        $email_details = [
            'email_type' => 'update',
            'subject' => $subject,
            'client_name' => $insertionorder->client->client_name,
            'address' => $insertionorder->client->address,
            'city' => $insertionorder->client->city,
            'province' => $insertionorder->client->province,
            'postal_code' => $insertionorder->client->postal_code,
            'billing_contact_name' => $insertionorder->client->billing_contact_name,
            'billing_contact_email' => $insertionorder->client->billing_contact_email,
            'billing_contact_phone' => $insertionorder->client->billing_contact_phone,
            'dti_id' => $insertionorder->client->dti_id,
            'sales_rep' => $sales_rep_name,
            'rep_number' => $sales_rep_number,
            'sales_email' => $insertionorder->user->email,
            'publication' => $insertionorder->user->publication,
            'insertion_date' => $insertionorder->insertion_date,
            'web_ad' => $insertionorder->web_ad,
            'order_number' => $insertionorder->order_number,
            'insertion_order' => $email_input,
            'what_changed' => $what_changed
        ];
        
        return $email_details;

    }

    public static function createClientEmailVars($client, $email_input, $what_changed, $subject)
    {
        $user_id = auth()->id();
        $user = User::find($user_id);
        $email_details = [
            'email_type' => 'update',
            'subject' => $subject,
            'client_data' => $email_input, 
            'what_changed' => $what_changed,
            'old_data' => $client,
            'sales_email' =>$user->email,
            'sales_rep' =>$user->name 
        ];

        return $email_details;

    }

    public static function getSalesRep($sales_id)
    {

            $sales_rep = User::find($sales_id);
            $sales_rep_name = $sales_rep->name;
            $sales_rep_number = $sales_rep->rep_number;

            $html = "<p> Sales Rep: " . $sales_rep_name . "</p>" . "<p>Rep Number: ". $sales_rep_number . "</p> ";

            return $html;
    }

    public static function getSalesRepNumber($sales_id)
    {

            $sales_rep = User::find($sales_id);
            
        

            return $sales_rep->rep_number;
    }

    

}