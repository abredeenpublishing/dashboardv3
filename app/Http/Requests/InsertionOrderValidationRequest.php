<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsertionOrderValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->request->all();
      //  dd($request);
        if ($request['order_type'] == "Print") {
            $rules = [
                'user_id' => ['required'],
                'client_id' => ['required'],
                'order_type' => ['required'],
                'po_number' => ['nullable'],
                'colour_charge' => ['nullable', 'numeric', 'between:0,99.99'],
                'subtotal_overide' => ['nullable'],
                'payment_method' => ['nullable'],
                'color' => ['required'],
                'postion_requested' => ['nullable'],
                'feature' => ['nullable'],
                'copy' => ['nullable'],
                'caption_line' => ['required'],
                'ad_type' => ['required'],
                'print_frequency_0' => ['required'],
                'print_start_date_0' => ['required', 'date'],
                'print_end_date_0' => ['required', 'date'],
                'print_publication_0' => ['required'],
                'print_no_insertions_0' => ['required'],
                'modular_size' => ['nullable'],
                'columns_wide' => ['required', 'numeric', 'between:0,99.99'],
                'inches_high' => ['required', 'numeric', 'between:0,99.99'],
                'column_rate' => ['required', 'numeric', 'between:0,99.99'],
                'subtotal_overide' => ['nullable', 'numeric', 'between:0,9999999.99'],
                'print_discount' => ['nullable', 'numeric']
            ];
            // construct all other fields to array $rules
            $i =1;
            while ($i <= 12){
                $start_date = "print_start_date_" . $i;
                $end_date = "print_end_date_" . $i;
                $number_of_insertions = "print_no_insertions_" . $i;
                if ($request[$start_date] != "") {
                    $rules[$start_date] = ['required', 'date'];
                    $rules[$end_date] = ['required', 'date'];
                    $rules[$number_of_insertions] = ['required', 'integer'];
                }
                $i++;
            }    

            if (array_key_exists('modular_size_yes_or_no', $request) == true)
            {
                // if modular sizes is checked column inches become irrelavent 
                $rules['modular_size'] = ['required'];
                $rules['columns_wide'] = ['nullable', 'numeric', 'between:0,99.99'];
                $rules['inches_high'] = ['nullable', 'numeric', 'between:0,99.99'];
                $rules['column_rate'] = ['nullable', 'numeric', 'between:0,99.99'];
                $rules['subtotal_overide'] = ['required', 'numeric', 'between:0,9999999.99'];
                
                
            } elseif (array_key_exists('override_subtotal_yes_or_no', $request) == true) {
                
                $rules['columns_wide'] = ['nullable', 'numeric', 'between:0,99.99'];
                $rules['inches_high'] = ['nullable', 'numeric', 'between:0,99.99'];
                $rules['column_rate'] = ['nullable', 'numeric', 'between:0,99.99'];
                $rules['subtotal_overide'] = ['required', 'numeric', 'between:0,9999999.99'];
                
            }
        } else if ($request['order_type'] == "Web") {

            $rules = [
                'user_id' => ['required'],
                'client_id' => ['required'],
                'order_type' => ['required'],
                'po_number' => ['nullable'],
                'web_url' => ['required', 'url'],
                'web_start_date' => ['required', 'date'],
                'web_end_date' => ['required', 'date'], 
                'web_online_asset' => ['required'],
                'web_ad_space' => ['required'],
                'rotation_percentage' => ['required'],
                'web_ad' => ['nullable'],
                'notes' => ['nullable'],
                'subtotal' => ['required'],
                'gst' => ['required'],
                'total' => ['required'],
                'web_ad' => ['nullable'],
                'web_ad.*' => ['mimes:jpeg,jpg,png,gif', 'max:1024']
            ];

            if ($request['rotation_percentage'] == "Impressions") {
                $rules['impressions'] = ['required'];
            } else {
                $rules['impressions'] = ['nullable'];
            }
        }  else if ($request['order_type'] == "Flyer") {
            $rules = [
                'user_id' => ['required'],
                'client_id' => ['required'],
                'order_type' => ['required'],
                'po_number' => ['nullable'],
                'payment_method' => ['nullable'],
                'print_start_date_0' => ['required', 'date'],
                'print_end_date_0' => ['required', 'date'],
                'print_publication_0' => ['required'],
                'print_no_insertions_0' => ['required'],
                'subtotal_overide' => ['nullable', 'numeric', 'between:0,9999999.99'],
                'flyer_dollars_per_thousand' => ['required', 'numeric', 'between:0,9999999.99'],
                'flyer_quanity' => ['required', 'numeric'],
                'flyer_discount' => ['nullable'],
                'flyer_identification' => ['required'],
                'flyer_distribution' => ['nullable']

            ];
        }  else if ($request['order_type'] == "Classified") {
            $rules = [
                'user_id' => ['required'],
                'client_id' => ['required'],
                'order_type' => ['required'],
                'po_number' => ['nullable'],
                'payment_method' => ['nullable'],
                'subtotal_overide' => ['nullable', 'numeric', 'between:0,9999999.99'],
                'classified_word_ad' => ['required'],
                'classified_category' => ['required'],
                'classified_number_of_weeks' => ['required'],
                'classified_start_date' => ['required', 'date'],
                'classified_end_date' => ['required', 'date']
            ];
        }

       // dd($rules);
        return $rules;
    }
}
