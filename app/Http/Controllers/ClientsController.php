<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\InsertionOrder;
use App\Models\User;
use DataTables;
use App\Helper\Helper;
use Illuminate\Support\Facades\Gate;
use App\Mail\ClientUpdateMail;
use Illuminate\Support\Facades\Mail;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$clients = Client::orderby('client_name', 'asc')->get();
       // $clients = Client::with(['id','dti_id','client_name', 'city'])->chunk(100);
        return view('clients.index');
    }
   
    public function getdata(Request $request, Client $ser)
    {
            $input = \Arr::except($request->all(),array('_token', '_method'));
            $data = Client::orderby('client_name')->get();
            
            if(isset($input['client_name'])) {
                $data = Client::where('client_name', 'like', '%'.$input['client_name'].'%')->orderby('client_name')->get();
            }
            if(isset($input['dti_id'])) {
               
                $data = Client::where('dti_id', $input['dti_id'])->orderby('client_name')->get();
            }
         
            return \DataTables::of($data)
                ->addColumn('action', function($data) {
                    $btn = "";
                    if (Gate::allows('sales-only', auth()->user())) {
                        $btn = '<a href="/insertionorders/printio/'. $data->id.'" class="edit btn btn-info btn-sm mx-1" title="Create Print IO"><i class="fas fa-plus-circle"></i> <strong>P</strong></a>';
                        $btn = $btn. '<a href="/insertionorders/webio/'. $data->id.'" class="edit btn btn-info btn-sm mx-1" title="Create Web IO"><i class="fas fa-plus-circle"></i> <strong>W</strong></a>';
                        $btn = $btn. '<a href="/insertionorders/flyerio/'. $data->id.'" class="edit btn btn-info btn-sm mx-1" title="Create Flyer IO"><i class="fas fa-plus-circle"></i> <strong>F</strong></a>';
                        $btn = $btn. '<a href="/insertionorders/classifiedio/'. $data->id.'" class="edit btn btn-info btn-sm mx-1" title="Create Classified IO"><i class="fas fa-plus-circle"></i> <strong>C</strong></a>';
                    }
                    $btn = $btn.'<a href="/clients/' . $data->id .'/edit" class="edit btn btn-primary btn-sm mx-1"><i class="fas fa-edit"></i> Edit</a>';
                    
                    
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'dti_id' => 'nullable',
            'client_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'province' => 'required',
            'postal_code' => 'required',
            'billing_contact_name' => 'required',
            'billing_contact_email' => 'required|email',
            'billing_contact_phone' => 'required',
            'client_contact_name' => 'nullable',
            'client_contact_phone' => 'nullable',
            'client_contact_email' => 'nullable'
        ]);

        $client = new Client;
        $client->dti_id = $request->input('dti_id');
        $client->client_name = $request->input('client_name');
        $client->address = $request->input('address');
        $client->city = $request->input('city');
        $client->province = $request->input('province');
        $client->postal_code = $request->input('postal_code');
        $client->billing_contact_name = $request->input('billing_contact_name');
        $client->billing_contact_email = $request->input('billing_contact_email');
        $client->billing_contact_phone = $request->input('billing_contact_phone');
        $client->client_contact_name = $request->input('client_contact_name');
        $client->client_contact_email = $request->input('client_contact_email');
        $client->client_contact_phone = $request->input('client_contact_phone');
        $client->save();

        return redirect('/clients')->with('success', 'Client has been added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $client = Client::find($id);
        //$insertionorders = InsertionOrder::where('client_id', $id);
        return view('clients.show')->with('client', $client);
        //dd('show client stuffs');
    }

    public function showinsertions(Request $request, $id)
    {
         
        $user_id = auth()->id();
       
        if (Gate::allows('front-only', auth()->user())) {
            $user = User::find($user_id);
            $email = $user->email;
            $q = InsertionOrder::select([
                'client_id',
                'insertion_orders.id',
                'order_number',
                'order_type',
                'caption_line',
                'total',
                'print_start_date_0',
                'print_end_date_0',
                'web_start_date',
                'web_end_date',
                'classified_start_date',
                'classified_end_date',
                'cancel',
                'users.name',
            ])  ->join('users','users.id','=','insertion_orders.user_id')
                ->where('users.office_admin_email', $email)
                ->where('client_id', $id); 
                
        } elseif(Gate::allows('admin-only', auth()->user())){ 
            $q = InsertionOrder::select([
                'client_id',
                'id',
                'user_id',
                'order_number',
                'order_type',
                'total',
                'print_start_date_0',
                'print_end_date_0',
                'web_start_date',
                'web_end_date',
                'classified_start_date',
                'classified_end_date',
                'cancel',
            ])->where('client_id', $id); 
        } else {
            $q = InsertionOrder::select([
                'client_id',
                'id',
                'user_id',
                'order_number',
                'order_type',
                'total',
                'print_start_date_0',
                'print_end_date_0',
                'web_start_date',
                'web_end_date',
                'classified_start_date',
                'classified_end_date',
                'cancel',
            ])->where('user_id', '!=', '1')->where('client_id', $id);  // show all orders except Admin's 
        }
       
       // dd($q);

        if ($dti_id = $request->input('dti_id')) {
            $q->wherehas('client', function($query)Use($dti_id){ $query->where('dti_id', $dti_id); });
        } elseif ($client_name = $request->input('client_name')) {
            $q->wherehas('client', function($query)Use($client_name){ $query->where('client_name','like', '%'.$client_name.'%'); });
        }
        
        $insertion_orders = $q->with('client:id,client_name')->get()->map(function($order){ 
            // this maps client_name into data-set
            $order->client_name = $order->client->client_name;
            return $order;

         });

        return \DataTables::of($insertion_orders)
            ->addColumn('action', function($data) {
                if (Gate::allows('front-only', auth()->user())) {
                    $btn = '<a href="'. route('insertionorders.editordernumber', $data->id).'"><button class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> Update Order #</button></a>';
                } else {
                    if (($data->order_number != null) && ($data->cancel != true)) {
                        $user_id = auth()->id();
                        if ($user_id != $data->user_id) { // if the user does not match don't allow them to edit.
                            $btn = '<button class="edit btn btn-primary btn-sm mx-1" disabled><i class="fas fa-edit"></i> Edit</button>';
                        } else {
                            $btn = '<a href="'.route('insertionorders.edit', $data->id).'" class="edit btn btn-primary btn-sm mx-1"><i class="fas fa-edit"></i> Edit</a>';
                        }
                    } else {
                        $btn = '<button class="edit btn btn-primary btn-sm mx-1" disabled><i class="fas fa-edit"></i> Edit</button>';
                    }
                    $btn = $btn.'<a href="/insertionorders/'. $data->id.'/edit?clone=y" class="edit btn btn-warning btn-sm mx-1" ><i class="far fa-copy"></i> Clone IO</a>';  
                }
                return $btn;
            })
            ->addColumn('io_type', function($data){
                if ($data->order_type == "1") {
                    $order_type = "Print";
                } else if ($data->order_type == "2") {
                    $order_type = "Web";
                } else if ($data->order_type == "3") {
                    $order_type = "Flyer";
                } else if ($data->order_type == "4") {
                    $order_type = "Classified";
                } else {
                    $order_type = "Not Found: " . $data->order_type;
                }
                return $order_type;
            })
            ->addColumn('start_date', function($data){
                if ($data->print_start_date_0 == "") {
                    if ($data->web_start_date == "") { // if there is no webdate then io is a classified
                        return Helper::cleanDate($data->classified_start_date); 
                    } else {
                        return Helper::cleanDate($data->web_start_date);
                    }
                } else {
                    return Helper::cleanDate($data->print_start_date_0);
                }
            })
            ->addColumn('end_date', function($data){
                if ($data->print_end_date_0 == "") {
                    if ($data->web_end_date == "") { // if there is no webdate then io is a calssified
                        return Helper::cleanDate($data->classified_end_date);
                    } else {
                        return Helper::cleanDate($data->web_end_date);
                    }
                } else {
                    return Helper::cleanDate($data->print_end_date_0);
                }
            })
            ->rawColumns(['action', 'io_type', 'start_date', 'end_date'])
            ->make(true);    


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::find($id);
        return view('clients.edit')->with('client', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'dti_id' => 'required',
            'client_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'province' => 'required',
            'postal_code' => 'required',
            'billing_contact_name' => 'required',
            'billing_contact_email' => 'required|email',
            'billing_contact_phone' => 'required',
            'client_contact_name' => 'nullable',
            'client_contact_phone' => 'nullable',
            'client_contact_email' => 'nullable'
        ]);

        $old_client_info = Client::find($id);
        $email_input = $request->except('_token', '_method'); 
        $what_changed = $this->track_client_changes($email_input, $old_client_info);
       

        $client = Client::where('id', $id)
            ->update([
                'dti_id' => $request->input('dti_id'),
                'client_name' => $request->input('client_name'),
                'address' => $request->input('address'),
                'city' => $request->input('city'),
                'province' => $request->input('province'),
                'postal_code' => $request->input('postal_code'),
                'billing_contact_name' => $request->input('billing_contact_name'),
                'billing_contact_email' => $request->input('billing_contact_email'),
                'billing_contact_phone' => $request->input('billing_contact_phone'),
                'client_contact_name' => $request->input('client_contact_name'),
                'client_contact_email' => $request->input('client_contact_email'),
                'client_contact_phone' => $request->input('client_contact_phone')

            ]);

        $user_id = auth()->id();
        $user = User::find($user_id);

        $subject = "Update to Client: " . $request->input('client_name') . " DTI ID: " . $request->input('dti_id');

        $email_details = Helper::createClientEmailVars($client, $email_input, $what_changed, $subject);

        Mail::to($user->office_admin_email)->queue(new ClientUpdateMail($email_details));

        return redirect(route('clients.show', $id))->with('success', 'The client has been udpated successfully');
    }

    public function track_client_changes($new_data, $old_data)
    {
        // new_date is an array
        // old_data is an object of insertionorder 
        $changes = [];

        foreach($new_data as $key => $value)
        {
            if ($value != $old_data->$key) {
                //insert div to mark changed area                
                $changes[$key] = "<div class='this-changed'>" . $value . "</div>";     
            }
        }
        return $changes;

        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
