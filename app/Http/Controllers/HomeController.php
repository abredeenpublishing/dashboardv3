<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\InsertionOrder;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = auth()->id();

        $user = User::find($user_id);
        $email = $user->email;

        $q = InsertionOrder::select([
            'client_id',
            'insertion_orders.id',
            'order_number',
            'order_type',
            'caption_line',
            'total',
            'print_start_date_0',
            'print_end_date_0',
            'web_start_date',
            'web_end_date',
            'users.name',
        ])  ->join('users','users.id','=','insertion_orders.user_id')
            ->where('users.office_admin_email', $email)
            ->where('order_number', null);
//->get();

            $insertionorders = $q->with('client:id,client_name,dti_id')->get()->map(function($order){ 
                // this maps client_name into data-set
                $order->client_name = $order->client->client_name;
                $order->dti_id = $order->client->dti_id;
                return $order;
    
             });    
         
        return view('home')->with('insertionorders', $insertionorders);

    }

    
}
