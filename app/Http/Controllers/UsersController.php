<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
//use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Eloquent\Builder;
//use vendor\datatables;

class UsersController extends Controller
{
    public function index(){

        if (Gate::allows('admin-only', auth()->user())) {

            $users = User::all();
            //dd($users);
            return view('users.index', compact('users'));
        } else {

            abort(403);

        }

    }

    public function create() {

        return view("users.create");

    }

    public function store (Request $request) {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'office_admin_email' => 'nullable|email',
            'production_email' => 'nullable|email',
            'publication' => 'required',
            'rep_number' => 'nullable'
        ]);  
      
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->office_admin_email = $request->input('office_admin_email');
        $user->production_email = $request->input('production_email');
        $user->publication = $request->input('publication');
        $user->rep_number = $request->input('rep_number');
        $user->classified_word_count = $request->input('classified_word_count');
        $user->classified_extra_word_cost = $request->input('classified_extra_word_cost');
        $user->classified_base = $request->input('classified_base');
        $user->isAdmin = $request->input('isAdmin');
        $user->isFront = $request->input('isFront');
        $user->save();

        return redirect('/users')->with('success', 'User has been added successfully');

    }

    public function show($id)
    {
        //
    }

    public function edit ($id) {
        
        $user = User::find($id);
        //dd($user);
        return view('users.edit')->with('user', $user);

    }

    public function update (Request $request, $id) {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|',
            'office_admin_email' => 'required|email',
            'production_email' => 'required|email',
            'publication' => 'required',
            'rep_number' => 'nullable'
        ]);  

        

        if ($request->input('password') != "") {
            $user = User::where('id', $id)
            ->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'office_admin_email' => $request->input('office_admin_email'),
                'production_email' => $request->input('production_email'),
                'publication' => $request->input('publication'),
                'rep_number' => $request->input('rep_number'),
                'classified_word_count' => $request->input('classified_word_count'),
                'classified_extra_word_cost' => $request->input('classified_extra_word_cost'),
                'classified_base' => $request->input('classified_base'),
                'isAdmin' => $request->input('isAdmin'),
                'isFront' => $request->input('isFront')
            ]);
        } else {
            $user = User::where('id', $id)
            ->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'office_admin_email' => $request->input('office_admin_email'),
                'production_email' => $request->input('production_email'),
                'publication' => $request->input('publication'),
                'rep_number' => $request->input('rep_number'),
                'classified_word_count' => $request->input('classified_word_count'),
                'classified_extra_word_cost' => $request->input('classified_extra_word_cost'),
                'classified_base' => $request->input('classified_base'),
                'isAdmin' => $request->input('isAdmin'),
                'isFront' => $request->input('isFront')
            ]);
        }
       
        return redirect('/users')->with('success', 'User has been updated successfully');

    }

    public function destroy ($id) 
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/users')->with('success', 'User has been deleted');
    }
}
