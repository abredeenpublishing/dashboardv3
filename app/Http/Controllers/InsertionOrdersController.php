<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsertionOrder;
use App\Models\Client;
use App\Models\User;
use DB;
use App\Http\Requests\InsertionOrderValidationRequest;
use App\Mail\InsertionOrderMail;
use Illuminate\Support\Facades\Mail;
use App\Helper\Helper;
use Illuminate\Support\Facades\Gate;

class InsertionOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //ddd($data2);
        return view('insertionorders.index');
    }

    public function search(Request $request)
    {
       
        $output="";
        $clients = DB::table('clients')->where('client_name','LIKE','%'.$request->search."%")->get();
        
        if($clients)
        {
            foreach ($clients as $key => $client) {
                $output.='<tr>'.
                '<td>'.$client->id.'</td>'.
                '<td>'. '<a href="/insertionorders/create/?id=' . $client->id . '&ad_type=' . $request->ad_type .
                '&search='.$request->search.'">' . $client->client_name .'</a></td>'.
                '<td>'.$client->city.'</td>'.
                '<td>'.$client->dti_id.'</td>'. 
                '</tr>';
            
            }
            return Response($output);
        }
        
    }

    public function getdata(Request $request)
    {
         
        $user_id = auth()->id();
        // check to see if front is logged in if so then show all orders belonging to them
        if (Gate::allows('front-only', auth()->user())) {
            $user = User::find($user_id);
            $email = $user->email;
            $q = InsertionOrder::select([
                'client_id',
                'insertion_orders.id',
                'order_number',
                'order_type',
                'caption_line',
                'total',
                'print_start_date_0',
                'print_end_date_0',
                'web_start_date',
                'web_end_date',
                'classified_start_date',
                'classified_end_date',
                'cancel',
                'users.name',
            ])  ->join('users','users.id','=','insertion_orders.user_id')
                ->where('users.office_admin_email', $email);
        } else if (Gate::allows('admin-only', auth()->user())) {    
            $user = User::find($user_id);
            $email = $user->email;
            $q = InsertionOrder::select([
                'client_id',
                'user_id',
                'insertion_orders.id',
                'order_number',
                'order_type',
                'caption_line',
                'total',
                'print_start_date_0',
                'print_end_date_0',
                'web_start_date',
                'web_end_date',
                'classified_start_date',
                'classified_end_date',
                'cancel', 
            ]); 
        } else { // sales only insertion orders
            $q = InsertionOrder::select([
                'client_id',
                'id',
                'order_number',
                'order_type',
                'total',
                'print_start_date_0',
                'print_end_date_0',
                'web_start_date',
                'web_end_date',
                'classified_start_date',
                'classified_end_date',
                'cancel',
            ])->where('user_id', $user_id); //->with('client:id,client_name')->get();
        }
       
        if ($dti_id = $request->input('dti_id')) {
            $q->wherehas('client', function($query)Use($dti_id){ $query->where('dti_id', $dti_id); });
        } elseif ($client_name = $request->input('client_name')) {
            $q->wherehas('client', function($query)Use($client_name){ $query->where('client_name','like', '%'.$client_name.'%'); });
        }
        
        $insertion_orders = $q->with('client:id,client_name')->get()->map(function($order){ 
            // this maps client_name into data-set
            $order->client_name = $order->client->client_name;
            return $order;

         });

        return \DataTables::of($insertion_orders)
            ->addColumn('action', function($data) {
                if (Gate::allows('front-only', auth()->user())) {
                    $btn = '<a href="'. route('insertionorders.editordernumber', $data->id).'"><button class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> Update Order #</button></a>';
                } else {
                    if (($data->order_number != null) && ($data->cancel != true)) {
                        $btn = '<a href="'.route('insertionorders.edit', $data->id).'" class="edit btn btn-primary btn-sm mx-1"><i class="fas fa-edit"></i> Edit</a>';
                    } else {
                        $btn = '<button class="edit btn btn-primary btn-sm mx-1" disabled><i class="fas fa-edit"></i> Edit</button>';
                    }
                    $btn = $btn.'<a href="/insertionorders/'. $data->id.'/edit?clone=y" class="edit btn btn-warning btn-sm mx-1" ><i class="far fa-copy"></i> Clone IO</a>';  
                }
                return $btn;
            })
            ->addColumn('io_type', function($data){
                if ($data->order_type == "1") {
                    $order_type = "Print";
                } else if ($data->order_type == "2") {
                    $order_type = "Web";
                } else if ($data->order_type == "3") {
                    $order_type = "Flyer";
                } else if ($data->order_type == "4") {
                    $order_type = "Classified";
                } else {
                    $order_type = "Not Found: " . $data->order_type;
                }
                return $order_type;
            })
            ->addColumn('start_date', function($data){
                if ($data->print_start_date_0 == "") {
                    if ($data->web_start_date == "") { // if there is no webdate then io is a classified
                        return Helper::cleanDate($data->classified_start_date); 
                    } else {
                        return Helper::cleanDate($data->web_start_date);
                    }
                } else {                               // This could be print io or flyer io
                    return Helper::cleanDate($data->print_start_date_0);
                }
            })
            ->addColumn('end_date', function($data){
                if ($data->print_end_date_0 == "") {
                    if ($data->web_end_date == "") { // if there is no webdate then io is a calssified
                        return Helper::cleanDate($data->classified_end_date);
                    } else {
                        return Helper::cleanDate($data->web_end_date);
                    }
                } else {                            // This could be print io or flyer io
                    return Helper::cleanDate($data->print_end_date_0);
                }
            })
            ->rawColumns(['action', 'io_type', 'start_date', 'end_date'])
            ->make(true);    


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->has('_token')) {
            $hasinput = count($request->all()) > 1;
        } else {
            $hasinput = count($request->all()) > 0;
        }
       // check to see if user and searched for client
        if ($hasinput == true)
        {
            if ($request->ad_type == "") {
                // check to see if user picked an ad type
                return view('insertionorders.create')->with('error', 'Please select Ad Type before trying to create an IO');
            }
            else {
                // success user picked ad type and searched client
                if ($request->ad_type=="Print") {
                    return redirect('/insertionorders/printio/'.$request->id);
                }
                if ($request->ad_type == "Web") {
                    return redirect('/insertionorders/webio/'.$request->id);
                }
                if ($request->ad_type == "Flyer") {
                    return redirect('/insertionorders/flyerio/'.$request->id);
                }
                if ($request->ad_type == "Classified") {
                    return redirect('/insertionorders/classifiedio/'.$request->id);
                }
            }
        } 
        else {
           // user visting page for first time
            return view('insertionorders.create')->with('error', 'none');
        }
        // Note: with('error') does not create a session, but instead creates a variable on the view which is why there are 'none'
        // error messages - this is so the variable is initialized to avoid errors on the view. 
    }
    
    public function printio($id)
    {
        //create a new print io
        $client = Client::find($id);
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $sales_reps = User::all();

        return view('insertionorders.printio')->with('client', $client)->with('user', $user)->with('sales_reps', $sales_reps);
    }

    public function webio($id)
    {
        //create a new web io
        $client = Client::find($id);
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $sales_reps = User::all();
        return view('insertionorders.webio')->with('client', $client)->with('user', $user)->with('sales_reps', $sales_reps);
    }

    public function flyerio($id)
    {
        // create a new flyer io
        $client = Client::find($id);
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $sales_reps = User::all();
        return view('insertionorders.flyerio')->with('client', $client)->with('user', $user)->with('sales_reps', $sales_reps);
    }

    public function classifiedio($id)
    {
        // create a new classified io
        $client = Client::find($id);
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $sales_reps = User::all();
        return view('insertionorders.classifiedio')->with('client', $client)->with('user', $user)->with('sales_reps', $sales_reps);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsertionOrderValidationRequest $request)
    {
        $request->validated();

        if ($request->input('order_type') == "Print") 
        {
            $order_type = "1";
        } else if ($request->input('order_type') == "Web") {
            $order_type = "2";
        } else if ($request->input('order_type') == "Flyer") {
            $order_type = "3";
        } else if ($request->input('order_type') == "Classified") {
            $order_type = "4";
        }
       
        $filenametostore = "";
         // Handle file upload with web ad
        if($request->hasFile('web_ad')) {
           // dd($request->file('web_ad'));
           foreach($request->file('web_ad') as $imagefile) {
                           
                // get file name with extension
                $filenamewithext = $imagefile->getClientOriginalName();
                // get just file name
                $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
                // get just extentsion
                $extension = $imagefile->getClientOriginalExtension();
                // File name to store
                $filenametostore = $filename.time().'.'.$extension;
                // upload image
                $path = $imagefile->storeAs('public/images', $filenametostore);
                $data[] = $filenametostore;  
            }
            $data = implode(', ', $data);
            //dd($data);    //
        } else {
            $data = $filenametostore;
        }
        // if there is no web ad to upate I don't want to update table with no ads in there are ads in there already.
        if ($data == "") {
            $input = $request->except('_token','tfn', 'order_type', 'web_ad', 'sales_rep');
        } else {
            $input = $request->except('_token','tfn', 'order_type', 'sales_rep');
            $input['web_ad'] = $data;
        }

        $input['tfn'] = $request->input('tfn')? true : false;
        $input['order_type'] = $order_type; 
        $input['cancel'] = false;

        $user_id = auth()->user()->id;
       
        // if user selected a different sales associate then instead selected themself as the sales agent then no need to record a serparate agent and clear out the field. 
        if ($input['sales_id'] == $user_id) {
            $input['sales_id'] = null;
        }
        
        $insertionorder = InsertionOrder::create($input);
        $what_changed = []; // Just need to declare it nothing will be changed on a new record
        $email_input = $request->except('_token', '_method'); 
        if ($data != "") {
            $email_input['web_ad'] = $data;
        }
        

        /*
        ####### Explanination on Variables: 

                            $insertionorder is an object passed to emailIOVars only key peices 
                            of information is extracted to select top level variables.

                            $email_input is an Array the request input vars and all its contents 
                            live with ['insertion_order'] key 

                            $what_changed is an Array of all the inputs that have changed from previous record

                            $subject is the subject for the email. 

                            $email_details is an Array of everything above. 
        ########## End
        */

        if ($order_type == "1") { // set print subject line
            $email_input['print_frequency_0'] = Helper::getFrequency($email_input['print_frequency_0']);
           
            $subject = 'IO#:'. $insertionorder->id . ' **NEW** PRINT IO for Client: ' . $insertionorder->client->client_name . ' - ' . $insertionorder->client->dti_id;
       
        } else if ($order_type == "2") {  // set web subject line
           
            $subject = 'IO#:'. $insertionorder->id . ' **NEW** WEB IO for Client: ' . $insertionorder->client->client_name . ' - ' . $insertionorder->client->dti_id;
             
        }  else if ($order_type == "3") {  // set Flyer subject line
           
            $subject = 'IO#:'. $insertionorder->id . ' **NEW** FLYER IO for Client: ' . $insertionorder->client->client_name . ' - ' . $insertionorder->client->dti_id;
             
        }  else if ($order_type == "4") {  // set Classified subject line
           
            $subject = 'IO#:'. $insertionorder->id . ' **NEW** CLASSIFIED IO for Client: ' . $insertionorder->client->client_name . ' - ' . $insertionorder->client->dti_id;
             
        }
        $email_details = Helper::createIOEmailVars($insertionorder, $email_input, $what_changed, $subject);

        
        // send email to sales
        Mail::to($insertionorder->user->email)->send(new InsertionOrderMail($email_details));

        // if the io was entered on behalf of another rep - email the "other" rep 
        if (!empty($input['sales_id'])) {
            $sales_rep = User::find($input['sales_id']);
            $email_details['message'] = "An IO has been generated on your behalf, this email is for your records.";
            Mail::to($sales_rep->email)->send(new InsertionOrderMail($email_details));
        }


         // send email to webads
         if(($request->hasFile('web_ad')) || ($insertionorder->web_ad != "")) {
            $webads_email = "webads@aberdeenpublishing.com";
            Mail::to($webads_email)->send(new InsertionOrderMail($email_details));
        }
        // send email to Admin
        $email_details['update_link'] = 'https://dashboard.aberdeenpublishing.com/insertionorders'.'/'.$insertionorder->id . "/edit-order-number";
        Mail::to($insertionorder->user->office_admin_email)->send(new InsertionOrderMail($email_details));

       

        return redirect('/insertionorders')->with('success', 'IO Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd('show insertion order');
        $insertionorder = InsertionOrder::find($id);
        return view('insertionorders.show')->with('insertionorder', $insertionorder);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(InsertionOrder $insertionorder)
    {
        
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $sales_reps = User::all();


        if ($insertionorder->order_type == "1") {
            return view('insertionorders.edit')->with('insertionorder', $insertionorder)->with('user', $user)->with('sales_reps', $sales_reps);
        } else if ($insertionorder->order_type == "2")  {   
            return view('insertionorders.editwebio')->with('insertionorder', $insertionorder)->with('user', $user)->with('sales_reps', $sales_reps);
        } else if ($insertionorder->order_type == "3")  {   
            return view('insertionorders.editflyerio')->with('insertionorder', $insertionorder)->with('user', $user)->with('sales_reps', $sales_reps);
        } else if ($insertionorder->order_type == "4")  {   
            return view('insertionorders.editclassifiedio')->with('insertionorder', $insertionorder)->with('user', $user)->with('sales_reps', $sales_reps);
        }
        //dd($insertionorder);   


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InsertionOrderValidationRequest $request, $id)
    {
        $user_id = auth()->user()->id;
        // check to see if inputs are good
        $request->validated();

        if ($request->input('order_type') == "Print")   // ################################# Print Order Type ##################################
        {
            $order_type = "1";
            // populate input data
            $input = $request->except(
                '_token',
                'tfn', 
                'order_type', 
                '_method',
                'modular_size_yes_or_no',
                'override_subtotal_yes_or_no',
                'column_inches',
                'discount',
                'sales_rep',
                'rep_number'
            );

            $input['tfn'] = $request->input('tfn')? true : false;
            $input['order_type'] = $order_type;

            //clear vars if hidden values are not true 
            if ($request->input('modular_size_yes_or_no') != "Y") {
                $input['modular_size'] = "";
            }
            if ($request->input('override_subtotal_yes_or_no') != "Y") {
                $input['subtotal_overide'] = null;
            }
            if ( $request->input('copy') == "" ) {
                $input['copy'] = "";
            }
            if ( $request->input('copy2') == "" ) {
                $input['copy2'] = "";
            }
            if ( $request->input('copy3') == "" ) {
                $input['copy3'] = "";
            }
            if ( $request->input('copy4') == "" ) {
                $input['copy4'] = "";
            }
            if ( $request->input('copy5') == "" ) {
                $input['copy5'] = "";
            }

        
        } else if ($request->input('order_type') == "Web") { // ################################# Web Order Type ##################################
            $order_type = "2";
            $removevars = [
                '_token',
                'order_type', 
                '_method',
                'sales_rep',
                'rep_number',
            ];
            $pub_code = [
                'cvp', 
                'fh',
                'ktw',
                'mh', 
                'plv',
                'tc'
            ]; // i need to remove all the ad space inputs as they all funnel into one text input. 
            for ($i = 1; $i <= 10; $i++) {
                foreach ($pub_code as $key=>$value) {
                    $removevars[] = $value.'_ad_space_'.$i;      
                }
            }

            $filenametostore = "";
            // Handle file upload with web ad
            if($request->hasFile('web_ad')) {
              // dd($request->file('web_ad'));
              foreach($request->file('web_ad') as $imagefile) {
                              
                   // get file name with extension
                   $filenamewithext = $imagefile->getClientOriginalName();
                   // get just file name
                   $filename = pathinfo($filenamewithext, PATHINFO_FILENAME);
                   // get just extentsion
                   $extension = $imagefile->getClientOriginalExtension();
                   // File name to store
                   $filenametostore = $filename.time().'.'.$extension;
                   // upload image
                   $path = $imagefile->storeAs('public/images', $filenametostore);
                   $data[] = $filenametostore;  
               }
               $data = implode(', ', $data);
               //dd($data);    //
           } else {
               $data = $filenametostore;
           }
           // if there is no web ad to upate I don't want to update table with no ads in there are ads in there already.
           if ($data == "") {
                $removevars[] = "web_ad";
                $input = $request->except($removevars);
           } else {
                $input = $request->except($removevars);
                $input['web_ad'] = $data;
           }
        } else if ($request->input('order_type') == "Flyer") { // ################################# Flyer Order Type ##################################
            $order_type = "3"; // 3 = Flyer
            // populate input data
            $input = $request->except(
                '_token',
                'order_type', 
                '_method',
                'override_subtotal_yes_or_no',
                'discount',
                'sales_rep',
                'rep_number'
            );

            
            $input['order_type'] = $order_type;

            //clear vars if hidden values are not true 
            if ($request->input('override_subtotal_yes_or_no') != "Y") {
                $input['subtotal_overide'] = null;
            }
        } else if ($request->input('order_type') == "Classified") { // ################################# Classified Order Type ##################################
            // classified update
            $order_type = "4"; // 4 = Classified
            // populate input data
            $input = $request->except(
                '_token',
                'order_type',
                '_method',
                'override_subtotal_yes_or_now',
                'discount',
                'ctrl_classified_word_count', 
                'ctrl_classified_extra_word_cost',
                'ctrl_classified_base',
                'sales_rep',
                'rep_number'
            );

        }
       
        // if user selected a different sales associate then instead selected themself as the sales agent then no need to record a serparate agent and clear out the field. 
        if ($input['sales_id'] == $user_id) {
            $input['sales_id'] = null;
        }
        // before I update database with new values I need what was there to track what changed
        $insertionorder = InsertionOrder::find($id);   
        $what_changed = $this->track_changes($input, $insertionorder);
        $email_input = $request->except('_token', '_method'); 
        $email_input['web_ad'] = $insertionorder->web_ad;
        
        if ($email_input['sales_id'] == $user_id) {
            $email_input['sales_id'] = null;
        }

        //dd($email_input);
        
        // update database
        InsertionOrder::where('id', $id)->update($input);


        if ($order_type == "1") { // set print subject line
            $email_input['print_frequency_0'] = Helper::getFrequency($email_input['print_frequency_0']);
           
            $subject = 'IO#:'. $insertionorder->id . ' ##Update## PRINT IO for Client: ' . $insertionorder->client->client_name . ' - '. $insertionorder->client->dti_id;
       
        } else if ($order_type == "2") {  // set web subject line
           
            $subject = 'IO#:'. $insertionorder->id . ' ##Update## WEB IO for Client: ' . $insertionorder->client->client_name . ' - '. $insertionorder->client->dti_id;
        } else if ($order_type == "3") {  // set flyer subject line
           
            $subject = 'IO#:'. $insertionorder->id . ' ##Update## FLYER IO for Client: ' . $insertionorder->client->client_name . ' - '. $insertionorder->client->dti_id;
        } else if ($order_type == "4") {  // set classified subject line
           
            $subject = 'IO#:'. $insertionorder->id . ' ##Update## CLASSIFIED IO for Client: ' . $insertionorder->client->client_name . ' - ' . $insertionorder->client->dti_id;     
        }
        //$email_input['print_frequency_0'] = Helper::getFrequency($email_input['print_frequency_0']);
        // setup email variables 
       // $subject = 'IO#:'. $id . ' Update IO for Client: ' . $insertionorder->client->dti_id;
        
        $email_details = Helper::createIOEmailVars($insertionorder, $email_input, $what_changed, $subject);
        
        // send email to sales
        Mail::to($insertionorder->user->email)->queue(new InsertionOrderMail($email_details));

        // if the io was updated on behalf of another rep - email the "other" rep 
        if (!empty($email_input['sales_id'])) {
            $sales_rep = User::find($input['sales_id']);
            $email_details['message'] = "This IO was edited by another rep, this email is for your records.";
            Mail::to($sales_rep->email)->send(new InsertionOrderMail($email_details));
            $email_details['message'] = "";
        }

        // send email to webads 
        if(($request->hasFile('web_ad')) || ($insertionorder->web_ad != "")) {
            $webads_email = "webads@aberdeenpublishing.com";
            Mail::to($webads_email)->queue(new InsertionOrderMail($email_details));
        }

        
        $email_details['update_link'] = 'https://dashboard.aberdeenpublishing.com/insertionorders'.'/'.$insertionorder->id . "/edit-order-number";
        // send email with update link to office admin
        Mail::to($insertionorder->user->office_admin_email)->queue(new InsertionOrderMail($email_details));

        
        return redirect (route('insertionorders.show', $insertionorder->id))->with('success', 'IO has been updated');
        
        
    }

    public function track_changes($new_data, $old_data)
    {
        // new_date is an array
        // old_data is an object of insertionorder 
        $changes = [];

        foreach($new_data as $key => $value)
        {
            if ($value != $old_data->$key) {
                //insert div to mark changed area
                
                if ($key == "print_frequency_0") {
                    if ($value == "1") {
                        $value = "Once a Week";
                    } elseif ($value == "2") {
                        $value = "Every Other Week";
                    } elseif ($value == "3") {
                        $value = "Every 3rd Week of the Month";
                    } elseif ($value == "4") {
                        $value = "Once a Month";
                    }
                
                }
                if (Helper::validateDate($value) == true) {
                    $value = Helper::cleanDate($value);
                }

                $changes[$key] = "<div class='this-changed'>" . $value . "</div>";     
            }
        }
        return $changes;

        
    }

    public function edit_order_number($id)
    {
        $insertionorder = InsertionOrder::find($id);
        
        return view('insertionorders.editordernumber')->with('insertionorder', $insertionorder);

    }

    public function update_order_number(Request $request, $id)
    {
        
        $rules = [
            'order_number' => 'required'
        ];

        if ($request->input('chk_dti_id') == "") {
            $rules['dti_id'] = 'required';
        }
        $request->validate($rules);

        $input = $request->except('_token', 'chk_dti_id', 'dti_id', '_method', 'client_id');
        
        // update DTI_ID in client table
        if ($request->input('chk_dti_id') == "") {
            $client_id = $request->input('client_id');
            Client::where('id', $client_id)->update([
                'dti_id' => $request->input('dti_id'),
            ]);
        }
        // update order_number in insertion_orders table
        InsertionOrder::where('id', $id)->update($input);

        $insertionorder = InsertionOrder::find($id);

        $email_input = $insertionorder->toArray();
        
        $email_input['column_inches'] = $insertionorder->columns_wide * $insertionorder->inches_high;
        $what_changed = [];
        // email confirmation
        // setup email variables 
        $subject = 'IO#:'. $insertionorder->id . ' **Confirmation** IO for Client: ' . $insertionorder->client->client_name . ' - ' . $insertionorder->client->dti_id;
        $email_details = Helper::createIOEmailVars($insertionorder, $email_input, $what_changed, $subject);
        
        $email_details['insertion_order']['order_type'] = Helper::getioType($insertionorder->order_type);
        
        // send email to sales
        Mail::to($insertionorder->user->email)->send(new InsertionOrderMail($email_details));
         // send confirmation to office admin
        // Mail::to($insertionorder->user->office_admin_email)->send(new InsertionOrderMail($email_details));
        // send confirmation to production
        Mail::to($insertionorder->user->production_email)->send(new InsertionOrderMail($email_details));

        return redirect ('/')->with('success', 'Order Number has been updated');
               
        
    }

    public function cancel ($id)
    {
        // cancel order 
        $insertionorder = InsertionOrder::find($id);

        return view('insertionorders.cancelio')->with('insertionorder', $insertionorder);


    }

    public function cancelio (Request $request, $id)
    {

        // update database of cancelation and email all. 
        $rules = [
            'cancel_message' => 'required'
        ];

        $request->validate($rules);
        $input = $request->except('_token', '_method');
        // update insertionorder as canceled
        InsertionOrder::where('id', $id)->update($input);

        $insertionorder = InsertionOrder::find($id);

        $email_input = $insertionorder->toArray();
        
        $email_input['column_inches'] = $insertionorder->columns_wide * $insertionorder->inches_high;
        $what_changed = [];
        
        // email confirmation
        // setup email variables 
        $subject = 'IO#:'. $insertionorder->id . ' **CANCEL** IO for Client: ' . $insertionorder->client->dti_id;
        $email_details = Helper::createIOEmailVars($insertionorder, $email_input, $what_changed, $subject);
        
        $email_details['insertion_order']['order_type'] = Helper::getioType($insertionorder->order_type);
        
        // send email to sales
        Mail::to($insertionorder->user->email)->send(new InsertionOrderMail($email_details));
        
        // send email to admin
        Mail::to($insertionorder->user->office_admin_email)->send(new InsertionOrderMail($email_details));
        if ($insertionorder->order_number != "") {
            // send confirmation to production
            Mail::to($insertionorder->user->production_email)->send(new InsertionOrderMail($email_details));
        }

        return redirect (route('insertionorders.show', $insertionorder->id))->with('success', 'Insertion Order has been canceled');


    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
