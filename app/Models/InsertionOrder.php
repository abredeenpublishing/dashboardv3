<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Client;

class InsertionOrder extends Model
{
    use HasFactory;

   // protected $dates = ['print_start_date_0','print_end_date_0'];
    //protected $casts = ['name_of_field' => 'date', 'name_of_2nd_field' => 'datetime'];



    protected $guarded=['id']; 
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    
}
