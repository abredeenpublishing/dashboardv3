<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\InsertionOrder;

class Client extends Model
{
    use HasFactory;

    public function insertionorders()
    {
        return $this->hasMany(InsertionOrder::class);
    }

}
