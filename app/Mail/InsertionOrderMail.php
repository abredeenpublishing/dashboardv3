<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InsertionOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->details['subject'];
        $from = $this->details['sales_email'];
        $name = $this->details['sales_rep'];
        return $this->subject($subject)
            ->from($from, $name)
            ->replyTo($from, $name)
            ->view('emails.insertionordermail');
        //return $this->view('view.name');
    }
}
