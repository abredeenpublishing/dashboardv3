<?php

use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\Auth\{ConfirmPasswordController, ForgetPasswordController};
//use App\Http\Controllers\Auth\ForgetPasswordController;
//use App\Http\Controllers\Auth\LoginController;
//use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\ClientsController;
use App\Http\Controllers\InsertionOrdersController;
//use vendor\DataTables;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/reset', [ResetPasswordController::class, 'reset'])->name('password/reset');

Route::post('/reset', [ResetPasswordController::class, 'update'])->name('password.update');
Auth::routes();

Route::group(['middleware' => ['auth','web']], function () {
    
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])
        ->name('home');
   
    Route::resource('users', UsersController::class);
    
    
    Route::get('/clients/getdata', [ClientsController::class, 'getdata'])->name('clients.getdata');
    Route::get('/clients/{client}/showinsertions', [ClientsController::class, 'showinsertions'])->name('clients.showinsertions');
    Route::resource('clients', ClientsController::class);
    
    Route::get('/insertionorders/getdata', [InsertionOrdersController::class, 'getdata'])->name('insertionorders.getdata');
    Route::get('/insertionorders/search', [InsertionOrdersController::class, 'search'])->name('insertionorders.search');
    Route::get('/insertionorders/printio/{client}', [InsertionOrdersController::class, 'printio'])->name('insertionorders.printio');
    Route::get('/insertionorders/webio/{client}', [InsertionOrdersController::class, 'webio'])->name('insertionorders.webio');
    Route::get('/insertionorders/flyerio/{client}', [InsertionOrdersController::class, 'flyerio'])->name('insertionorders.flyerio');
    Route::get('/insertionorders/classifiedio/{client}', [InsertionOrdersController::class, 'classifiedio'])->name('insertionorders.classifiedio');
    Route::get('/insertionorders/{insertionorder}/edit-order-number', [InsertionOrdersController::class, 'edit_order_number'])->name('insertionorders.editordernumber');
    Route::put('/insertionorders/edit-order-number/{insertionorder}', [InsertionOrdersController::class, 'update_order_number'])->name('insertionorders.updateordernumber');
    Route::get('/insertionorders/cancel/{insertionorder}', [InsertionOrdersController::class, 'cancel'])->name('insertionorders.cancel');
    Route::put('/insertionorders/cancel/{insertionorder}', [InsertionOrdersController::class, 'cancelio'])->name('insertionorders.cancelio');
    Route::resource('insertionorders', InsertionOrdersController::class);
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home2'); 
    
 });


