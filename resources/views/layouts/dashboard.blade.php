
@extends('adminlte::page')


@section('content_header')
@include('inc.messages')
@stop
 
@section('content')

    @yield('content')   

@stop


@section('right-sidebar')
    <div class="container">
        <div class="row">
            <div class='col m-3'>
       @if (Gate::allows('admin-only', auth()->user())) 
            <a href="/users/"><i class="fas fa-users"></i> Manage Users</a>
        @endif
            </div>
        </div>

    </div>
@stop

@include('inc.footer') 

