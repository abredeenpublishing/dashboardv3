@extends('layouts.dashboard')


@section('content')
@section('title', 'Dashboard - Home')
    <h1>Dashboard</h1>
   
    @if (Gate::allows('front-only', auth()->user()))
           Please see the below IO's that need to be entered into DTI. Please update the order numbers accordingly.
            
                <table id="insertions" class="table table-condensed dataTable no-footer" role="grid" aria-describedby="insertions-table_info">
                    <thead>
                        <tr role="row">
                            <th>IO #</th>
                            <th>Sales Rep</th>
                            <th>Client Name</th>
                            <th>DTI ID</th>
                            <th>Caption Line</th>
                            <th>Order Type</th>
                            <th>Action</th>
             
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($insertionorders as $insertionorder )
                            <tr>
                                 <td><a href="{{ route('insertionorders.show', $insertionorder->id) }}">{{ $insertionorder->id }}</a></td>
                                 <td>{{ $insertionorder->name }}</td>
                                 <td><a href="{{ route('clients.show', $insertionorder->client_id ) }}">{{ $insertionorder->client_name }}</a></td>
                                 <td>{{ $insertionorder->dti_id }}</td>
                                 <td>{{ $insertionorder->caption_line }}</td>
                                 <td>{{ Helper::getioType($insertionorder->order_type) }}</td>
                                 <td>
                                     <div class="row">
                                         <div class="col-xs-6 px-2">
                                            <a href="{{ route('insertionorders.editordernumber', $insertionorder->id) }}">
                                                <button class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> Update Order #</button>
                                            </a>
                                         </div>
                                        
                                     </div>
                                 </td>
             
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <br><br>
            
    @endif
    @if (Gate::allows('sales-only', auth()->user()))
        <div class="row">
            <div class="col">
                <div class="card">
                <h5 class="card-header bg-success"><i class="fas fa-fw fa-plus-circle "></i> Create IO</h5>
                <div class="card-body">
                    <h5 class="card-title">Create Print or Web IO</h5>
                    <p class="card-text">Here you can select the type of IO you wish to create and search for the client.</p>
                    <a href="{{ route('insertionorders.create') }}" class="btn btn-success">Create IO</a>
                </div>
                </div>
            </div>            
            <div class="col">
                <div class="card">
                    <h5 class="card-header bg-info"><i class="fas fa-fw fa-money-check-alt "></i> Insertion Orders</h5>
                    <div class="card-body">
                        <h5 class="card-title">View all your IO's</h5>
                        <p class="card-text">Here you can view all of the IO's that you have entered, edit them and clone!</p>
                        <a href="{{ route('insertionorders.index') }}" class="btn btn-info">View Insertion Orders</a>
                    </div>
                    </div>
            </div>            
            <div class="col">
                <div class="card">
                    <h5 class="card-header bg-warning"><i class="fas fa-fw fa-briefcase "></i> Client Database</h5>
                    <div class="card-body">
                        <h5 class="card-title">View all Clients</h5>
                        <p class="card-text">Here you can view/search all client data and add new clients to the database.</p>
                        <a href="{{ route('clients.index') }}" class="btn btn-warning">View Client Database</a>
                    </div>
                    </div>
            </div>            
        </div>
    @endif
@stop


@section('js')
<script> 
    jQuery(function($) {
       //initiate dataTables plugin
       var myTable = 
       $('#insertions')
           
       .DataTable( {
           bFilter: false,
           responsive: true,
           columns: [
           {data: 'id', name: 'id'},
           {data: 'name', name: 'name'},
           {data: '', name: 'Action'}
       ],
       initComplete: function () {
           this.api().columns().every(function () {
               var column = this;
               var input = document.createElement("input");
               $(input).appendTo($(column.footer()).empty())
               .on('change', function () {
                   column.search($(this).val(), false, false, true).draw();
               });
           });
       }
           });
       });
   
   console.log('Hi!'); </script>
@stop