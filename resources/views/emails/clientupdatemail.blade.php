<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Client Update Email</title>
   
<style>
    .this-changed {
        background:yellow;
        color:black;
        font-family: "Verdana";
        display:inline;
    }

    table, tr, td {
        font-family: "Verdana";
        padding: 0px;
    }
    td {
    
    }
    h1, h2, h3 {
        font-family: "Verdana";
        margin:0;
    }
    .table-header {
        background: #384c61;
        color:#fff;
        
    }
    .td-header {
        background-clip: padding-box; /* this has been added */
        
    
        border: 5px solid #384c61;
    }
    hr {
      border-top: 1px solid #e75333;
    }
    .money {
      font-family: "courier new", courier, monospace;
      font-size: 18px;
    }
    .canceled {
        color:darkred;
    }

</style>
</head>
<body>

    <table width="800" >
        <tr class="table-header">
            <td class="td-header" colspan="2">
                <h3>Client Update Notification</h3>
            </td>
        </tr>
        <tr>
            <td><strong>DTI ID:</strong></td>
            <td>{!! array_key_exists('dti_id', $details['what_changed']) == false ? $details['client_data']['dti_id'] : $details['what_changed']['dti_id'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Company Name:</strong></td>
            <td>{!! array_key_exists('client_name', $details['what_changed']) == false ? $details['client_data']['client_name'] : $details['what_changed']['client_name'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Address:</strong></td>
            <td>{!! array_key_exists('address', $details['what_changed']) == false ? $details['client_data']['address'] : $details['what_changed']['address'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>City:</strong></td>
            <td>{!! array_key_exists('city', $details['what_changed']) == false ? $details['client_data']['city'] : $details['what_changed']['city'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Provice:</strong></td>
            <td>{!! array_key_exists('province', $details['what_changed']) == false ? $details['client_data']['province'] : $details['what_changed']['province'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Postal Code:</strong></td>
            <td>{!! array_key_exists('postal_code', $details['what_changed']) == false ? $details['client_data']['postal_code'] : $details['what_changed']['postal_code'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Billing Contact Name:</strong></td>
            <td>{!! array_key_exists('billing_contact_name', $details['what_changed']) == false ? $details['client_data']['billing_contact_name'] : $details['what_changed']['billing_contact_name'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Billing Contact Phone:</strong></td>
            <td>{!! array_key_exists('billing_contact_phone', $details['what_changed']) == false ? $details['client_data']['billing_contact_phone'] : $details['what_changed']['billing_contact_phone'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Billing Contact Email:</strong></td>
            <td>{!! array_key_exists('billing_contact_email', $details['what_changed']) == false ? $details['client_data']['billing_contact_email'] : $details['what_changed']['billing_contact_email'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Client Contact Name:</strong></td>
            <td>{!! array_key_exists('client_contact_name', $details['what_changed']) == false ? $details['client_data']['client_contact_name'] : $details['what_changed']['client_contact_name'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Client Contact Phone:</strong></td>
            <td>{!! array_key_exists('client_contact_phone', $details['what_changed']) == false ? $details['client_data']['client_contact_phone'] : $details['what_changed']['client_contact_phone'] !!}</td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td><strong>Client Contact Email:</strong></td>
            <td>{!! array_key_exists('client_contact_email', $details['what_changed']) == false ? $details['client_data']['client_contact_email'] : $details['what_changed']['client_contact_email'] !!}</td>
        </tr>
    </table>


</body>
</html>
