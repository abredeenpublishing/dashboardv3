<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Insertion Order Email</title>
   
<style>
    .this-changed {
        background:yellow;
        color:black;
        font-family: "Verdana";
        display:inline;
    }

    table, tr, td {
        font-family: "Verdana";
        padding: 0px;
    }
    td {
    
    }
    h1, h2, h3 {
        font-family: "Verdana";
        margin:0;
    }
    .table-header {
        background: #384c61;
        color:#fff;
        
    }
    .td-header {
        background-clip: padding-box; /* this has been added */
        
    
        border: 5px solid #384c61;
    }
    hr {
      border-top: 1px solid #e75333;
    }
    .money {
      font-family: "courier new", courier, monospace;
      font-size: 18px;
    }
    .canceled {
        color:darkred;
    }

</style>
</head>
<body>
    @if (array_key_exists('update_link', $details))
        <a href="{{ $details['update_link'] }}">Update Order Number</a>
    @endif
    @if (array_key_exists('message', $details))
       <strong>{{ $details['message'] }}</strong>
    @endif
   @if (array_key_exists('cancel', $details['insertion_order']) == true)
        @if ($details['insertion_order']['cancel'] == "1")
            <div>
                <h1 class="canceled">Order Canceled:</h1>
                <p class="cenceled">{{ $details['insertion_order']['cancel_message'] }}</p>
            </div>
            <br><br>
            <p>Please remove this order from the billing system. </p>
        @endif
    @endif
    <table width="800" >
        <tr class="table-header">
            <td class="td-header" >
                <h3 style="float: left;">{{ $details['publication'] }}</h3>
                <h3 style="float: right;">{{ date('M d, Y', strtotime($details['insertion_date'])) }}</h3>
            <h3 style="float:left; margin-left:200px; margin-right:auto;">{{ $details['insertion_order']['order_type'] }} IO</h3></td>
        </tr>
        <tr>
            <td><hr></td>
        </tr>
    </table>
    <table width="800">
        <tr> <!-- Row 1 -->
            <td width="400" valign="top"> <!-- Column 1 -->
                <table width="100%">
                    <tr>
                        <td>Advertiser:</td>
                        <td><h3>
                            {{ $details['client_name'] }}
                        </h3></td>
                    </tr>
                    <tr>
                        <td valign="top"></td><td>{{ $details['address'] }}</td> <!--Address_1-->
                    </tr>
                    <tr>
                        <td>&nbsp;</td><td>{{ $details['city'] }}, {{ $details['province'] }}</td>  <!--City, Prvovice-->
                    </tr>
                    <tr>
                        <td>&nbsp;</td><td>{{ $details['postal_code'] }}</td> <!--Postal Code-->
                    </tr>
                    <tr>
                        <td>Contact: </td><td>{{ $details['billing_contact_name'] }}</td> <!--Phone -->
                    </tr>
                    <tr>
                        <td>Phone: </td><td>{{ $details['billing_contact_phone'] }}</td> <!--Phone -->
                    </tr>
                    <tr>
                        <td>Email: </td><td><a href="mailto:{{ $details['billing_contact_email'] }}">{{ $details['billing_contact_email'] }}</a></td> <!--email -->
                    </tr>
                </table>
            </td>
            <td width="400" valign="top"> <!-- Column 2 -->
                <table width="100%">
                    <tr>
                        <td><h3>DTI Account #: </h3></td><td><h3>{{ $details['dti_id'] }}</h3></td>
                    </tr>
                   
                    <tr>
                        <td>PO Number: </td><td>{!! array_key_exists('po_number', $details['what_changed']) == false ? $details['insertion_order']['po_number'] : $details['what_changed']['po_number'] !!}</td>
                    </tr>
                    @if (array_key_exists('sales_id', $details['what_changed'])) 
                        <tr>
                            <td>Sales Rep: </td><td><div class='this-changed'>{{ $details['sales_rep'] }}</div></td>
                        </tr>
                        <tr>
                            <td>Sales Rep No: </td><td><div class='this-changed'>{{ $details['rep_number'] }}</div></td>
                        </tr> 
                    @else 
                        <tr>
                            <td>Sales Rep: </td><td>{{ $details['sales_rep'] }}</td>
                        </tr>
                        <tr>
                            <td>Sales Rep No: </td><td>{{ $details['rep_number'] }}</td>
                        </tr> 
                    @endif                  
                    @if ($details['order_number'] != "")
                    <tr>
                        <td colspan="2"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                        <td><h3 style="color:#3CA10F;">Order Number:</h3></td><td><h3 style="color:#3CA10F;">{{ $details['order_number'] }}</h3></td>
                    </tr>
                    @endif
                    
                    
                </table>     
            </td>
        </tr> <!-- Ends Row 1 -->
        <tr>
            <td colspan="2"><p>&nbsp;</p></td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>Order Details</h3>
                <hr>
            </td>
        </tr>
      
        @if ($details['insertion_order']['order_type'] == "Print")
        <tr>
            <td with="400" valign="top"> <!-- Column 1 -->
                <table width="100%">
                    <tr>
                        <td><strong>Ad Specs:</strong></td>
                    </tr>
                    @if (array_key_exists('modular_size_yes_or_no', $details['insertion_order']) == true)
                        <tr>
                            <td> Modular Size: 
                                {!! array_key_exists('modular_size', $details['what_changed']) == false ? $details['insertion_order']['modular_size'] : $details['what_changed']['modular_size'] !!}
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td>
                            Columns Wide: 
                                <strong>
                                {!! array_key_exists('columns_wide', $details['what_changed']) == false ? $details['insertion_order']['columns_wide'] : $details['what_changed']['columns_wide'] !!}    
                                </strong> 
                            x Inches High: 
                                <strong>
                                {!! array_key_exists('inches_high', $details['what_changed']) == false ? $details['insertion_order']['inches_high'] : $details['what_changed']['inches_high'] !!}
                                </strong> 
                            </td>
                        </tr>
                        <tr>
                            <td>Column Inches:
                               
                                {{ $details['insertion_order']['column_inches'] }}</td>
                        </tr>
                        <tr>
                            <td>Column Rate: $
                                {!! array_key_exists('column_rate', $details['what_changed']) == false ? $details['insertion_order']['column_rate'] : $details['what_changed']['column_rate'] !!}
                                </td>
                        </tr>
                        <tr>
                            <td>Colour Charge: 
                                {!! array_key_exists('colour_charge', $details['what_changed']) == false ? $details['insertion_order']['colour_charge'] : $details['what_changed']['colour_charge'] !!}
                                </td>
                        </tr>    
                    @endif
                   <tr><td>&nbsp;</td></tr>
                   <tr>
                        <td class="money">Discount: $ 
                            <?php 
                                $output = 0;
                                if (array_key_exists('print_discount', $details['insertion_order']) == true) {
                                    $discount =  $details['insertion_order']['print_discount'];
                                    if ($discount != null) {
                                        $subtotal = $details['insertion_order']['subtotal'];
                                        // need to find inverse of the original pertage then divide the subtotal by the inverse to get the original subtotal. This is neseccesy to figure out the dollar amount of the total discount given. 
                                        $inverse_amount = (100 - $discount)/100;
                                        $old_total = $subtotal / $inverse_amount;
                                        $discount_amount = $old_total - $subtotal;
                                        if (array_key_exists('print_discount', $details['what_changed']) == true) {
                                            $output = "<div class='this-changed'>" . $discount_amount . " (". $discount ."% off)" . "</div>";
                                        } else {
                                            $output = $discount_amount . " (". $discount ."% off)";
                                        }
                                    }
                                }
                                ?>
                        {!! $output !!}
                        </td>
                    </tr>
                    <tr>
                        <td class="money">Subtotal: $
                            {!! array_key_exists('subtotal', $details['what_changed']) == false ? $details['insertion_order']['subtotal'] : $details['what_changed']['subtotal'] !!}
                            </td>
                    </tr>
                   
                    <tr>
                        <td class="money">GST (5%): $
                            {!! array_key_exists('gst', $details['what_changed']) == false ? $details['insertion_order']['gst'] : $details['what_changed']['gst'] !!}
                            </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td class="money"><strong>Total: &nbsp;&nbsp;&nbsp;$
                            {!! array_key_exists('total', $details['what_changed']) == false ? $details['insertion_order']['total'] : $details['what_changed']['total'] !!}
                            </td>
                    </tr>
                    @if (array_key_exists('payment_method', $details['insertion_order']) == true)
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td>Method of Payment: 
                                {!! array_key_exists('payment_method', $details['what_changed']) == false ? $details['insertion_order']['payment_method'] : $details['what_changed']['payment_method'] !!}
                            </td>
                        </tr>
                    @endif
                </table>
            </td>
            <td width="400" valign="top"> <!-- Column 2 -->
                <table width="100%">
                    <tr>
                        <td>
                            <strong>Production Information:</strong>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Caption Line:</strong>
                            {!! array_key_exists('caption_line', $details['what_changed']) == false ? $details['insertion_order']['caption_line'] : $details['what_changed']['caption_line'] !!}
                            </td>
                    </tr>
                        <tr>
                            <td>Position Requested:
                                {!! array_key_exists('position_requested', $details['what_changed']) == false ? $details['insertion_order']['position_requested'] : $details['what_changed']['position_requested'] !!}
                                </td>
                        </tr>
                        <tr>
                            <td>Feature: 
                                {!! array_key_exists('feature', $details['what_changed']) == false ? $details['insertion_order']['feature'] : $details['what_changed']['feature'] !!}
                            </td>
                        </tr>
                        <tr>
                            <td>Colour: 
                                {!! array_key_exists('color', $details['what_changed']) == false ? $details['insertion_order']['color'] : $details['what_changed']['color'] !!}
                            </td>
                        </tr>
                        <tr>
                            <td>Copy: 
                                @if (array_key_exists('copy', $details['insertion_order']))
                                <li>{!! array_key_exists('copy', $details['what_changed']) == false ? $details['insertion_order']['copy'] : $details['what_changed']['copy'] !!}</li>
                                @endif
                                @if (array_key_exists('copy2', $details['insertion_order']))
                                <li>{!! array_key_exists('copy2', $details['what_changed']) == false ? $details['insertion_order']['copy2'] : $details['what_changed']['copy2'] !!}</li>
                                @endif
                                @if (array_key_exists('copy3', $details['insertion_order']))
                                <li>{!! array_key_exists('copy3', $details['what_changed']) == false ? $details['insertion_order']['copy3'] : $details['what_changed']['copy3'] !!}</li>
                                @endif
                                @if (array_key_exists('copy4', $details['insertion_order']))
                                <li>{!! array_key_exists('copy4', $details['what_changed']) == false ? $details['insertion_order']['copy4'] : $details['what_changed']['copy4'] !!}</li>
                                @endif
                                @if (array_key_exists('copy5', $details['insertion_order']))
                                <li>{!! array_key_exists('copy5', $details['what_changed']) == false ? $details['insertion_order']['copy5'] : $details['what_changed']['copy5'] !!}</li>
                                @endif
                            </td>
                        </tr>
                       
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td>
                            {!! array_key_exists('print_frequency_0', $details['what_changed']) == false ? $details['insertion_order']['print_frequency_0'] : $details['what_changed']['print_frequency_0'] !!}
                        </td>
                        <td colspan="2">
                        Ad Type: <h3 style="display:inline;">  {!! array_key_exists('ad_type', $details['what_changed']) == false ? $details['insertion_order']['ad_type'] : $details['what_changed']['ad_type'] !!}</h3>
                        </td>
                        <td>TFN: 
                            @if (array_key_exists('tfn', $details['insertion_order']))
                            {!! array_key_exists('tfn', $details['what_changed']) == false ? $details['insertion_order']['tfn'] : $details['what_changed']['tfn'] !!}
                            @else
                                No
                            @endif
                        </td>
                    </tr>
                   
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                   
                    <tr>
                        <td><strong>Start Date</strong></td>
                        <td><strong>End Date</strong></td>
                        <td><strong>Publication</strong></td>
                        <td><strong>No. of Insertions</strong></td>
                    </tr>
                    <tr>
                        <td colspan="4"><hr></td>
                    </tr>
                    <?php $i = 0; ?>
                    @while ($i <= 12)
                        @if ($i == 0)
                            <tr>
                                <td>{!! array_key_exists('print_start_date_'.$i, $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['print_start_date_'.$i]) : $details['what_changed']['print_start_date_'.$i] !!}</td>
                                <td>{!! array_key_exists('print_end_date_'.$i, $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['print_end_date_'.$i]) : $details['what_changed']['print_end_date_'.$i] !!}</td>
                                <td>{!! array_key_exists('print_publication_'.$i, $details['what_changed']) == false ? $details['insertion_order']['print_publication_'.$i] : $details['what_changed']['print_publication_'.$i] !!}</td>
                                <td>{!! array_key_exists('print_no_insertions_'.$i, $details['what_changed']) == false ? $details['insertion_order']['print_no_insertions_'.$i] : $details['what_changed']['print_no_insertions_'.$i] !!}</td>
                            </tr>
                            <tr>
                                <td colspan="4"><hr></td>
                            </tr>    
                        @elseif ($details['insertion_order']['print_start_date_'.$i] != "")
                            <tr>
                                <td>{!! array_key_exists('print_start_date_'.$i, $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['print_start_date_'.$i]) : $details['what_changed']['print_start_date_'.$i] !!}</td>
                                <td>{!! array_key_exists('print_end_date_'.$i, $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['print_end_date_'.$i]) : $details['what_changed']['print_end_date_'.$i] !!}</td>
                                <td>{!! array_key_exists('print_publication_'.$i, $details['what_changed']) == false ? $details['insertion_order']['print_publication_'.$i] : $details['what_changed']['print_publication_'.$i] !!}</td>
                                <td>{!! array_key_exists('print_no_insertions_'.$i, $details['what_changed']) == false ? $details['insertion_order']['print_no_insertions_'.$i] : $details['what_changed']['print_no_insertions_'.$i] !!}</td>
                            </tr>
                            <tr>
                                <td colspan="4"><hr></td>
                            </tr>    
                        @endif
                       
                    <?php $i++; ?>
                    @endwhile
                    <tr>
                        <td>&nbsp;</td>
                    </tr>     
                </table>

            </td>
        </tr>
      
        @elseif ($details['insertion_order']['order_type'] == "Web") <!--Web Order details -->
            <tr>    
                <td>
                    <strong>Ad Details:</strong><br><br>
                    URL ad Links to: {!! array_key_exists('web_url', $details['what_changed']) == false ? $details['insertion_order']['web_url'] : $details['what_changed']['web_url'] !!}
                    <br>
                    Start Date: {!! array_key_exists('web_start_date', $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['web_start_date']) : $details['what_changed']['web_start_date'] !!}
                    <br>
                    End Date: {!! array_key_exists('web_end_date', $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['web_end_date']) : $details['what_changed']['web_end_date'] !!}
                    <br><br>
                    Subtotal: {!! array_key_exists('subtotal', $details['what_changed']) == false ? $details['insertion_order']['subtotal'] : $details['what_changed']['subtotal'] !!}
                    <br>
                    GST (5%): {!! array_key_exists('gst', $details['what_changed']) == false ? $details['insertion_order']['gst'] : $details['what_changed']['gst'] !!}
                    <br><Br>
                    Total: {!! array_key_exists('total', $details['what_changed']) == false ? $details['insertion_order']['total'] : $details['what_changed']['total'] !!}
                </td>
                <td>
                    <strong>Ad Spot &amp; Instructions:</strong>
                    <br><br>
                    Online Asset: {!! array_key_exists('web_online_asset', $details['what_changed']) == false ? $details['insertion_order']['web_online_asset'] : $details['what_changed']['web_online_asset'] !!}
                    <br>
                    Ad Network and Space:<br> {!! array_key_exists('web_ad_space', $details['what_changed']) == false ? $details['insertion_order']['web_ad_space'] : $details['what_changed']['web_ad_space'] !!}
                    <br><br>
                    Rotation Percentage: {!! array_key_exists('rotation_percentage', $details['what_changed']) == false ? $details['insertion_order']['rotation_percentage'] : $details['what_changed']['rotation_percentage'] !!}
                    <br><br>
                    Impressions: {!! array_key_exists('impressions', $details['what_changed']) == false ? $details['insertion_order']['impressions'] : $details['what_changed']['impressions'] !!}
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>   
                @if(array_key_exists('web_ad', $details['insertion_order']) == true)
                    <tr>
                        <td colspan="2">
                            Web Ads:
                        
                            @foreach (explode(', ', $details['insertion_order']['web_ad']) as $filename)
                                <li> {{ config('app.url') }}/storage/images/{{ $filename }}
                            @endforeach
                        </td>   
                    </tr>
                @else
                    @if ($details['web_ad'] != "")
                        <tr>
                            <td colspan="2">
                                Web Ads:
                            
                                @foreach (explode(', ', $details['web_ad']) as $filename)
                                    <li> {{ config('app.url') }}/storage/images/{{ $filename }}
                                @endforeach
                            </td>
                        </tr>
                    @endif
                @endif
           
        @elseif ($details['insertion_order']['order_type'] == "Flyer") <!--FLYER Order details -->
            <tr>
                <td>
                    Dollars Per Thousand: {!! array_key_exists('flyer_dollars_per_thousand', $details['what_changed']) == false ? $details['insertion_order']['flyer_dollars_per_thousand'] : $details['what_changed']['flyer_dollars_per_thousand'] !!}
                    <br><br>
                    Flyer Quantity: {!! array_key_exists('flyer_quanity', $details['what_changed']) == false ? $details['insertion_order']['flyer_quanity'] : $details['what_changed']['flyer_quanity'] !!} 
                    <br><br>
                    Flyer Discount: {!! array_key_exists('flyer_discount', $details['what_changed']) == false ? $details['insertion_order']['flyer_discount'] : $details['what_changed']['flyer_discount'] !!} 
                    <br><br>
                    Subtotal: {!! array_key_exists('subtotal', $details['what_changed']) == false ? $details['insertion_order']['subtotal'] : $details['what_changed']['subtotal'] !!}
                    <br>
                    GST (5%): {!! array_key_exists('gst', $details['what_changed']) == false ? $details['insertion_order']['gst'] : $details['what_changed']['gst'] !!}
                    <br><Br>
                    Total: {!! array_key_exists('total', $details['what_changed']) == false ? $details['insertion_order']['total'] : $details['what_changed']['total'] !!}
                </td>
                <td valign="top">
                    <strong>Additional Information:</strong><br>
                    Flyer Indentification:<br> {!! array_key_exists('flyer_identification', $details['what_changed']) == false ? $details['insertion_order']['flyer_identification'] : $details['what_changed']['flyer_indentification'] !!} 
                    <br><br>
                    Distribution Instructions:<br> {!! array_key_exists('flyer_distribution', $details['what_changed']) == false ? $details['insertion_order']['flyer_distribution'] : $details['what_changed']['flyer_distribution'] !!} 
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>   
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr>
                            <td><strong>Start Date</strong></td>
                            <td><strong>End Date</strong></td>
                            <td><strong>Publication</strong></td>
                            <td><strong>No. of Insertions</strong></td>
                        </tr>
                        <tr>
                            <td colspan="4"><hr></td>
                        </tr>
                        <?php $i = 0; ?>
                        @while ($i <= 12)
                            @if ($i == 0)
                                <tr>
                                    <td>{!! array_key_exists('print_start_date_'.$i, $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['print_start_date_'.$i]) : $details['what_changed']['print_start_date_'.$i] !!}</td>
                                    <td>{!! array_key_exists('print_end_date_'.$i, $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['print_end_date_'.$i]) : $details['what_changed']['print_end_date_'.$i] !!}</td>
                                    <td>{!! array_key_exists('print_publication_'.$i, $details['what_changed']) == false ? $details['insertion_order']['print_publication_'.$i] : $details['what_changed']['print_publication_'.$i] !!}</td>
                                    <td>{!! array_key_exists('print_no_insertions_'.$i, $details['what_changed']) == false ? $details['insertion_order']['print_no_insertions_'.$i] : $details['what_changed']['print_no_insertions_'.$i] !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><hr></td>
                                </tr>    
                            @elseif ($details['insertion_order']['print_start_date_'.$i] != "")
                                <tr>
                                    <td>{!! array_key_exists('print_start_date_'.$i, $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['print_start_date_'.$i]) : $details['what_changed']['print_start_date_'.$i] !!}</td>
                                    <td>{!! array_key_exists('print_end_date_'.$i, $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['print_end_date_'.$i]) : $details['what_changed']['print_end_date_'.$i] !!}</td>
                                    <td>{!! array_key_exists('print_publication_'.$i, $details['what_changed']) == false ? $details['insertion_order']['print_publication_'.$i] : $details['what_changed']['print_publication_'.$i] !!}</td>
                                    <td>{!! array_key_exists('print_no_insertions_'.$i, $details['what_changed']) == false ? $details['insertion_order']['print_no_insertions_'.$i] : $details['what_changed']['print_no_insertions_'.$i] !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><hr></td>
                                </tr>    
                            @endif
                        
                        <?php $i++; ?>
                        @endwhile
                        <tr>
                            <td>&nbsp;</td>
                        </tr>   
                    </table>  
                </td>
            </tr>
        @elseif ($details['insertion_order']['order_type'] == "Classified") <!--Classifieds Order details -->
        <tr>
            <td colspan="2">
            Category: {!! array_key_exists('classified_category', $details['what_changed']) == false ? $details['insertion_order']['classified_category'] : $details['what_changed']['classified_category'] !!}
            <br><br>
            Classified Word Ad:<br>
            {!! array_key_exists('classified_word_ad', $details['what_changed']) == false ? $details['insertion_order']['classified_word_ad'] : $details['what_changed']['classified_word_ad'] !!}
            <br><br>
            Classified Word Count: {!! array_key_exists('classfied_word_count', $details['what_changed']) == false ? $details['insertion_order']['classified_word_count'] : $details['what_changed']['classified_word_count'] !!}
            <br><br>
            Payment Method: {!! array_key_exists('payment_method', $details['what_changed']) == false ? $details['insertion_order']['payment_method'] : $details['what_changed']['payment_method'] !!}
            <br><br>
            How Many Weeks: {!! array_key_exists('classified_number_of_weeks', $details['what_changed']) == false ? $details['insertion_order']['classified_number_of_weeks'] : $details['what_changed']['classified_number_of_weeks'] !!}
            <br> <br>    
            Start Date: {!! array_key_exists('classified_start_date', $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['classified_start_date']) : $details['what_changed']['classified_start_date'] !!}
            <br>
            End Date: {!! array_key_exists('classified_end_date', $details['what_changed']) == false ? Helper::cleanDate($details['insertion_order']['classified_end_date']) : $details['what_changed']['classified_end_date'] !!}
            <br><br>
            Extra Word Cost: {!! array_key_exists('classified_extra_word_cost', $details['what_changed']) == false ? $details['insertion_order']['classified_extra_word_cost'] : $details['what_changed']['classified_extra_word_cost'] !!}
            <br><br>
            Subtotal: {!! array_key_exists('subtotal', $details['what_changed']) == false ? $details['insertion_order']['subtotal'] : $details['what_changed']['subtotal'] !!}
            <br>
            GST (5%): {!! array_key_exists('gst', $details['what_changed']) == false ? $details['insertion_order']['gst'] : $details['what_changed']['gst'] !!}
            <br><Br>
            Total: {!! array_key_exists('total', $details['what_changed']) == false ? $details['insertion_order']['total'] : $details['what_changed']['total'] !!}
            </td>
           
        </tr>

        @endif
        <tr>
            <td>&nbsp;</td>
        </tr>   
        <tr>
            <td colspan="2"><strong>Special Instructions:</strong> 
            
                {!! array_key_exists('notes', $details['what_changed']) == false ? $details['insertion_order']['notes'] : $details['what_changed']['notes'] !!}
            </td>
        </tr>
    </table>
  
</body>
</html>