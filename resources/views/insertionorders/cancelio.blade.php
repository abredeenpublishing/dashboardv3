@extends('layouts.dashboard')

@section('content')
@section('title', 'Cancel Insertion Order :: Dashboard')
<div >
    <h1>Cancel Insertion Order</h1>    
    <p>&nbsp;</p>
</div> 

<div class="container">
    <div class="row">
        <div class="col">
                Insertion Order #: <strong>{{ $insertionorder->id }}</strong>
        </div>
        <div class="col">
                Client: <strong>{{ $insertionorder->client->client_name }}</strong>
        </div>
        <div class="col">
                DTI ID: 
                    <strong>{{ $insertionorder->client->dti_id }}</strong>
        </div>
    </div>
    <form action="/insertionorders/cancel/{{ $insertionorder->id }}" method="POST">
        <div class="form-row">&nbsp;</div>
        <div class="form-row">
            @csrf
            @method("PUT")
            <input type="hidden" name="cancel" value="1">
           
            <label for="order_number">Please enter reason for canceling IO:</label>
            <textarea rows="5" name="cancel_message" class="form-control {{ $errors->has('cancel_message') ? 'is-invalid' : '' }}"></textarea>
        </div>
            
            
       
        <div clss="form-row">&nbsp;</div>
        <div class="form-row">
            <button type="submit" class="btn btn-primary">Cancel IO</button> 
        </div>
    </form>



</div>
    




@stop

@section('js')
    <script> 

   console.log('Hi!'); </script>
  
@stop