 @extends('layouts.dashboard')

@section('content')

@section('title', 'Edit IO :: Dashboard')
    <div >
        <h1>Edit IO</h1>    
        <p>&nbsp;</p>
    </div> 
    
    <div class="container">
        
        @if (app('request')->input('clone')=="y")
        <form action="/insertionorders" method="POST">
            @csrf
        @else
        <form action="/insertionorders/{{ $insertionorder->id }}" method="POST">
            @csrf
            @method("PUT")
        @endif
            <input type="hidden" name="client_id" value="{{ old('client_id') == "" ? $insertionorder->client->id : old('client_id') }}">
            <input type="hidden" name="user_id" value="{{ $user->id  }}">
            <input type="hidden" name="sales_id" id="sales_id" value="{{ old('sales_id') == "" ? $insertionorder->sales_id : old('sales_id') }}">
            <input type="hidden" name="order_type" value="Print">
            
            <div class="form-row">
                <div class="col md-6">
                    <label>Client Information:</label>
                    <p>{{ $insertionorder->client->client_name }}<br>
                    {{ $insertionorder->client->address }}<br>
                    {{ $insertionorder->client->city }}, {{ $insertionorder->client->province }}<br>
                    {{ $insertionorder->client->postal_code }}</p>
                    <label>Billing Contact:</label>
                    <p>{{ $insertionorder->client->billing_contact_name }}<br> 
                        Email: {{ $insertionorder->client->billing_contact_email }}<br>
                        Tel: {{ $insertionorder->client->billing_contact_phone }}
                    </p>
                    <p><a href="/clients/{{ $insertionorder->client->id }}/edit" class="text-primary">Edit</a></p>
                </div>
                <div class="col md-6">
                    <label>Order Information:</label>
                    @if ((empty($insertionorder->sales_id)) || ($insertionorder->sales_id == "0" ))               
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-lg">Sales Rep:</span>
                                </div>
                            <select name="sales_rep" id="sales_rep" class='form-control form-control-sm' onchange="UpdateRepNumber()">
                                @foreach ($sales_reps as $rep)
                                    @if (old('sales_rep') == "")
                                        @if ($rep->id == $user->id)
                                            <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                        @else
                                            <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                        @endif
                                    @else
                                        @if ($rep->id == old('sales_rep'))
                                            <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                        @else
                                            <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                        @endif  

                                    @endif
                                    
                                @endforeach  
                                
                            </select>
                        </div>
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">Rep Number:</span>
                            </div>
                            <input  type="text" 
                                    class="form-control" 
                                    aria-label="Large" 
                                    aria-describedby="inputGroup-sizing-sm" 
                                    name="rep_number"
                                    id="rep_number"
                                    readonly 
                                    value="{{ old('rep_number') == "" ? $user->rep_number : old('rep_number') }}">
                        </div>
                    @else
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">Sales Rep:</span>
                            </div>
                            <select name="sales_rep" id="sales_rep" class='form-control form-control-sm' onchange="UpdateRepNumber()">
                                @foreach ($sales_reps as $rep)
                                    @if (old('sales_rep') == "")
                                        @if ($rep->id == $insertionorder->sales_id)
                                            <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                        @else
                                            <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                        @endif
                                    @else
                                        @if ($rep->id == old('sales_rep'))
                                            <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                        @else
                                            <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                        @endif  

                                    @endif
                                    
                                @endforeach  
                            
                            </select>
                        </div>    
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">Rep Number:</span>
                            </div>
                            <input  type="text" 
                                    class="form-control" 
                                    aria-label="Large" 
                                    aria-describedby="inputGroup-sizing-sm" 
                                    name="rep_number"
                                    id="rep_number"
                                    readonly 
                                    value="{{ old('rep_number') == "" ? Helper::getSalesRepNumber($insertionorder->sales_id) : old('rep_number') }}">
                        </div>
                   @endif
                  
                    <p>Email: {{ $user->email }}</p>
                    <label>Account / DTI ID:</label>
                    {{ $insertionorder->client->dti_id }}<br>
                    <label for="po_number">PO Number</label>
                    <input  type="text" 
                            class="form-control form-control-sm" 
                            name="po_number" 
                            value="{{ old('po_number') == "" ? $insertionorder->po_number : old('po_number') }}">
                </div>
            </div>
            <div class="form-row">
                <div class="col md-6"><!-- First Column of form -->
                    <label>Ad Specs:</label><br>
                    <label for="modular_size_yes_or_no">Modular Size / Supplement?</label>
                    <!-- if its first time load and the io in database has modular size then it needs to be on -->
                    @if ((count($errors) == 0) && (old('modular_size_yes_or_no') == "") && (($insertionorder->modular_size != "")))
                        <input   type="checkbox" 
                        class="form-check form-check-inline" 
                        name="modular_size_yes_or_no" id="modular_size_yes_or_no" 
                        value="Y"
                        checked  
                        onchange="ShowHideDiv('show-m', 'modular_size_yes_or_no');">
                        <div id="show-m" style="display:flex">
                    <!-- if user changes IO but errors occur and modular size is "Y" then show modular size -->
                    @elseif ((count($errors) > 0) && (old('modular_size_yes_or_no') == "Y")))
                        <input   type="checkbox" 
                        class="form-check form-check-inline" 
                        name="modular_size_yes_or_no" id="modular_size_yes_or_no" 
                        value="Y"
                        checked  
                        onchange="ShowHideDiv('show-m', 'modular_size_yes_or_no');">
                        <div id="show-m" style="display:flex">   
                    @else
                        <input   type="checkbox" 
                        class="form-check form-check-inline" 
                        name="modular_size_yes_or_no" id="modular_size_yes_or_no" 
                        value="Y" 
                        onchange="ShowHideDiv('show-m', 'modular_size_yes_or_no');">
                        <div id="show-m" style="display:none">
                    @endif   
                   
                        <label for="modular_size">Select Modular Size Ad</label>
                        <select name="modular_size" id="modular_size" class='form-control form-control-sm {{ $errors->has('modular_size') ? 'is-invalid' : '' }}'>
                            <option value="">Select One</option>
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "Full Page"))
                                <option value="Full Page" class="" selected>Full Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "Full Page"))
                                <option value="Full Page" class="" selected>Full Page</option>
                            @else
                                <option value="Full Page" class="">Full Page</option>
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "4/5 Page"))
                                <option value="4/5 Page" class="" selected>4/5 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "4/5 Page"))
                                <option value="4/5 Page" class="" selected>4/5 Page</option>
                            @else
                                <option value="4/5 Page" class="">4/5 Page</option>    
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "3/5 Page"))
                                <option value="3/5 Page" class="" selected>3/5 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "3/5 Page"))
                                <option value="3/5 Page" class="" selected>3/5 Page</option>
                            @else
                                <option value="3/5 Page" class="">3/5 Page</option> 
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "1/2 Page"))
                                <option value="1/2 Page" class="" selected>1/2 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "1/2 Page"))
                                <option value="1/2 Page" class="" selected>1/2 Page</option>
                            @else
                                <option value="1/2 Page" class="">1/2 Page</option>    
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "2/5 Page"))
                                <option value="2/5 Page" class="" selected>2/5 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "2/5 Page"))
                                <option value="2/5 Page" class="" selected>2/5 Page</option>
                            @else
                                <option value="2/5 Page" class="">2/5 Page</option>   
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "1/3 Page"))
                                <option value="1/3 Page" class="" selected>1/3 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "1/3 Page"))
                                <option value="1/3 Page" class="" selected>1/3 Page</option>
                            @else
                                <option value="1/3 Page" class="">1/3 Page</option> 
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "1/4 Page"))
                                <option value="1/4 Page" class="" selected>1/4 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "1/4 Page"))
                                <option value="1/4 Page" class="" selected>1/4 Page</option>
                            @else
                                <option value="1/4 Page" class="">1/4 Page</option>
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "1/5 Page"))
                                <option value="1/5 Page" class="" selected>1/5 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "1/5 Page"))
                                <option value="1/5 Page" class="" selected>1/5 Page</option>
                            @else
                                <option value="1/5 Page" class="">1/5 Page</option>
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "1/8 Page"))
                                <option value="1/8 Page" class="" selected>1/8 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "1/8 Page"))
                                <option value="1/8 Page" class="" selected>1/8 Page</option>
                            @else
                                <option value="1/8 Page" class="">1/8 Page</option>
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "1/10 Page"))
                                <option value="1/10 Page" class="" selected>1/10 Page</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "1/10 Page"))
                                <option value="1/10 Page" class="" selected>1/10 Page</option>
                            @else
                                <option value="1/10 Page" class="">1/10 Page</option>
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "Business Card"))
                                <option value="Business Card" class="" selected>Business Card</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "Business Card"))
                                <option value="Business Card" class="" selected>Business Card</option>
                            @else
                                <option value="Business Card" class="">Business Card</option> 
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "Banner 1"))
                                <option value="Banner 1" class="" selected>Banner 1</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "Banner 1"))
                                <option value="Banner 1" class="" selected>Banner 1</option>
                            @else
                                <option value="Banner 1" class="">Banner 1</option>
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "Banner 2"))
                                <option value="Banner 2" class="" selected>Banner 2</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "Banner 2"))
                                <option value="Banner 2" class="" selected>Banner 2</option>
                            @else
                                <option value="Banner 2" class="">Banner 2</option>   
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "2/3"))
                                <option value="2/3" class="" selected>2/3</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "2/3"))
                                <option value="2/3" class="" selected>2/3</option>
                            @else
                                <option value="2/3" class="">2/3</option>   
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "1/6"))
                                <option value="1/6" class="" selected>1/6</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "1/6"))
                                <option value="1/6" class="" selected>1/6</option>
                            @else
                                <option value="1/6" class="">1/6</option>   
                            @endif
                            @if ((count($errors) == 0) && ($insertionorder->modular_size == "1/12"))
                                <option value="1/12" class="" selected>1/12</option>
                            @elseif ((count($errors) > 0) && (old('modular_size') == "1/12"))
                                <option value="1/12" class="" selected>1/12</option>
                            @else
                                <option value="1/12" class="">1/12</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-row">
                        <!--spacer--> <p>&nbsp;</p>
                    </div>
                    
                    <?php
                       // need to calculate column inches as it is not stored in the database
                       $column_inches = 0;
                        if ($insertionorder->columns_wide != '"' ){
                            $column_inches = $insertionorder->columns_wide * $insertionorder->inches_high;
                            
                        }
                    ?>
                    <div class='form-row'>
                        <div class="col-md-6">
                            <div class="form-row">
                                <label>Column Inches: </label>&nbsp;
                                <span   id="span_column_inches" class="text-success fw-bold">
                                    {{ old('column_inches') == "" ? $column_inches : old('column_inches') }}
                                </span>
                            </div>

                            <div class="form-row">
                                <div class="col-md-5">
                                    <label for="columns_wide">Wide</label>
                                    <input  type="text" 
                                            class="form-control form-control-sm {{ $errors->has('columns_wide') ? 'is-invalid' : '' }}" 
                                            name="columns_wide" 
                                            id="wide" 
                                            value="{{ old('columns_wide') == "" ? $insertionorder->columns_wide : old('columns_wide') }}" 
                                            onchange="CalcColumnInches();">
                                </div> 
                                <div class="col-md-5">
                                    <label for="inches_high">High</label>
                                    <input  type="text" 
                                            class="form-control form-control-sm {{ $errors->has('inches_high') ? 'is-invalid' : '' }}" 
                                            name="inches_high" 
                                            id="high" 
                                            value="{{ old('inches_high') == "" ? $insertionorder->inches_high : old('inches_high') }}" 
                                            onchange="CalcColumnInches();">
                                </div>   
                            </div>
                            <div class="form-row">
                                <div class="col-md-2">
                                    <input  type="hidden" 
                                            name="column_inches" 
                                            id="column_inches" 
                                            value="{{ old('column_inches') == "" ? $column_inches : old('column_inches') }}" 
                                            class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <div class="form-row">
                                <!--spacer--> <p>&nbsp;</p>
                            </div>
                            <div class="form-row">
                                <div class="col-md-5">
                                    <label for="column_rate">Column Rate</label>
                                    <input  type="text"
                                            name="column_rate"
                                            id="column_rate"
                                            value="{{ old('column_rate') == "" ? $insertionorder->column_rate : old('column_rate') }}"
                                            class="form-control form-control-sm {{ $errors->has('column_rate') ? 'is-invalid' : '' }}"
                                            onchange="CalcSubtotal();">
                                </div>
                                <div class="col-md-5">
                                    <label for="colour_charge">Colour Charge</label>
                                    <input  type="text"
                                            name="colour_charge"
                                            id="colour_charge"
                                            value="{{ old('colour_charge') == "" ? $insertionorder->colour_charge : old('colour_charge') }}"
                                            class="form-control form-control-sm {{ $errors->has('color_change') ? 'is-invalid' : '' }}"
                                            onchange="CalcSubtotal();">
                                </div>
                            </div>
                            <div class="form-row" style="padding-top:10px;">
                                <div class="col-md-8 input-group-prepend">
                                    <span class="input-group-text">% Discount:</span>
                                    <input  type="text"
                                            name="print_discount"
                                            id="print_discount"
                                            value="{{ old('print_discount') == "" ? $insertionorder->print_discount : old('print_discount') }}"
                                            class="form-control {{ $errors->has('print_discount') ? 'is-invalid' : '' }}"
                                            onchange="CalcSubtotal();">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 bg-dark rounded p-2">
                            @if ((count($errors) == 0) && ($insertionorder->subtotal_overide != ""))
                                <div class="form-row" id="show_ors" style="display:flex;">
                            @elseif (old('override_subtotal_yes_or_no') == "Y" )
                                <div class="form-row" id="show_ors" style="display:flex;">
                            @else
                                <div class="form-row" id="show_ors" style="display:none;">
                            @endif
                            <div class="col-md-12">
                                    <label for="subtotal_overide">Override Subtotal</label>
                                    <input  type="text"
                                            name="subtotal_overide"
                                            id="override_subtotal"
                                            value="{{ old('subtotal_overide') == "" ? $insertionorder->subtotal_overide : old('subtotal_overide') }}"
                                            class="form-control form-control-sm {{ $errors->has('subtotal_overide') ? 'is-invalid' : '' }}"
                                            onchange="CalcSubtotal();">
                                </div>
                                
                            </div>
                            <div class="form-row">
                                    <div calss="col-md-12">
                                        <label for="override_subtotal_yes_or_no">Override Subtotal?</label>
                                        @if ((count($errors) == 0) && ($insertionorder->subtotal_overide != "") )
                                            <input  type="checkbox" 
                                            class="form-check form-check-inline" 
                                            name="override_subtotal_yes_or_no" 
                                            id="override_subtotal_yes_or_no" 
                                            value="Y" 
                                            checked 
                                            onchange="ShowHideDiv('show_ors', 'override_subtotal_yes_or_no');">
                                        @elseif (old('override_subtotal_yes_or_no') == "Y")
                                            <input  type="checkbox" 
                                                    class="form-check form-check-inline" 
                                                    name="override_subtotal_yes_or_no" 
                                                    id="override_subtotal_yes_or_no" 
                                                    value="Y" 
                                                    checked 
                                                    onchange="ShowHideDiv('show_ors', 'override_subtotal_yes_or_no');">

                                        @else
                                            <input  type="checkbox" 
                                                    class="form-check form-check-inline" 
                                                    name="override_subtotal_yes_or_no" 
                                                    id="override_subtotal_yes_or_no" 
                                                    value="Y" 
                                                    onchange="ShowHideDiv('show_ors', 'override_subtotal_yes_or_no');">
                                        @endif       
                                    </div>
                            </div>
                            <div class="form-row">
                                <label>Discount:</label>&nbsp;
                                <div class="col-md-3">
                                   <span id="span_discount" class="text-success fw-bold">{{ old('discount') == "" ? $insertionorder->print_discount : old('print_discount') }}</span>
                                    <input type="hidden" name="discount" id="input_discount" value="{{ old('discount') == "" ? $insertionorder->print_discount : old('print_discount') }}" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="form-row ">
                                <label>Subtotal:</label>
                                <div class="col-md-3">
                                    <span   id="span_sub_total" class="text-success fw-bold">
                                        {{ old('subtotal') == "" ? $insertionorder->subtotal : old('subtotal') }}
                                    </span>
                                    <input  type="hidden" 
                                            name="subtotal" 
                                            id="sub_total" 
                                            value="{{ old('subtotal') == "" ? $insertionorder->subtotal : old('subtotal') }}" 
                                            class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                
                                
                                </div>
                            </div>
                            <div class="form-row">
                                <label>GST (5%):</label>
                                <div class="col-md-2">
                                    <span id="span_gst" class="text-success">
                                        {{ old('gst') == "" ? $insertionorder->gst : old('gst') }}
                                    </span>
                                    <input  type="hidden" 
                                            name="gst" 
                                            id="gst" 
                                            value="{{ old('gst') == "" ? $insertionorder->gst : old('gst') }}" 
                                            class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <div class="form-row ">
                                <label>Total:</label>
                                <div class="col-md-2">
                                    <span id="span_total" class="text-success fw-bold fs-4">
                                        {{ old('total') == "" ? $insertionorder->total : old('total') }}
                                    </span>
                                    <input  type="hidden" 
                                            name="total" 
                                            id="total" 
                                            value="{{ old('total') == "" ? $insertionorder->total : old('total') }}" 
                                            class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
                <div class="col md-6"><!-- -start of second column in form-->
                   <label>Production Information:</label>
                    <div class="form-row">
                       <div class="col-md-3">
                            <div class="col-md-12">
                                <label for="color">Colour?</label>
                                <select     name="color" 
                                            class='form-control form-control-sm {{ $errors->has('color') ? 'is-invalid' : '' }}' 
                                            id="color">
                                    <option value="">Select One</option>
                                    @if ((count($errors) == 0) && ($insertionorder->color == "Yes"))
                                        <option value="Yes" class="" selected>Yes</option>
                                    @elseif (old('color') == "Yes")
                                        <option value="Yes" class="" selected>Yes</option>
                                    @else
                                        <option value="Yes" class="">Yes</option>
                                    @endif
                                    @if ((count($errors) == 0) && ($insertionorder->color == "No"))
                                        <option value="No" class="" selected>No</option>  
                                    @elseif (old('color') == "No")
                                        <option value="No" class="" selected>No</option>  
                                    @else
                                        <option value="No" class="">No</option>  
                                    @endif
                                </select>                               
                            </div>
                        </div>    
                        <div class="col-md-4">
                            <label for="feature">Feature</label>
                            <input  type="text" 
                                    name="feature" 
                                    value="{{ old('feature') == "" ? $insertionorder->feature : old('feature') }}" 
                                    class="form-control form-control-sm {{ $errors->has('feature') ? 'is-invalid' : '' }}">
                        </div>
                        <div class="col-md-4">
                            <label for="position_requested">Position?</label>
                            <input  type="text" 
                                    name="position_requested" 
                                    value="{{ old('position_requested') == "" ? $insertionorder->position_requested : old('position_requsted') }}" 
                                    class="form-control form-control-sm {{ $errors->has('position_requested') ? 'is-invalid' : '' }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <label>Copy:</label>
                                <div class="form-check">
                                    @if ((count($errors) == 0) && ($insertionorder->copy == "p/u"))
                                        <input class="form-check-input" type="checkbox" name="copy" checked value="p/u">
                                    @elseif (old('copy') == "p/u")
                                        <input class="form-check-input" type="checkbox" name="copy" checked value="p/u">
                                    @else
                                        <input class="form-check-input" type="checkbox" name="copy" value="p/u">
                                    @endif
                                    <label class="form-check-label" for="copy1">
                                    p/u
                                    </label>
                                </div>
                                <div class="form-check">
                                    @if ((count($errors) == 0) && ($insertionorder->copy2 == "p/u/w/c"))
                                        <input class="form-check-input" type="checkbox" name="copy2" checked value="p/u/w/c">
                                    @elseif (old('copy2') == "p/u/w/c")
                                        <input class="form-check-input" type="checkbox" name="copy2" checked value="p/u/w/c">
                                    @else
                                        <input class="form-check-input" type="checkbox" name="copy2" value="p/u/w/c">
                                    @endif     
                                    <label class="form-check-label" for="copy2">
                                    p/u/w/c
                                    </label>
                                </div>
                                <div class="form-check">
                                    @if ((count($errors) == 0) && ($insertionorder->copy3 == "new"))
                                        <input class="form-check-input" type="checkbox" name="copy3" checked value="new">
                                    @elseif (old('copy3') == "new")
                                        <input class="form-check-input" type="checkbox" name="copy3" checked value="new">
                                    @else
                                        <input class="form-check-input" type="checkbox" name="copy3" value="new"> 
                                    @endif
                                    <label class="form-check-label" for="copy3">
                                    new
                                    </label>
                                </div>
                                <div class="form-check">
                                    @if ((count($errors) == 0) && ($insertionorder->copy4 == "c/r"))
                                        <input class="form-check-input" type="checkbox" name="copy4" checked value="c/r">
                                    @elseif (old('copy4') == "c/r")
                                        <input class="form-check-input" type="checkbox" name="copy4" checked value="c/r">
                                    @else
                                        <input class="form-check-input" type="checkbox" name="copy4" value="c/r">
                                    @endif
                                    <label class="form-check-label" for="copy4">
                                    c/r
                                    </label>
                                </div>
                                <div class="form-check">
                                    @if ((count($errors) == 0) && ($insertionorder->copy5 == "same until further notice"))
                                        <input class="form-check-input" type="checkbox" name="copy5" checked value="same until further notice">
                                    @elseif (old('copy5') == "same until further notice")
                                        <input class="form-check-input" type="checkbox" name="copy5" checked value="same until further notice">
                                    @else
                                        <input class="form-check-input" type="checkbox" name="copy5" value="same until further notice"> 
                                    @endif
                                    <label class="form-check-label" for="copy5">
                                    same until further notice
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <label for="caption_line">Caption Line</label>
                                 <input     type="text" 
                                            name="caption_line" 
                                            value="{{ old('caption_line') == "" ? $insertionorder->caption_line : old('caption_line') }}" 
                                            class="form-control form-control-sm {{ $errors->has('caption_line') ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <label>Method of Payment</label><br>
                    @if ((count($errors) == 0) && ($insertionorder->payment_method == "Invoice"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" checked value="Invoice">
                            <label class="form-check-label" for="inlineRadio1">Invoice</label>
                        </div>
                    @elseif (old('payment_method')== "Invoice")
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" checked value="Invoice">
                            <label class="form-check-label" for="inlineRadio1">Invoice</label>
                        </div>
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  value="Invoice">
                            <label class="form-check-label" for="inlineRadio1">Invoice</label>
                        </div>    
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->payment_method == "Mastercard"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" checked value="Mastercard">
                            <label class="form-check-label" for="inlineRadio2">Mastercard</label>
                        </div> 
                    @elseif (old('payment_method') == 'Mastercard')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" checked value="Mastercard">
                            <label class="form-check-label" for="inlineRadio2">Mastercard</label>
                        </div> 
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" value="Mastercard">
                            <label class="form-check-label" for="inlineRadio2">Mastercard</label>
                        </div>
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->payment_method == "Visa"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  checked value="Visa">
                            <label class="form-check-label" for="inlineRadio3">Visa</label>
                        </div> 
                    @elseif (old('payment_method') == 'Visa')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  checked value="Visa">
                            <label class="form-check-label" for="inlineRadio3">Visa</label>
                        </div> 
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  value="Visa">
                            <label class="form-check-label" for="inlineRadio3">Visa</label>
                        </div> 
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->payment_method == "American Express"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  checked value="American Express">
                            <label class="form-check-label" for="inlineRadio3">American Express</label>
                        </div> 
                    @elseif (old('payment_method') == 'American Express')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  checked value="American Express">
                            <label class="form-check-label" for="inlineRadio3">American Express</label>
                        </div> 
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  value="American Express">
                            <label class="form-check-label" for="inlineRadio3">American Express</label>
                        </div> 
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->payment_method == "Cash"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" checked value="Cash">
                            <label class="form-check-label" for="inlineRadio3">Cash</label>
                        </div> 
                    @elseif (old('payment_method') == 'Cash')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" checked value="Cash">
                            <label class="form-check-label" for="inlineRadio3">Cash</label>
                        </div> 
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  value="Cash">
                            <label class="form-check-label" for="inlineRadio3">Cash</label>
                        </div> 
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->payment_method == "Debit"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  checked value="Debit">
                            <label class="form-check-label" for="inlineRadio3">Debit</label>
                        </div>  
                    @elseif (old('payment_method') == 'Debit')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  checked value="Debit">
                            <label class="form-check-label" for="inlineRadio3">Debit</label>
                        </div>  
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  value="Debit">
                            <label class="form-check-label" for="inlineRadio3">Debit</label>
                        </div>  
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->payment_method == "Cheque"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" checked value="Cheque">
                            <label class="form-check-label" for="inlineRadio3">Cheque</label>
                        </div> 
                    @elseif (old('payment_method') == 'Cheque')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method" checked value="Cheque">
                            <label class="form-check-label" for="inlineRadio3">Cheque</label>
                        </div> 
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payment_method"  value="Cheque">
                            <label class="form-check-label" for="inlineRadio3">Cheque</label>
                        </div> 
                    @endif
                    @if ($errors->has('payment_method'))
                    <div class="text-danger">
                        {{ $errors->first('payment_method') }}
                      </div>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <div class="form-row">
                <div class="col-md-6">
                    <label>Ad Type</label><br>
                    @if ((count($errors) == 0) && ($insertionorder->ad_type == "Display"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type"  checked value="Display">
                            <label class="form-check-label" for="inlineRadio1">Display</label>
                        </div>    
                    @elseif (old('ad_type') == 'Display')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type"  checked value="Display">
                            <label class="form-check-label" for="inlineRadio1">Display</label>
                        </div>    
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type"  value="Display">
                            <label class="form-check-label" for="inlineRadio1">Display</label>
                        </div>
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->ad_type == "Classified Display"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type" checked value="Classified Display">
                            <label class="form-check-label" for="inlineRadio2">Classified Display</label>
                        </div>
                    @elseif (old('ad_type') == 'Classified Display')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type" checked value="Classified Display">
                            <label class="form-check-label" for="inlineRadio2">Classified Display</label>
                        </div>
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type" value="Classified Display">
                            <label class="form-check-label" for="inlineRadio2">Classified Display</label>
                        </div>
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->ad_type == "National"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type" checked value="National">
                            <label class="form-check-label" for="inlineRadio3">National</label>
                        </div>  
                    @elseif (old('ad_type') == 'National')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type" checked value="National">
                            <label class="form-check-label" for="inlineRadio3">National</label>
                        </div>  
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type"  value="National">
                            <label class="form-check-label" for="inlineRadio3">National</label>
                        </div> 
                    @endif
                    @if ((count($errors) == 0) && ($insertionorder->ad_type == "National Classified"))
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type"  checked value="National Classified">
                            <label class="form-check-label" for="inlineRadio3">National Classified</label>
                        </div>    
                    @elseif (old('ad_type') == 'National Calssified')
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type"  checked value="National Classified">
                            <label class="form-check-label" for="inlineRadio3">National Classified</label>
                        </div>    
                    @else
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="ad_type"  value="National Classified">
                            <label class="form-check-label" for="inlineRadio3">National Classified</label>
                        </div> 
                    @endif
                    @if ($errors->has('ad_type'))
                    <div class="text-danger">
                        {{ $errors->first('ad_type') }}
                      </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <label>TFN</label><br>
                    <div class="form-check form-check-inline">
                        @if ((count($errors) == 0) && ($insertionorder->tfn == "1"))
                            <input class="form-check-input" type="checkbox" name="tfn" checked value="Yes">
                        @elseif (old('tfn') == "Yes")
                            <input class="form-check-input" type="checkbox" name="tfn" checked value="Yes">
                        @else
                            <input class="form-check-input" type="checkbox" name="tfn" value="Yes">
                        @endif
                        <label class="form-check-label" for="inlineCheckbox1">Yes</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <label for="print_frequency_0">Frequency</label>
                    <select name="print_frequency_0" class='form-control form-control-sm {{ $errors->has('print_frequency_0') ? 'is-invalid' : '' }}' id="print_frequency_0" onchange="insertion('0');">
                        <option value="">Select One</option>
                        @if ((count($errors) == 0) && ($insertionorder->print_frequency_0 == "1"))
                            <option value="1" class="" selected>Once a Week</option>
                        @elseif (old('print_frequency_0') == "1")
                            <option value="1" class="" selected>Once a Week</option>
                        @else
                            <option value="1" class="">Once a Week</option>
                        @endif
                        @if ((count($errors) == 0) && ($insertionorder->print_frequency_0 == "2"))
                            <option value="2" class="" selected>Every Other Week</option>  
                        @elseif (old('print_frequency_0') == "2")
                            <option value="2" class="" selected>Every Other Week</option>  
                        @else
                            <option value="2" class="">Every Other Week</option>  
                        @endif
                        @if ((count($errors) == 0) && ($insertionorder->print_frequency_0 == "3"))
                            <option value="3" class="" selected>Every 3rd Week of the Month</option>
                        @elseif (old('print_frequency_0') == "3")
                            <option value="3" class="" selected>Every 3rd Week of the Month</option>
                        @else
                            <option value="3" class="">Every 3rd Week of the Month</option>
                        @endif
                        @if ((count($errors) == 0) && ($insertionorder->print_frequency_0 == "4"))
                            <option value="4" class="" selected>Once a Month</option>  
                        @elseif (old('print_frequency_0') == "4")
                            <option value="4" class="" selected>Once a Month</option>  
                        @else
                            <option value="4" class="">Once a Month</option>  
                        @endif       
                    </select>
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>       
           <?php 
                $i = 0;     
                // needed to covert print details into an array so that I could referce each value with $i
                $ad_details = [
                    'print_start_date_0' => $insertionorder->print_start_date_0,
                    'print_end_date_0' => $insertionorder->print_end_date_0,
                    'print_publication_0' => $insertionorder->print_publication_0,
                    'print_no_insertions_0' => $insertionorder->print_no_insertions_0,
                    'print_start_date_1' => $insertionorder->print_start_date_1,
                    'print_end_date_1' => $insertionorder->print_end_date_1,
                    'print_publication_1' => $insertionorder->print_publication_1,
                    'print_no_insertions_1' => $insertionorder->print_no_insertions_1,
                    'print_start_date_2' => $insertionorder->print_start_date_2,
                    'print_end_date_2' => $insertionorder->print_end_date_2,
                    'print_publication_2' => $insertionorder->print_publication_2,
                    'print_no_insertions_2' => $insertionorder->print_no_insertions_2,
                    'print_start_date_3' => $insertionorder->print_start_date_3,
                    'print_end_date_3' => $insertionorder->print_end_date_3,
                    'print_publication_3' => $insertionorder->print_publication_3,
                    'print_no_insertions_3' => $insertionorder->print_no_insertions_3,
                    'print_start_date_4' => $insertionorder->print_start_date_4,
                    'print_end_date_4' => $insertionorder->print_end_date_4,
                    'print_publication_4' => $insertionorder->print_publication_4,
                    'print_no_insertions_4' => $insertionorder->print_no_insertions_4,
                    'print_start_date_5' => $insertionorder->print_start_date_5,
                    'print_end_date_5' => $insertionorder->print_end_date_5,
                    'print_publication_5' => $insertionorder->print_publication_5,
                    'print_no_insertions_5' => $insertionorder->print_no_insertions_5,
                    'print_start_date_6' => $insertionorder->print_start_date_6,
                    'print_end_date_6' => $insertionorder->print_end_date_6,
                    'print_publication_6' => $insertionorder->print_publication_6,
                    'print_no_insertions_6' => $insertionorder->print_no_insertions_6,
                    'print_start_date_7' => $insertionorder->print_start_date_7,
                    'print_end_date_7' => $insertionorder->print_end_date_7,
                    'print_publication_7' => $insertionorder->print_publication_7,
                    'print_no_insertions_7' => $insertionorder->print_no_insertions_7,
                    'print_start_date_8' => $insertionorder->print_start_date_8,
                    'print_end_date_8' => $insertionorder->print_end_date_8,
                    'print_publication_8' => $insertionorder->print_publication_8,
                    'print_no_insertions_8' => $insertionorder->print_no_insertions_8,
                    'print_start_date_9' => $insertionorder->print_start_date_9,
                    'print_end_date_9' => $insertionorder->print_end_date_9,
                    'print_publication_9' => $insertionorder->print_publicaition_9,
                    'print_no_insertions_9' => $insertionorder->print_no_insertions_9,
                    'print_start_date_10' => $insertionorder->print_start_date_10,
                    'print_end_date_10' => $insertionorder->print_end_date_10,
                    'print_publication_10' => $insertionorder->print_publication_10,
                    'print_no_insertions_10' => $insertionorder->print_no_insertions_10,
                    'print_start_date_11' => $insertionorder->print_start_date_11,
                    'print_end_date_11' => $insertionorder->print_end_date_11,
                    'print_publication_11' => $insertionorder->print_publication_11,
                    'print_no_insertions_11' => $insertionorder->print_no_insertions_11,
                    'print_start_date_12' => $insertionorder->print_start_date_12,
                    'print_end_date_12' => $insertionorder->print_end_date_12,
                    'print_publication_12' => $insertionorder->print_publication_12,
                    'print_no_insertions_12' => $insertionorder->print_no_insertions_12
                ];
               //dd($ad_details);
           ?>
            @while ($i <= 12)   
                @if ($i == 0)
                    <div class="form-row" id="add_row_{{ $i }}" >
                @else
                    @if ((old('print_start_date_'.$i) != "") || ((count($errors) == 0) && ($ad_details['print_start_date_'.$i] != "")))
                        <div class="form-row" id="add_row_{{ $i }}" style="display:flex">
                    @else
                        <div class="form-row" id="add_row_{{ $i }}" style="display:none">    
                    @endif
                @endif
                    <div class="col-md-3">
                        <label for="print_start_date_{{ $i }}">Start Date</label><br>
                        <input  type="date" 
                                name="print_start_date_{{ $i }}" 
                                id="print_start_date_{{ $i }}" 
                                value="{{ old('print_start_date_'.$i) == "" ? $ad_details['print_start_date_'.$i] : old('print_start_date_'.$i) }}" 
                                class="form-control form-control-sm {{ $errors->has('print_start_date_'.$i) ? 'is-invalid' : '' }}" 
                                onchange="insertion('{{ $i }}');">
                    </div>
                    <div class="col-md-3">
                        <label for="print_end_date_{{ $i }}">End Date</label><br>
                        <input  type="date" 
                                name="print_end_date_{{ $i }}" 
                                id="print_end_date_{{ $i }}" 
                                value="{{ old('print_end_date_'.$i) == "" ? $ad_details['print_end_date_'.$i] : old('print_end_date_'.$i) }}" 
                                class="form-control form-control-sm {{ $errors->has('print_end_date_'.$i) ? 'is-invalid' : '' }}" 
                                onchange="insertion('{{  $i }}');">
                    </div>
                    <div class="col-md-3">
                        <label for="print_publication_{{ $i }}">Publication</label>
                        <select     name="print_publication_{{ $i }}" 
                                    class='form-control form-control-sm {{ $errors->has('print_publication_'.$i) ? 'is-invalid' : '' }}'>
                            <option value="">Select One</option>
                            @if (count($errors) > 0) <!-- Then User has hit submit and I need to retrieve submitted data instead-->
                                @if (old('print_publication_'.$i ) == "Columbia Valley Pioneer")
                                    <option value="Columbia Valley Pioneer" class="" selected>Columbia Valley Pioneer</option>
                                @else
                                    <option value="Columbia Valley Pioneer" class="">Columbia Valley Pioneer</option>
                                @endif
                                @if (old('print_publication_'.$i) == "Fitzhugh")
                                    <option value="Fitzhugh" class="" selected>Fitzhugh</option>    
                                @else
                                    <option value="Fitzhugh" class="">Fitzhugh</option>
                                @endif
                                @if (old('print_publication_'.$i) == "Kamloops This Week")
                                    <option value="Kamloops This Week" class="" selected>Kamloops This Week</option>
                                @else
                                    <option value="Kamloops This Week" class="">Kamloops This Week</option>
                                @endif
                                @if (old('print_publication_'.$i) == "Merritt Herald")
                                    <option value="Merritt Herald" class="" selected>Merritt Herald</option>
                                @else
                                    <option value="Merritt Herald" class="">Merritt Herald</option>   
                                @endif
                                @if (old('print_publication_'.$i) == "Peachland View")
                                    <option value="Peachland View" class="" selected>Peachland View</option>
                                @else
                                    <option value="Peachland View" class="">Peachland View</option>  
                                @endif
                                @if (old('print_publication_'.$i) == "Times Chronicle")
                                    <option value="Times Chronicle" class="" selected>Times Chronicle</option>
                                @else
                                    <option value="Times Chronicle" class="">Times Chronicle</option>
                                @endif
                            @else
                                @if ($ad_details['print_publication_'.$i] == "Columbia Valley Pioneer")
                                    <option value="Columbia Valley Pioneer" class="" selected> Columbia Valley Pioneer</option>
                                @else
                                    <option value="Columbia Valley Pioneer" class="">Columbia Valley Pioneer</option>
                                @endif
                                @if ($ad_details['print_publication_'.$i] == "Fitzhugh")
                                    <option value="Fitzhugh" class="" selected>Fitzhugh</option>    
                                @else
                                    <option value="Fitzhugh" class="">Fitzhugh</option>
                                @endif
                                @if ($ad_details['print_publication_'.$i] == "Kamloops This Week")
                                    <option value="Kamloops This Week" class="" selected>Kamloops This Week</option>
                                @else
                                    <option value="Kamloops This Week" class="">Kamloops This Week</option>
                                @endif
                                @if ($ad_details['print_publication_'.$i] == "Merritt Herald")
                                    <option value="Merritt Herald" class="" selected>Merritt Herald</option>
                                @else
                                    <option value="Merritt Herald" class="">Merritt Herald</option>   
                                @endif
                                @if ($ad_details['print_publication_'.$i] == "Peachland View")
                                    <option value="Peachland View" class="" selected>Peachland View</option>
                                @else
                                    <option value="Peachland View" class="">Peachland View</option>  
                                @endif
                                @if ($ad_details['print_publication_'.$i] == "Times Chronicle")
                                    <option value="Times Chronicle" class="" selected>Times Chronicle</option>
                                @else
                                    <option value="Times Chronicle" class="">Times Chronicle</option>
                                @endif
                            @endif
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="print_no_insertions_{{ $i }}">No. Insertions</label><br>
                        <input  type="text" 
                                name="print_no_insertions_{{ $i }}" 
                                id="print_no_insertions_{{ $i }}"  
                                value="{{ old('print_no_insertions_'.$i) == "" ? $ad_details['print_no_insertions_'.$i] : old('print_no_insertions_'.$i) }}"
                                class="form-control form-control-sm {{ $errors->has('print_no_insertions_'.$i) ? 'is-invalid' : '' }}">
                    </div>
                    <div class="col-md-1">
                        @if ($i == 0)
                            &nbsp;
                        @else
                            <a href="javascript:" class="text-danger v-bottom" onclick="remove_row(this,{{ $i }});">(X) Remove</a>
                        @endif
                    </div>
                    <div class="form-row" id="row_{{ $i }}" syle="display:flex">
                        <div class="col-md-12">
                            @if ($i == 12)
                                &nbsp;
                            @else
                                <a href="javascript:;" class="text-primary" onclick="show_row(this,{{ $i+1 }});">(+) Add Row</a>
                            @endif     
                        </div>
                    </div>
                    
                </div> <!-- closes add_row_$i -->
                <div class="form-row spacer"></div> 
                
                <?php $i++  ?>
            @endwhile
           <div class="form-row">
                <div class="col-md-12">
                    <label for="notes">Other Notes / Special Instructions:</label>
                    <textarea name="notes" rows="5"  class="form-control" >{{ old('notes') == "" ? $insertionorder->notes : old('notes') }}</textarea>
                </div>
           </div>
            <div class="form-row">
            <!--spacer--> <p>&nbsp;</p>
             </div>
            <div class="form-row">
                @if (app('request')->input('clone')=="y")
                    <button type="submit" class="btn btn-primary">Create IO</button>
                @else
                    <button type="submit" class="btn btn-primary">Edit IO</button>
                @endif
            </div>
        </form>
    </div>
   <p>&nbsp;</p><p>&nbsp;</p>
   <p>&nbsp;</p><p>&nbsp;</p>
@stop



@section('js')

<script type="text/javascript">
    function ShowHideDiv(field_id_to_change, property_value) {
        var change_property = document.getElementById(field_id_to_change);
        var incoming_value = document.getElementById(property_value);
        change_property.style.display = incoming_value.checked == true ? "flex" : "none";
    // console.log(incoming_value.checked);
    }
    function CalcColumnInches(){
        var wide = document.getElementById('wide');
        var high = document.getElementById('high');
        var spancolumninches = document.getElementById('span_column_inches');
        var inputcolumninches = document.getElementById('column_inches');
        var inches = parseFloat(wide.value) * parseFloat(high.value);
        spancolumninches.innerText = inches.toFixed(2);
        inputcolumninches.value = inches.toFixed(2);
        
        if (document.getElementById('column_rate').value != "") {
            CalcSubtotal();
        }

    }
    function CalcSubtotal(){
        var columnrate = document.getElementById('column_rate');
        var colorcharge = document.getElementById('colour_charge');
        var spansubtotal = document.getElementById('span_sub_total');
        var inputsubtotal = document.getElementById('sub_total');
        var columninches = document.getElementById('column_inches');
        var spangst = document.getElementById('span_gst');
        var inputgst = document.getElementById("gst");
        var spantotal = document.getElementById('span_total');
        var inputtotal = document.getElementById("total");
        var overridesubtotal = document.getElementById('override_subtotal'); 
        var spandiscount = document.getElementById('span_discount');
        var inputdiscount = document.getElementById('input_discount');
        var print_discount = document.getElementById('print_discount').value;
        var calc_discount = 0;
        if (isNaN(parseFloat(colorcharge.value))) {
            colorcharge.value = 0;
        }
       
        var subtotal = (parseFloat(columnrate.value) * parseFloat(columninches.value))+parseFloat(colorcharge.value);
         // find discount value if there is one
        if (!isNaN(parseFloat(print_discount))){
            calc_discount = parseFloat(print_discount)/100;
            discount_amount = subtotal*calc_discount;
            spandiscount.innerText = discount_amount.toFixed(2);
            inputdiscount.value = discount_amount.toFixed(2);
            subtotal = subtotal - (subtotal*calc_discount);
        } else {
            spandiscount.innerText = 0;
            inputdiscount.value =  0;
        }
        var gst = 0;
        var total = 0;
        if (!isNaN(parseFloat(overridesubtotal.value))) {
            oldsubtotal = subtotal;
            spansubtotal.innerText = overridesubtotal.value;
            inputsubtotal.value = overridesubtotal.value; 
            subtotal = parseFloat(overridesubtotal.value);
            spandiscount.innerText = 0;
            inputdiscount.value =  0;
            if (oldsubtotal > subtotal) {
                //spandiscount.innerText = parseFloat(oldsubtotal) - parseFloat(subtotal);
                //inputdiscount.value =  parseFloat(oldsubtotal) - parseFloat(subtotal);
            } else {
                //spandiscount.innerText = 0;
                //inputdiscount.value =  0;
            }

        }
        spansubtotal.innerText = subtotal.toFixed(2);
        inputsubtotal.value = subtotal.toFixed(2);
        gst = subtotal * 0.05;
        spangst.innerText = gst.toFixed(2);
        inputgst.value = gst.toFixed(2);
        total = subtotal+gst;
        spantotal.innerText = total.toFixed(2);
        inputtotal.value = total.toFixed(2);

        
        console.log(subtotal);

    }
    function insertion(number){
        var start_date = 'print_start_date_'+number;
        var end_date = 'print_end_date_'+number;
        var change_field = "print_no_insertions_"+number;
        var date1 = new Date (document.getElementById(start_date).value);
        var date2 = new Date (document.getElementById(end_date).value);
        var print_frequency_0 = parseInt(document.getElementById('print_frequency_0').value);
        var no_insertions = 0;

        if ((date1 != null) && (date2 != null) && (print_frequency_0 != null)) {
            // To calculate the time difference of two dates
            var Difference_In_Time = date2.getTime() - date1.getTime();
            // To calculate the no. of days between two dates
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            print_frequency_0 = print_frequency_0 * 7;
            
            if (Difference_In_Time <= 0) {
                no_insertions = 1;
            } else {
                no_insertions = Math.round(Difference_In_Days / print_frequency_0);
                no_insertions = no_insertions + 1;
            }

            document.getElementById(change_field).value = no_insertions;
        }

    }
   
    function show_row(placeholder, number) {
        $.ajax({
            url: $(placeholder).attr('rel'),
            type: "GET",
            success:function(){
               // alert("done");
                var row = number;
                var prev = parseInt(number) - 1;
                var make_visible = "add_row_" + row;
                var hide_prev_btn = "row_" + prev;
                console.log (make_visible);

                document.getElementById(make_visible).style.display = "flex";
                document.getElementById(hide_prev_btn).style.display = "none";
            },
            error:function (){
                alert("testing error");
            }
        });
        return false;
    }
    function remove_row(placeholder, number) {
        $.ajax({
            url: $(placeholder).attr('rel'),
            type: "GET",
            success:function(){
               // alert("done");
                var row = number;
                var prev = parseInt(number) - 1;
                var make_visible = "row_" + prev;
                var hide_prev_btn = "add_row_" + row;   
                var print_start_date_clear = "print_start_date_" + row;
                var print_end_date_clear = "print_end_date_" + row;
                var print_no_insertions_clear = "print_no_insertions_" + row;
                // clear out data if there is any if they are removing the row
                document.getElementById(print_start_date_clear).value = "";
                document.getElementById(print_end_date_clear).value = "";
                document.getElementById(print_no_insertions_clear).value = "";

                document.getElementById(make_visible).style.display = "flex";
                document.getElementById(hide_prev_btn).style.display = "none";
            },
            error:function (){
                alert("testing error");
            }
        });
        return false;
    }      
   
    function UpdateRepNumber() {
        // sales_rep contains: "userid, repnumber"
        var sales_rep = document.getElementById('sales_rep').value;
        var values = sales_rep.split(',');
        var user_id = values[0];
        var rep_number = values[1];

        document.getElementById('rep_number').value = rep_number.trim();
        document.getElementById('sales_id').value = user_id.trim();
    }
   
  
</script>
    <script> console.log('Hi!'); </script>
   
@stop