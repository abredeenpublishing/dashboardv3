@extends('layouts.dashboard')

@section('content')
@section('title', 'Insertion Orders :: Dashboard')
@section('plugins.Datatables', true)
<div class="float-left">
    <h1>Insertion Orders</h1>    
    <p>&nbsp;</p>
</div> 
<div class="float-right">
    @if (!Gate::allows('front-only', auth()->user()))
    <a href="/insertionorders/create"><button type="button" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Create IO</button></a>
    @endif
</div>   
<div style="clear:both"></div>
<div class="card">
    <div class="card-body">
        <div class="card-title">
            Advance Searching
        </div>
        <form method="GET" id="search-form">
            <div class="row">
                <div class="col-lg-3">
                    <input type="text" class="form-control adv-search-input" placeholder="Client Name" name="client_name" value="{{ (isset($_GET['client_name']) && $_GET['client_name'] != '')?$_GET['client_name']:'' }}">
                </div>
                <div class="col-lg-3">
                    <input type="text" class="form-control adv-search-input" placeholder="DTI ID" name="dti_id" value="{{ (isset($_GET['dti_id']) && $_GET['dti_id'] != '')?$_GET['dti_id']:'' }}">
                </div>
                <div class="col-lg-12">
                    <hr>
                    <button class="btn btn-success" type="submit" id="extraSearch">Search</button>
                    <a class="btn btn-danger" href="/insertionorders">Reset</a>
                </div>
            </div>
        </form>
    </div>
</div>
    
   <table id="io-table" class="table table-condensed dataTable no-footer" role="grid" aria-describedby="io-table_info">
       <thead>
           <tr role="row">
               <th>ID</th>
               <th>Client Name</th>
               <th>Order Number</th>
               <th>Order Type</th>
               <th>Order Total</th>
               <th>Start Date</th>
               <th>End Date</th>
               <th>Action</th>

           </tr>
       </thead>
       <tbody>
           
       </tbody>
   </table>

   <p>&nbsp;</p><p>&nbsp;</p>
   <p>&nbsp;</p><p>&nbsp;</p>
@stop

@section('js')
    <script> 
    jQuery(function($) {
       //initiate dataTables plugin
       var myTable = 
       $('#io-table').DataTable( {
           processing: true,
           serverSide: true,
           bFilter: false,
           responsive: true,
           "order": [[ 0, "desc" ]],
           ajax: {
                url: '{{ route('insertionorders.getdata') }}',
                    data: function (d) {
                    d.client_name = $('input[name=client_name]').val();
                    d.dti_id = $('input[name=dti_id]').val();
                }
            },
            "createdRow": function( row, data, dataIndex){
                if( data['cancel'] ==  `1`){
                    $(row).addClass('cancel');
                }
           },
           columns: [
            {data: 'id', name: 'id',
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if(oData.client_name) {
                        $(nTd).html("<a href='/insertionorders/"+oData.id+"'>"+oData.id+"</a>");
                    }
                }
            },
            {data: 'client_name', name: 'Client Name',
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if(oData.client_name) {
                        $(nTd).html("<a href='/clients/"+oData.client_id+"' class='text-info'>"+oData.client_name+"</a>");
                    }
                }
            },
            {data: 'order_number', name: 'Order Number'},
            {data: 'io_type', name: 'Orde Type',
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if(oData.client_name) {
                        $(nTd).html("<a href='/insertionorders/"+oData.id+"'>"+oData.io_type+"</a>");
                    }
                }
            },
            {data: 'total',
                render: $.fn.dataTable.render.number( ',', '.', 2 )},
            {data: 'start_date', name: 'Start Date'},
            {data: 'end_date', name: 'End Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false, width: '15%'},
           
           ],
       
       
        });
    });
   
   console.log('Hi!'); </script>
  
@stop