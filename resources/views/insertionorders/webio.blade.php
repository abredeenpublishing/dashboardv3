@extends('layouts.dashboard')

@section('content')

@section('title', 'Create Web IO :: Dashboard')
    <div >
        <h1>Create Web IO</h1>    
        <p>&nbsp;</p>
    </div> 
    
    <div class="container">
        
        <form action="/insertionorders" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="client_id" value="{{ $client->id }}">
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <input type="hidden" name="sales_id" id="sales_id" value="{{ old('sales_id') }}">
            <input type="hidden" name="order_type" value="Web">
            <input type="hidden" name="insertion_date" value="{{ now() }}">
            <div class="form-row">
                <div class="col md-6">
                    <label>Client Information:</label>
                    <p>{{ $client->client_name }}<br>
                    {{ $client->address }}<br>
                    {{ $client->city }}, {{ $client->province }}<br>
                    {{ $client->postal_code }}</p>
                    <label>Billing Contact:</label>
                    <p>{{ $client->billing_contact_name }}<br> 
                        Email: {{ $client->billing_contact_email }}<br>
                        Tel: {{ $client->billing_contact_phone }}
                    </p>
                    <p><a href="/clients/{{ $client->id }}/edit" class="text-primary">Edit</a></p>
                </div>
                <div class="col md-6">
                    <label>Order Information:</label>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">Sales Rep:</span>
                            </div>
                           <select name="sales_rep" id="sales_rep" class='form-control form-control-sm' onchange="UpdateRepNumber()">
                            @foreach ($sales_reps as $rep)
                                @if (old('sales_rep') == "")
                                    @if ($rep->id == $user->id)
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                    @else
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                    @endif
                                @else
                                    @if ($rep->id == old('sales_rep'))
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                    @else
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                    @endif  

                                @endif
                                
                            @endforeach  
                            
                           </select>
                    </div>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Rep Number:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="rep_number"
                                id="rep_number"
                                readonly 
                                value="{{ old('rep_number') == "" ? $user->rep_number : old('rep_number') }}">
                    </div>
                  
                    <p>Email: {{ $user->email }}</p>
                    <label>Account / DTI ID:</label>
                    {{ $client->dti_id }}<br>
                    <label for="po_number">PO Number</label>
                    <input type="text" class="form-control form-control-sm" name="po_number" value="{{ old('po_number') }}">
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> 
                <p>&nbsp;</p>
             </div>
            <div class="form-row">
                <div class="col">
                    <label for="web_url">Website URL:</label>
                    <input  type="text" 
                            name="web_url" 
                            value="{{ old('web_url') }}" 
                            placeholder="https://www.thesiteurl.com"
                            class="form-control form-control-sm {{ $errors->has('web_url') ? 'is-invalid' : '' }}">                    
                    <small class="form-text text-muted">This is the URL in which the webad links to</small>
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
                 </div>
            <div class="form-row">
                <div class="col">
                    <label for="web_start_date">Start Date:</label>
                    <input  type="date" 
                            name="web_start_date" 
                            value="{{ old('web_start_date') }}"
                            class="form-control form-control-sm {{ $errors->has('web_start_date') ? 'is-invalid' : '' }}">
                </div>
                <div class="col">
                    <label for="web_end_date">End Date:</label>
                    <input  type="date" 
                            name="web_end_date" 
                            value="{{ old('web_end_date') }}"
                            class="form-control form-control-sm {{ $errors->has('web_end_date') ? 'is-invalid' : '' }}">
                </div>
            </div>  
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
                 </div>
            <div class="form-row">
                <div class="col">
                    <label for="web_online_asset">Select Online Asset</label>
                    <select     name="web_online_asset" 
                                id="web_online_asset" 
                                onchange="ShowOnlineAsset();"
                                class='form-control form-control-sm {{ $errors->has('web_online_asset') ? 'is-invalid' : '' }}'>
                        <option value="">Select One</option>
                        @if (old('web_online_asset') == "Columbia Valley Pioneer")
                            <option value="Columbia Valley Pioneer" class="" selected>Columbia Valley Pioneer</option>
                        @else
                            <option value="Columbia Valley Pioneer" class="">Columbia Valley Pioneer</option>
                        @endif
                        @if (old('web_online_asset') == "Fitzhugh")
                            <option value="Fitzhugh" class="" selected>Fitzhugh</option>    
                        @else
                            <option value="Fitzhugh" class="">Fitzhugh</option>
                        @endif
                        @if (old('web_online_asset') == "Kamloops This Week")
                            <option value="Kamloops This Week" class="" selected>Kamloops This Week</option>
                        @else
                            <option value="Kamloops This Week" class="">Kamloops This Week</option>
                        @endif
                        @if (old('web_online_asset') == "Merritt Herald")
                            <option value="Merritt Herald" class="" selected>Merritt Herald</option>
                        @else
                            <option value="Merritt Herald" class="">Merritt Herald</option>   
                        @endif
                        @if (old('web_online_asset') == "Peachland View")
                            <option value="Peachland View" class="" selected>Peachland View</option>
                        @else
                            <option value="Peachland View" class="">Peachland View</option>  
                        @endif
                        @if (old('web_online_asset') == "Times Chronicle")
                            <option value="Times Chronicle" class="" selected>Times Chronicle</option>
                        @else
                            <option value="Times Chronicle" class="">Times Chronicle</option>
                        @endif
                    </select>
                </div>
            </div>  
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <!-- based on what is selected above show available webad slots -->
            @if (old('web_online_asset') == "Columbia Valley Pioneer")
                <div class="form-row" id="cvp" style="display:flex;">    
            @else
                <div class="form-row" id="cvp" style="display:none;">
            @endif 
                <div class="col">
                <label>Columbia Valley Pioneer Ad Network</label>
                <div class="form-check">
                    @if (old('cvp_ad_space_1') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_1" 
                                    id="cvp_ad_space_1" 
                                    onchange="FillWebAdSpace('cvp_ad_space_1');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">   
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_1" 
                                    id="cvp_ad_space_1" 
                                    onchange="FillWebAdSpace('cvp_ad_space_1');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">
                    @endif
                        <label class="form-check-label" for="cvp_ad_space_1">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard HP</label>
                    </div>
                    <div class="form-check">
                        @if (old('cvp_ad_space_2') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_2" 
                                    id="cvp_ad_space_2" 
                                    onchange="FillWebAdSpace('cvp_ad_space_2');"
                                    checked
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">         
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_2" 
                                    id="cvp_ad_space_2" 
                                    onchange="FillWebAdSpace('cvp_ad_space_2');"
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">
                        @endif   
                        <label class="form-check-label" for="cvp_ad_space_2"> [1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S1</label>
                    </div>
                    <div class="form-check">
                        @if (old('cvp_ad_space_3') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_3" 
                                    id="cvp_ad_space_3" 
                                    onchange="FillWebAdSpace('cvp_ad_space_3');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_3" 
                                    id="cvp_ad_space_3" 
                                    onchange="FillWebAdSpace('cvp_ad_space_3');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">
                        @endif
                        <label class="form-check-label" for="cvp_ad_space_3">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S2</label>
                    </div>
                    <div class="form-check">
                        @if (old('cvp_ad_space_4') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_4"
                                    id="cvp_ad_space_4"  
                                    onchange="FillWebAdSpace('cvp_ad_space_4');"
                                    checked
                                    value="[300x600] - SkyScraper">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_4"
                                    id="cvp_ad_space_4"  
                                    onchange="FillWebAdSpace('cvp_ad_space_4');"
                                    value="[300x600] - SkyScraper">
                        @endif
                        <label class="form-check-label" for="cvp_ad_space_4">[300x600] - SkyScraper</label>
                    </div>
                    <div class="form-check">
                        @if (old('cvp_ad_space_5') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_5" 
                                    id="cvp_ad_space_5" 
                                    onchange="FillWebAdSpace('cvp_ad_space_5');"
                                    checked
                                    value="[300x250] Square - Sidebar 1">    
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_5" 
                                    id="cvp_ad_space_5" 
                                    onchange="FillWebAdSpace('cvp_ad_space_5');"
                                    value="[300x250] Square - Sidebar 1">
                        @endif
                        <label class="form-check-label" for="cvp_ad_space_5">[300x250] Square - Sidebar 1</label>
                    </div>
                    <div class="form-check">
                        @if (old('cvp_ad_space_6') != "")
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="cvp_ad_space_6" 
                                id="cvp_ad_space_6" 
                                onchange="FillWebAdSpace('cvp_ad_space_6');"
                                checked
                                value="[300x250] Square - Sidebar 2">    
                        @else
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="cvp_ad_space_6" 
                                id="cvp_ad_space_6" 
                                onchange="FillWebAdSpace('cvp_ad_space_6');"
                                value="[300x250] Square - Sidebar 2">                          
                        @endif
                        <label class="form-check-label" for="cvp_ad_space_6">[300x250] Square - Sidebar 2</label>
                    </div>
                    <div class="form-check">
                        @if (old('cvp_ad_space_7') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_7" 
                                    id="cvp_ad_space_7" 
                                    onchange="FillWebAdSpace('cvp_ad_space_7');"
                                    checked
                                    value="[300x250] Square - Inline Article">       
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="cvp_ad_space_7" 
                                    id="cvp_ad_space_7" 
                                    onchange="FillWebAdSpace('cvp_ad_space_7');"
                                    value="[300x250] Square - Inline Article">
                        @endif
                        <label class="form-check-label" for="cvp_ad_space_7">[300x250] Square - Inline Article</label>
                    </div>
                </div>
            </div>      
            @if (old('web_online_asset') == "Fitzhugh")
                <div class="form-row" id="fh" style="display:flex;">    
            @else
                <div class="form-row" id="fh" style="display:none;">
            @endif 
                <div class="col">
                <label>Fitzhugh Ad Network</label>
                <div class="form-check">
                    @if (old('fh_ad_space_1') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_1" 
                                    id="fh_ad_space_1" 
                                    onchange="FillWebAdSpace('fh_ad_space_1');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">   
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_1" 
                                    id="fh_ad_space_1" 
                                    onchange="FillWebAdSpace('fh_ad_space_1');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">
                    @endif
                        <label class="form-check-label" for="fh_ad_space_1">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard HP</label>
                    </div>
                    <div class="form-check">
                        @if (old('fh_ad_space_2') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_2" 
                                    id="fh_ad_space_2" 
                                    onchange="FillWebAdSpace('fh_ad_space_2');"
                                    checked
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">         
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_2" 
                                    id="fh_ad_space_2" 
                                    onchange="FillWebAdSpace('fh_ad_space_2');"
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">
                        @endif   
                        <label class="form-check-label" for="fh_ad_space_2"> [1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S1</label>
                    </div>
                    <div class="form-check">
                        @if (old('fh_ad_space_3') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_3" 
                                    id="fh_ad_space_3" 
                                    onchange="FillWebAdSpace('fh_ad_space_3');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_3" 
                                    id="fh_ad_space_3" 
                                    onchange="FillWebAdSpace('fh_ad_space_3');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">
                        @endif
                        <label class="form-check-label" for="fh_ad_space_3">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S2</label>
                    </div>
                    <div class="form-check">
                        @if (old('fh_ad_space_4') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_4"
                                    id="fh_ad_space_4"  
                                    onchange="FillWebAdSpace('fh_ad_space_4');"
                                    checked
                                    value="[300x600] - SkyScraper">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_4"
                                    id="fh_ad_space_4"  
                                    onchange="FillWebAdSpace('fh_ad_space_4');"
                                    value="[300x600] - SkyScraper">
                        @endif
                        <label class="form-check-label" for="fh_ad_space_4">[300x600] - SkyScraper</label>
                    </div>
                    <div class="form-check">
                        @if (old('fh_ad_space_5') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_5" 
                                    id="fh_ad_space_5" 
                                    onchange="FillWebAdSpace('fh_ad_space_5');"
                                    checked
                                    value="[300x250] Square - Sidebar 1">    
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_5" 
                                    id="fh_ad_space_5" 
                                    onchange="FillWebAdSpace('fh_ad_space_5');"
                                    value="[300x250] Square - Sidebar 1">
                        @endif
                        <label class="form-check-label" for="fh_ad_space_5">[300x250] Square - Sidebar 1</label>
                    </div>
                    <div class="form-check">
                        @if (old('fh_ad_space_6') != "")
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="fh_ad_space_6" 
                                id="fh_ad_space_6" 
                                onchange="FillWebAdSpace('fh_ad_space_6');"
                                checked
                                value="[300x250] Square - Sidebar 2">    
                        @else
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="fh_ad_space_6" 
                                id="fh_ad_space_6" 
                                onchange="FillWebAdSpace('fh_ad_space_6');"
                                value="[300x250] Square - Sidebar 2">                          
                        @endif
                        <label class="form-check-label" for="fh_ad_space_6">[300x250] Square - Sidebar 2</label>
                    </div>
                    <div class="form-check">
                        @if (old('fh_ad_space_7') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_7" 
                                    id="fh_ad_space_7" 
                                    onchange="FillWebAdSpace('fh_ad_space_7');"
                                    checked
                                    value="[300x250] Square - Inline Article">       
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="fh_ad_space_7" 
                                    id="fh_ad_space_7" 
                                    onchange="FillWebAdSpace('fh_ad_space_7');"
                                    value="[300x250] Square - Inline Article">
                        @endif
                        <label class="form-check-label" for="fh_ad_space_7">[300x250] Square - Inline Article</label>
                    </div>
                </div>
            </div>  
            @if (old('web_online_asset') == "Kamloops This Week")
                <div class="form-row" id="ktw" style="display:flex;">    
            @else
                <div class="form-row" id="ktw" style="display:none;">
            @endif 
                <div class="col">
                <label>Kamloops This Week Ad Network</label>
                <div class="form-check">
                    @if (old('ktw_ad_space_1') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_1" 
                                    id="ktw_ad_space_1" 
                                    onchange="FillWebAdSpace('ktw_ad_space_1');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">   
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_1" 
                                    id="ktw_ad_space_1" 
                                    onchange="FillWebAdSpace('ktw_ad_space_1');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">
                    @endif
                        <label class="form-check-label" for="ktw_ad_space_1">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard HP</label>
                    </div>
                    <div class="form-check">
                        @if (old('ktw_ad_space_2') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_2" 
                                    id="ktw_ad_space_2" 
                                    onchange="FillWebAdSpace('ktw_ad_space_2');"
                                    checked
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">         
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_2" 
                                    id="ktw_ad_space_2" 
                                    onchange="FillWebAdSpace('ktw_ad_space_2');"
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">
                        @endif   
                        <label class="form-check-label" for="ktw_ad_space_2"> [1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S1</label>
                    </div>
                    <div class="form-check">
                        @if (old('ktw_ad_space_3') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_3" 
                                    id="ktw_ad_space_3" 
                                    onchange="FillWebAdSpace('ktw_ad_space_3');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_3" 
                                    id="ktw_ad_space_3" 
                                    onchange="FillWebAdSpace('ktw_ad_space_3');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">
                        @endif
                        <label class="form-check-label" for="ktw_ad_space_3">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S2</label>
                    </div>
                    <div class="form-check">
                        @if (old('ktw_ad_space_4') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_4"
                                    id="ktw_ad_space_4"  
                                    onchange="FillWebAdSpace('ktw_ad_space_4');"
                                    checked
                                    value="[300x600] - SkyScraper">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_4"
                                    id="ktw_ad_space_4"  
                                    onchange="FillWebAdSpace('ktw_ad_space_4');"
                                    value="[300x600] - SkyScraper">
                        @endif
                        <label class="form-check-label" for="ktw_ad_space_4">[300x600] - SkyScraper</label>
                    </div>
                    <div class="form-check">
                        @if (old('ktw_ad_space_5') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_5" 
                                    id="ktw_ad_space_5" 
                                    onchange="FillWebAdSpace('ktw_ad_space_5');"
                                    checked
                                    value="[300x250] Square - Sidebar 1">    
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_5" 
                                    id="ktw_ad_space_5" 
                                    onchange="FillWebAdSpace('ktw_ad_space_5');"
                                    value="[300x250] Square - Sidebar 1">
                        @endif
                        <label class="form-check-label" for="ktw_ad_space_5">[300x250] Square - Sidebar 1</label>
                    </div>
                    <div class="form-check">
                        @if (old('ktw_ad_space_6') != "")
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="ktw_ad_space_6" 
                                id="ktw_ad_space_6" 
                                onchange="FillWebAdSpace('ktw_ad_space_6');"
                                checked
                                value="[300x250] Square - Sidebar 2">    
                        @else
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="ktw_ad_space_6" 
                                id="ktw_ad_space_6" 
                                onchange="FillWebAdSpace('ktw_ad_space_6');"
                                value="[300x250] Square - Sidebar 2">                          
                        @endif
                        <label class="form-check-label" for="ktw_ad_space_6">[300x250] Square - Sidebar 2</label>
                    </div>
                    <div class="form-check">
                        @if (old('ktw_ad_space_7') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_7" 
                                    id="ktw_ad_space_7" 
                                    onchange="FillWebAdSpace('ktw_ad_space_7');"
                                    checked
                                    value="[300x250] Square - Inline Article">       
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="ktw_ad_space_7" 
                                    id="ktw_ad_space_7" 
                                    onchange="FillWebAdSpace('ktw_ad_space_7');"
                                    value="[300x250] Square - Inline Article">
                        @endif
                        <label class="form-check-label" for="ktw_ad_space_7">[300x250] Square - Inline Article</label>
                    </div>
                </div>
            </div>  
            @if (old('web_online_asset') == "Merritt Herald")
                <div class="form-row" id="mh" style="display:flex;">    
            @else
                <div class="form-row" id="mh" style="display:none;">
            @endif 
                <div class="col">
                <label>Merritt Herald Ad Network</label>
                <div class="form-check">
                    @if (old('mh_ad_space_1') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_1" 
                                    id="mh_ad_space_1" 
                                    onchange="FillWebAdSpace('mh_ad_space_1');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">   
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_1" 
                                    id="mh_ad_space_1" 
                                    onchange="FillWebAdSpace('mh_ad_space_1');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">
                    @endif
                        <label class="form-check-label" for="mh_ad_space_1">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard HP</label>
                    </div>
                    <div class="form-check">
                        @if (old('mh_ad_space_2') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_2" 
                                    id="mh_ad_space_2" 
                                    onchange="FillWebAdSpace('mh_ad_space_2');"
                                    checked
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">         
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_2" 
                                    id="mh_ad_space_2" 
                                    onchange="FillWebAdSpace('mh_ad_space_2');"
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">
                        @endif   
                        <label class="form-check-label" for="mh_ad_space_2"> [1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S1</label>
                    </div>
                    <div class="form-check">
                        @if (old('mh_ad_space_3') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_3" 
                                    id="mh_ad_space_3" 
                                    onchange="FillWebAdSpace('mh_ad_space_3');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_3" 
                                    id="mh_ad_space_3" 
                                    onchange="FillWebAdSpace('mh_ad_space_3');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">
                        @endif
                        <label class="form-check-label" for="mh_ad_space_3">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S2</label>
                    </div>
                    <div class="form-check">
                        @if (old('mh_ad_space_4') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_4"
                                    id="mh_ad_space_4"  
                                    onchange="FillWebAdSpace('mh_ad_space_4');"
                                    checked
                                    value="[300x600] - SkyScraper">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_4"
                                    id="mh_ad_space_4"  
                                    onchange="FillWebAdSpace('mh_ad_space_4');"
                                    value="[300x600] - SkyScraper">
                        @endif
                        <label class="form-check-label" for="mh_ad_space_4">[300x600] - SkyScraper</label>
                    </div>
                    <div class="form-check">
                        @if (old('mh_ad_space_5') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_5" 
                                    id="mh_ad_space_5" 
                                    onchange="FillWebAdSpace('mh_ad_space_5');"
                                    checked
                                    value="[300x250] Square - Sidebar 1">    
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_5" 
                                    id="mh_ad_space_5" 
                                    onchange="FillWebAdSpace('mh_ad_space_5');"
                                    value="[300x250] Square - Sidebar 1">
                        @endif
                        <label class="form-check-label" for="mh_ad_space_5">[300x250] Square - Sidebar 1</label>
                    </div>
                    <div class="form-check">
                        @if (old('mh_ad_space_6') != "")
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="mh_ad_space_6" 
                                id="mh_ad_space_6" 
                                onchange="FillWebAdSpace('mh_ad_space_6');"
                                checked
                                value="[300x250] Square - Sidebar 2">    
                        @else
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="mh_ad_space_6" 
                                id="mh_ad_space_6" 
                                onchange="FillWebAdSpace('mh_ad_space_6');"
                                value="[300x250] Square - Sidebar 2">                          
                        @endif
                        <label class="form-check-label" for="mh_ad_space_6">[300x250] Square - Sidebar 2</label>
                    </div>
                    <div class="form-check">
                        @if (old('mh_ad_space_7') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_7" 
                                    id="mh_ad_space_7" 
                                    onchange="FillWebAdSpace('mh_ad_space_7');"
                                    checked
                                    value="[300x250] Square - Inline Article">       
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="mh_ad_space_7" 
                                    id="mh_ad_space_7" 
                                    onchange="FillWebAdSpace('mh_ad_space_7');"
                                    value="[300x250] Square - Inline Article">
                        @endif
                        <label class="form-check-label" for="mh_ad_space_7">[300x250] Square - Inline Article</label>
                    </div>
                </div>
            </div>    
            @if (old('web_online_asset') == "Peachland View")
                <div class="form-row" id="plv" style="display:flex;">    
            @else
                <div class="form-row" id="plv" style="display:none;">
            @endif 
                <div class="col">
                <label>Peachland View Ad Network</label>
                <div class="form-check">
                    @if (old('plv_ad_space_1') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_1" 
                                    id="plv_ad_space_1" 
                                    onchange="FillWebAdSpace('plv_ad_space_1');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">   
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_1" 
                                    id="plv_ad_space_1" 
                                    onchange="FillWebAdSpace('plv_ad_space_1');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard HP">
                    @endif
                        <label class="form-check-label" for="plv_ad_space_1">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard HP</label>
                    </div>
                    <div class="form-check">
                        @if (old('plv_ad_space_2') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_2" 
                                    id="plv_ad_space_2" 
                                    onchange="FillWebAdSpace('plv_ad_space_2');"
                                    checked
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">         
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_2" 
                                    id="plv_ad_space_2" 
                                    onchange="FillWebAdSpace('plv_ad_space_2');"
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">
                        @endif   
                        <label class="form-check-label" for="plv_ad_space_2"> [1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S1</label>
                    </div>
                    <div class="form-check">
                        @if (old('plv_ad_space_3') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_3" 
                                    id="plv_ad_space_3" 
                                    onchange="FillWebAdSpace('plv_ad_space_3');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_3" 
                                    id="plv_ad_space_3" 
                                    onchange="FillWebAdSpace('plv_ad_space_3');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S2">
                        @endif
                        <label class="form-check-label" for="plv_ad_space_3">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S2</label>
                    </div>
                    <div class="form-check">
                        @if (old('plv_ad_space_4') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_4"
                                    id="plv_ad_space_4"  
                                    onchange="FillWebAdSpace('plv_ad_space_4');"
                                    checked
                                    value="[300x600] - SkyScraper">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_4"
                                    id="plv_ad_space_4"  
                                    onchange="FillWebAdSpace('plv_ad_space_4');"
                                    value="[300x600] - SkyScraper">
                        @endif
                        <label class="form-check-label" for="plv_ad_space_4">[300x600] - SkyScraper</label>
                    </div>
                    <div class="form-check">
                        @if (old('plv_ad_space_5') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_5" 
                                    id="plv_ad_space_5" 
                                    onchange="FillWebAdSpace('plv_ad_space_5');"
                                    checked
                                    value="[300x250] Square - Sidebar 1">    
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_5" 
                                    id="plv_ad_space_5" 
                                    onchange="FillWebAdSpace('plv_ad_space_5');"
                                    value="[300x250] Square - Sidebar 1">
                        @endif
                        <label class="form-check-label" for="plv_ad_space_5">[300x250] Square - Sidebar 1</label>
                    </div>
                    <div class="form-check">
                        @if (old('plv_ad_space_6') != "")
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="plv_ad_space_6" 
                                id="plv_ad_space_6" 
                                onchange="FillWebAdSpace('plv_ad_space_6');"
                                checked
                                value="[300x250] Square - Sidebar 2">    
                        @else
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="plv_ad_space_6" 
                                id="plv_ad_space_6" 
                                onchange="FillWebAdSpace('plv_ad_space_6');"
                                value="[300x250] Square - Sidebar 2">                          
                        @endif
                        <label class="form-check-label" for="plv_ad_space_6">[300x250] Square - Sidebar 2</label>
                    </div>
                    <div class="form-check">
                        @if (old('plv_ad_space_7') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_7" 
                                    id="plv_ad_space_7" 
                                    onchange="FillWebAdSpace('plv_ad_space_7');"
                                    checked
                                    value="[300x250] Square - Inline Article">       
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="plv_ad_space_7" 
                                    id="plv_ad_space_7" 
                                    onchange="FillWebAdSpace('plv_ad_space_7');"
                                    value="[300x250] Square - Inline Article">
                        @endif
                        <label class="form-check-label" for="plv_ad_space_7">[300x250] Square - Inline Article</label>
                    </div>
                </div>
            </div>  
            @if (old('web_online_asset') == "Times Chronicle")
                <div class="form-row" id="tc" style="display:flex;">    
            @else
                <div class="form-row" id="tc" style="display:none;">
            @endif 
                <div class="col">
                   <label>Times Chronicle Ad Network</label>
                   <div class="form-check">
                       @if (old('tc_ad_space_1') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_1" 
                                    id="tc_ad_space_1" 
                                    onchange="FillWebAdSpace('tc_ad_space_1');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">   
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_1" 
                                    id="tc_ad_space_1" 
                                    onchange="FillWebAdSpace('tc_ad_space_1');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S1">
                       @endif
                        <label class="form-check-label" for="tc_ad_space_1">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S1 (1st - Header)</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_2') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_2" 
                                    id="tc_ad_space_2" 
                                    onchange="FillWebAdSpace('tc_ad_space_2');"
                                    checked
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard Inside">         
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_2" 
                                    id="tc_ad_space_2" 
                                    onchange="FillWebAdSpace('tc_ad_space_2');"
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard Inside">
                        @endif   
                        <label class="form-check-label" for="tc_ad_space_2"> [1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard Inside (2nd)</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_3') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_3" 
                                    id="tc_ad_space_3" 
                                    onchange="FillWebAdSpace('tc_ad_space_3');"
                                    checked
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard Home S2">         
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_3" 
                                    id="tc_ad_space_3" 
                                    onchange="FillWebAdSpace('tc_ad_space_3');"
                                    value=" [1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard Home S2">
                        @endif   
                        <label class="form-check-label" for="tc_ad_space_3"> [1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard Home S2 (3rd)</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_4') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_4" 
                                    id="tc_ad_space_4" 
                                    onchange="FillWebAdSpace('tc_ad_space_4');"
                                    checked
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S3">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_4" 
                                    id="tc_ad_space_4" 
                                    onchange="FillWebAdSpace('tc_ad_space_4');"
                                    value="[1280x200 | 1170x150 | 728x90 | 468x60 | 320x50] - Leaderboard S3">
                        @endif
                        <label class="form-check-label" for="tc_ad_space_4">[1280x200, 1170x150, 728x90, 468x60, 320x50] - Leaderboard S3 (4th - Footer)</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_5') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_5"
                                    id="tc_ad_space_5"  
                                    onchange="FillWebAdSpace('tc_ad_space_5');"
                                    checked
                                    value="[300x250] - Sqaure S1">        
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_5"
                                    id="tc_ad_space_5"  
                                    onchange="FillWebAdSpace('tc_ad_space_5');"
                                    value="[300x250] - Square S1">
                        @endif
                        <label class="form-check-label" for="tc_ad_space_5">[300x250] - Square S1</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_6') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_6" 
                                    id="tc_ad_space_6" 
                                    onchange="FillWebAdSpace('tc_ad_space_6');"
                                    checked
                                    value="[300x250] Square - S2">    
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_6" 
                                    id="tc_ad_space_6" 
                                    onchange="FillWebAdSpace('tc_ad_space_6');"
                                    value="[300x250] Square - S2">
                        @endif
                        <label class="form-check-label" for="tc_ad_space_6">[300x250] - Square S2</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_7') != "")
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="tc_ad_space_7" 
                                id="tc_ad_space_7" 
                                onchange="FillWebAdSpace('tc_ad_space_7');"
                                checked
                                value="[300x250] Square - S3">    
                        @else
                        <input  class="form-check-input" 
                                type="checkbox" 
                                name="tc_ad_space_7" 
                                id="tc_ad_space_7" 
                                onchange="FillWebAdSpace('tc_ad_space_7');"
                                value="[300x250] Square - S3">                          
                        @endif
                        <label class="form-check-label" for="tc_ad_space_7">[300x250] Square - S3</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_8') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_8" 
                                    id="tc_ad_space_8" 
                                    onchange="FillWebAdSpace('tc_ad_space_8');"
                                    checked
                                    value="[300x600] - Skyscraper">       
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_8" 
                                    id="tc_ad_space_8" 
                                    onchange="FillWebAdSpace('tc_ad_space_8');"
                                    value="[300x600] - SkyScraper">
                        @endif
                        <label class="form-check-label" for="tc_ad_space_8">[300x600] Skyscraper</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_9') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_9" 
                                    id="tc_ad_space_9" 
                                    onchange="FillWebAdSpace('tc_ad_space_9');"
                                    checked
                                    value="[300x250] Square - S4">       
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_9" 
                                    id="tc_ad_space_9" 
                                    onchange="FillWebAdSpace('tc_ad_space_9');"
                                    value="[300x250] Square - S4">
                        @endif
                        <label class="form-check-label" for="tc_ad_space_9">[300x250] Square - S4</label>
                    </div>
                    <div class="form-check">
                        @if (old('tc_ad_space_10') != "")
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_10" 
                                    id="tc_ad_space_10" 
                                    onchange="FillWebAdSpace('tc_ad_space_10');"
                                    checked
                                    value="[300x250] Square - Inline Article">       
                        @else
                            <input  class="form-check-input" 
                                    type="checkbox" 
                                    name="tc_ad_space_10" 
                                    id="tc_ad_space_10" 
                                    onchange="FillWebAdSpace('tc_ad_space_10');"
                                    value="[300x250] Square - Inline Article">
                        @endif
                        <label class="form-check-label" for="tc_ad_space_10">[300x250] Square - Inline Article</label>
                    </div>
                </div>
            </div>  
            <div class="form-row">
                <div class="col">
                    <label for="web_ad_space">Web Ad Space:</label>
                    <input  type="text" 
                            name="web_ad_space" 
                            id="web_ad_space" 
                            value="{{ old('web_ad_space') }}" 
                            readonly
                            class="form-control form-control-sm {{ $errors->has('web_ad_space') ? 'is-invalid' : '' }}">
                </div>
            </div>  
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <div class="form-row">
                <div class="col">
                    <label for="rotation_percentage">Rotation Percentage / Impressions?</label>
                    <select     name="rotation_percentage" 
                                id="rotation_percentage" 
                                onchange="ShowImpressions();"
                                class="form-control form-control-sm {{ $errors->has('rotation_percentage') ? 'is-invalid' : '' }}">
                        <option value="">Select One</option>
                        @if (old('rotation_percentage') == "25%")
                            <option value="25%" selected>25%</option>    
                        @else
                            <option value="25%">25%</option>
                        @endif
                        @if (old('rotation_percentage') == "50%")
                            <option value="50%" selected>50%</option>    
                        @else
                            <option value="50%">50%</option>
                        @endif
                        @if (old('rotation_percentage') == "75%")
                            <option value="75%" selected>75%</option>    
                        @else
                            <option value="75%">75%</option>
                        @endif
                        @if (old('rotation_percentage') == "100%")
                            <option value="100%" selected>100%</option>    
                        @else
                            <option value="100%">100%</option>
                        @endif
                        @if (old('rotation_percentage') == "Impressions")
                            <option value="Impressions" selected>Impressions</option>    
                        @else
                            <option value="Impressions">Impressions</option>
                        @endif
                    </select>
                </div>
            </div>  
            @if (old('rotation_percentage') == "Impressions")
                <div class="form-row" id="impressions" style="display:flex">    
            @else
                <div class="form-row" id="impressions" style="display:none">    
            @endif
                <div class="col">
                    <label for="impressions">Impressions:</label>
                    <input  type="text" 
                            name="impressions" 
                            value="{{ old('impressions') }}" 
                            class="form-control form-control-sm {{ $errors->has('impressions') ? 'is-invalid' : '' }}">
                </div>
            </div>   
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <label for="notes">Other Notes / Special Instructions:</label>
                    <textarea name="notes" rows="5"  class="form-control" >{{ old('notes') }}</textarea>
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <div class="form-row">
                <div class="col">
                    <label for="subtotal">Subtotal:</label>
                    <input  type="text" 
                            name="subtotal" 
                            id="subtotal"
                            value="{{ old('subtotal') }}" 
                            onchange="CalcSubtotal();"
                            class="form-control form-control-sm {{ $errors->has('subtotal') ? 'is-invalid' : '' }}">
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <div class="form-row">
                <div class="col-2">
                    <label for="gst">GST:</label>
                    <input  type="text" 
                            name="gst" 
                            id="gst"
                            value="{{ old('gst') }}" 
                            readonly
                            class="form-control form-control-sm {{ $errors->has('gst') ? 'is-invalid' : '' }}">
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            <div class="form-row">
                <div class="col-2">
                    <label for="total">Total:</label>
                    <input  type="text" 
                            name="total" 
                            id="total"
                            value="{{ old('total') }}" 
                            readonly
                            class="form-control form-control-sm {{ $errors->has('total') ? 'is-invalid' : '' }}">
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
            
            <div class="form-row">
                <div class="col">
                    <label for="web_ad">Web Ads Upload</label>
                    <input  type="file" 
                            name="web_ad[]" 
                            id="web_ad"
                            class="form-control {{ $errors->has('web_ad') ? 'is-invalid' : '' }}" 
                            multiple="multiple"
                            value="{{ old('web_ad') }}">
                </div>
            </div>
            <div class="form-row">
                <div class="preview"></div>
            </div>
            <div class="form-row">
            <!--spacer--> <p>&nbsp;</p>
             </div>
            <div class="form-row">
                <button type="submit" class="btn btn-primary">Submit IO</button>
            </div>
        </form>
    </div>
   <p>&nbsp;</p><p>&nbsp;</p>
   <p>&nbsp;</p><p>&nbsp;</p>
@stop

@section('js')

<script type="text/javascript">
     $(function() {
        var imgPrev = function(input, imgPlaceholder) {

            if (input.files) {
                var allFiles = input.files.length;

                for (i = 0; i < allFiles; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPlaceholder);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('#web_ad').on('change', function() {
            imgPrev(this, 'div.preview');
            console.log ('image prev')
        });
    });    
    
    
    function ShowOnlineAsset() {
        //var change_property = document.getElementById(field_id_to_change);
        //var incoming_value = document.getElementById(property_value);
        var online_asset = document.getElementById('web_online_asset').value;

        document.getElementById('tc').style.display = "none";
        document.getElementById('plv').style.display = "none";
        document.getElementById('cvp').style.display = "none";
        document.getElementById('fh').style.display = "none";
        document.getElementById('ktw').style.display = "none";
        document.getElementById('mh').style.display = "none";
        if (online_asset == "Times Chronicle") {
            document.getElementById('tc').style.display = "flex";
          
        } else if(online_asset == "Peachland View") {   
          
            document.getElementById('plv').style.display = "flex";
          
        } else if(online_asset == "Columbia Valley Pioneer") {   
          
          document.getElementById('cvp').style.display = "flex";
        
        } else if(online_asset == "Fitzhugh") {   
          
          document.getElementById('fh').style.display = "flex";
        
        } else if(online_asset == "Kamloops This Week") {   
          
          document.getElementById('ktw').style.display = "flex";
        
        } else if(online_asset == "Merritt Herald") {   
          
          document.getElementById('mh').style.display = "flex";
        
        } else {
            // do nothing
        }
        //change_property.style.display = incoming_value.checked == true ? "flex" : "none";
    // console.log(incoming_value.checked);
    }
    function FillWebAdSpace(ad_space_id) {

       var ad_space = document.getElementById(ad_space_id).value;
       var web_ad_space = document.getElementById('web_ad_space');
       var remove_ad_space = web_ad_space.value;
       var comma_plus_ad_space = ', ' + ad_space;
       var ad_space_plus_comma = ad_space + ", ";

       if (document.getElementById(ad_space_id).checked == false) {
            if (web_ad_space.value.search(",") != -1) {
                
                web_ad_space.value = web_ad_space.value.replace(comma_plus_ad_space,"");
                web_ad_space.value = web_ad_space.value.replace(ad_space_plus_comma,"");
                console.log('i removed adspace');
                console.log(ad_space);
                            
                ad_space = "";
                
            } else {
                web_ad_space.value = "";
            }

       } else { // im adding 
            if (web_ad_space.value != "") {
                    web_ad_space.value = web_ad_space.value + ', ' + ad_space;
            } else {
                    web_ad_space.value = ad_space;
            }
       }
       

    }
    
    function ShowImpressions() {
        var online_percentage = document.getElementById('rotation_percentage').value
        document.getElementById('impressions').style.display = "none";
        if (online_percentage == "Impressions") {
            document.getElementById('impressions').style.display = "flex";
        }
    }
    function CalcSubtotal(){
        var subtotal = document.getElementById('subtotal').value;
        var gst = document.getElementById('gst').value = parseFloat(subtotal) * .05;
        
        document.getElementById('gst').value = gst;
        document.getElementById('total').value = parseFloat(subtotal) + gst;

        console.log(subtotal);

    }

    function UpdateRepNumber() {
        // sales_rep contains: "userid, repnumber"
        var sales_rep = document.getElementById('sales_rep').value;
        var values = sales_rep.split(',');
        var user_id = values[0];
        var rep_number = values[1];

        document.getElementById('rep_number').value = rep_number.trim();
        document.getElementById('sales_id').value = user_id.trim();
    }
  
</script>
    <script> console.log('Hi!'); </script>
   
@stop