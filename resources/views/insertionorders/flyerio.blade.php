@extends('layouts.dashboard')

@section('content')

@section('title', 'Create Flyer IO :: Dashboard')
    <div >
        <h1>Create Flyer IO</h1>    
        <p>&nbsp;</p>
    </div> 
    
    <div class="container">
        
        <form action="/insertionorders" method="POST">
            @csrf
            <input type="hidden" name="client_id" value="{{ $client->id }}">
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <input type="hidden" name="sales_id" id="sales_id" value="{{ old('sales_id') }}">
            <input type="hidden" name="order_type" value="Flyer">
            <input type="hidden" name="insertion_date" value="{{ now() }}">
            <div class="form-row">
                <div class="col md-6">
                    <label>Client Information:</label>
                    <p>{{ $client->client_name }}<br>
                    {{ $client->address }}<br>
                    {{ $client->city }}, {{ $client->province }}<br>
                    {{ $client->postal_code }}</p>
                    <label>Billing Contact:</label>
                    <p>{{ $client->billing_contact_name }}<br> 
                        Email: {{ $client->billing_contact_email }}<br>
                        Tel: {{ $client->billing_contact_phone }}
                    </p>
                    <p><a href="/clients/{{ $client->id }}/edit" class="text-primary">Edit</a></p>
                </div>
                <div class="col md-6">
                    <label>Order Information:</label>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">Sales Rep:</span>
                            </div>
                           <select name="sales_rep" id="sales_rep" class='form-control form-control-sm' onchange="UpdateRepNumber()">
                            @foreach ($sales_reps as $rep)
                                @if (old('sales_rep') == "")
                                    @if ($rep->id == $user->id)
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                    @else
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                    @endif
                                @else
                                    @if ($rep->id == old('sales_rep'))
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                    @else
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                    @endif  

                                @endif
                                
                            @endforeach  
                            
                           </select>
                    </div>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Rep Number:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="rep_number"
                                id="rep_number"
                                readonly 
                                value="{{ old('rep_number') == "" ? $user->rep_number : old('rep_number') }}">
                    </div>
                    
                    <p>Email: {{ $user->email }}</p>
                    <label>Account / DTI ID:</label>
                    {{ $client->dti_id }}<br>
                    <label for="po_number">PO Number</label>
                    <input type="text" class="form-control form-control-sm" name="po_number" value="{{ old('po_number') }}">
                </div>
            </div>
            <div class="form-row">
                <div class="col md-6"><!-- First Column of form -->
                    <label>Flyer Details:</label><br>
                        <div class="col">
                            <label for="flyer_dollars_per_thousand">Dollars per Thousand</label>
                            <input  type="text"
                                    name="flyer_dollars_per_thousand"
                                    id="flyer_dollars_per_thousand"
                                    value="{{ old('flyer_dollars_per_thousand') }}"
                                    onchange="CalcSubtotal();"
                                    class="form-control form-control-sm {{ $errors->has('flyer_dollars_per_thousand') ? 'is-invalid' : '' }}">
                            <br>
                            <label for="flyer_quanity">Flyer Quantity</label>
                            <input  type="text"
                                    name="flyer_quanity"
                                    id="flyer_quanity"
                                    value="{{ old('flyer_quanity') }}"
                                    onchange="CalcSubtotal();"
                                    class="form-control form-control-sm {{ $errors->has('flyer_quanity') ? 'is-invalid' : '' }}">        
                            <br>
                            <label for="flyer_discount">Discount:</label>
                            <select     name="flyer_discount" 
                                        id="flyer_discount"
                                        class="form-control form-control-sm"
                                        onchange="CalcSubtotal();">
                                    <option value=""></option>
                                    @if (old('flyer_discount') == ".05")
                                        <option value=".05" selected>5%</option>
                                    @else
                                        <option value=".05">5%</option>
                                    @endif
                                    @if (old('flyer_discount') == ".10")
                                        <option value=".10" selected>10%</option>
                                    @else
                                        <option value=".10">10%</option>
                                    @endif
                                    @if (old('flyer_discount') == ".15")
                                        <option value=".15" selected>15%</option>
                                    @else
                                        <option value=".15">15%</option>
                                    @endif
                                    @if (old('flyer_discount') == ".20")
                                    <option value=".20">20%</option>
                                    @else

                                    @endif
                                    @if (old('flyer_discount') == ".25")
                                    <option value=".25">25%</option>
                                    @else

                                    @endif
                            </select>
                        </div>
                        <br>
                        <div class="col bg-dark rounded p-2">
                            @if (old('override_subtotal_yes_or_no') == "Y" )
                                <div class="form-row" id="show_ors" style="display:flex;">
                            @else
                                <div class="form-row" id="show_ors" style="display:none;">
                            @endif
                            <div class="col-md-7">
                                    <label for="subtotal_overide">Override Subtotal</label>
                                    <input  type="text"
                                            name="subtotal_overide"
                                            id="override_subtotal"
                                            value="{{ old('subtotal_overide') }}"
                                            class="form-control form-control-sm {{ $errors->has('subtotal_overide') ? 'is-invalid' : '' }}"
                                            onchange="CalcSubtotal();">
                                </div>
                                <div class="col-md-5">
                                <label>Discount:</label>&nbsp;
                                <span id="span_discount" class="text-success fw-bold">{{ old('discount') }}</span>
                                <input type="hidden" name="discount" id="input_discount" value="{{ old('discount') }}" class="form-control form-control-sm">
                                
                                </div>
                            </div>
                            <div class="form-row">
                                    <div calss="col-md-12">
                                        <label for="override_subtotal_yes_or_no">Override Subtotal?</label>
                                        @if (old('override_subtotal_yes_or_no') == "Y")
                                            <input  type="checkbox" 
                                                    class="form-check form-check-inline" 
                                                    name="override_subtotal_yes_or_no" 
                                                    id="override_subtotal_yes_or_no" 
                                                    value="Y" 
                                                    checked 
                                                    onchange="ShowHideDiv('show_ors', 'override_subtotal_yes_or_no');">

                                        @else
                                            <input  type="checkbox" 
                                                    class="form-check form-check-inline" 
                                                    name="override_subtotal_yes_or_no" 
                                                    id="override_subtotal_yes_or_no" 
                                                    value="Y" 
                                                    onchange="ShowHideDiv('show_ors', 'override_subtotal_yes_or_no');">
                                        @endif       
                                    </div>
                            </div>
                            
                            <div class="form-row ">
                                <label>Subtotal:</label>
                                <div class="col-md-3">
                                    <span id="span_sub_total" class="text-success fw-bold">{{ old('subtotal') }}</span>
                                    <input type="hidden" name="subtotal" id="sub_total" value="{{ old('subtotal') }}" class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                
                                
                                </div>
                            </div>
                            <div class="form-row">
                                <label>GST (5%):</label>
                                <div class="col-md-2">
                                    <span id="span_gst" class="text-success">{{ old('gst') }}</span>
                                    <input type="hidden" name="gst" id="gst" value="{{ old('gst') }}" class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <div class="form-row ">
                                <label>Total:</label>
                                <div class="col-md-2">
                                    <span id="span_total" class="text-success fw-bold fs-4">{{ old('total') }}</span>
                                    <input type="hidden" name="total" id="total" value="{{ old('total') }}" class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                           
                        </div>
                    
                </div>
                <div class="col md-6"><!-- -start of second column in form-->
                   <label>Additional Information:</label>
                    <div class="form-row">
                        <div class="col">
                            <label for="flyer_identification">Flyer Identification</label>
                            <input  type="text"
                                    name="flyer_identification"
                                    class="form-control form-control-sm {{ $errors->has('flyer_identification') ? 'is-invalid' : '' }}"
                                    value="{{ old('flyer_identification') }}">
                        </div>    
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col">
                            <label for="flyer_distribution">Flyer Distribution Instructions</label>
                            <textarea name='flyer_distribution' class="form-control">{{ old('flyer_distribution') }}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Method of Payment</label><br>
                            @if (old('payment_method')== "Invoice")
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method" checked value="Invoice">
                                    <label class="form-check-label" for="inlineRadio1">Invoice</label>
                                </div>
                            @else
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  value="Invoice">
                                    <label class="form-check-label" for="inlineRadio1">Invoice</label>
                                </div>    
                            @endif
                            @if (old('payment_method') == 'Mastercard')
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method" checked value="Mastercard">
                                    <label class="form-check-label" for="inlineRadio2">Mastercard</label>
                                </div> 
                            @else
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method" value="Mastercard">
                                    <label class="form-check-label" for="inlineRadio2">Mastercard</label>
                                </div>
                            @endif
                            @if (old('payment_method') == 'Visa')
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  checked value="Visa">
                                    <label class="form-check-label" for="inlineRadio3">Visa</label>
                                </div> 
                            @else
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  value="Visa">
                                    <label class="form-check-label" for="inlineRadio3">Visa</label>
                                </div> 
                            @endif
                            @if (old('payment_method') == 'American Express')
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  checked value="American Express">
                                    <label class="form-check-label" for="inlineRadio3">American Express</label>
                                </div> 
                            @else
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  value="American Express">
                                    <label class="form-check-label" for="inlineRadio3">American Express</label>
                                </div> 
                            @endif
                            @if (old('payment_method') == 'Cash')
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method" checked value="Cash">
                                    <label class="form-check-label" for="inlineRadio3">Cash</label>
                                </div> 
                            @else
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  value="Cash">
                                    <label class="form-check-label" for="inlineRadio3">Cash</label>
                                </div> 
                            @endif
                            @if (old('payment_method') == 'Debit')
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  checked value="Debit">
                                    <label class="form-check-label" for="inlineRadio3">Debit</label>
                                </div>  
                            @else
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  value="Debit">
                                    <label class="form-check-label" for="inlineRadio3">Debit</label>
                                </div>  
                            @endif
                            @if (old('payment_method') == 'Cheque')
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method" checked value="Cheque">
                                    <label class="form-check-label" for="inlineRadio3">Cheque</label>
                                </div> 
                            @else
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="payment_method"  value="Cheque">
                                    <label class="form-check-label" for="inlineRadio3">Cheque</label>
                                </div> 
                            @endif
                            @if ($errors->has('payment_method'))
                            <div class="text-danger">
                                {{ $errors->first('payment_method') }}
                              </div>
                            @endif
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>       
           <?php $i = 0 ?>
            @while ($i <= 12)
                @if ($i == 0)
                    <div class="form-row" id="add_row_{{ $i }}" >
                @else
                    @if (old('print_start_date_'.$i) != "")
                        <div class="form-row" id="add_row_{{ $i }}" style="display:flex">
                    @else
                        <div class="form-row" id="add_row_{{ $i }}" style="display:none">    
                    @endif
                @endif
                    <div class="col-md-3">
                        <label for="print_start_date_{{ $i }}">Start Date</label><br>
                        <input  type="date" 
                                name="print_start_date_{{ $i }}" 
                                id="print_start_date_{{ $i }}" 
                                value="{{ old('print_start_date_'.$i) }}" 
                                class="form-control form-control-sm {{ $errors->has('print_start_date_'.$i) ? 'is-invalid' : '' }}" 
                                onchange="insertion('{{ $i }}');">
                    </div>
                    <div class="col-md-3">
                        <label for="print_end_date_{{ $i }}">End Date</label><br>
                        <input  type="date" 
                                name="print_end_date_{{ $i }}" 
                                id="print_end_date_{{ $i }}" 
                                value="{{ old('print_end_date_'.$i) }}" 
                                class="form-control form-control-sm {{ $errors->has('print_end_date_'.$i) ? 'is-invalid' : '' }}" 
                                onchange="insertion('{{  $i }}');">
                    </div>
                    
                    <div class="col-md-3">
                        <label for="print_publication_{{ $i }}">Publication</label>
                        <select     name="print_publication_{{ $i }}" 
                                    class='form-control form-control-sm {{ $errors->has('print_publication_'.$i) ? 'is-invalid' : '' }}'>
                            <option value="">Select One</option>
                            @if (count($errors) > 0) <!-- Then User has hit submit and I need to retrieve submitted data instead-->
                                @if (old('print_publication_'.$i ) == "Columbia Valley Pioneer")
                                    <option value="Columbia Valley Pioneer" class="" selected>Columbia Valley Pioneer</option>
                                @else
                                    <option value="Columbia Valley Pioneer" class="">Columbia Valley Pioneer</option>
                                @endif
                                @if (old('print_publication_'.$i) == "Fitzhugh")
                                    <option value="Fitzhugh" class="" selected>Fitzhugh</option>    
                                @else
                                    <option value="Fitzhugh" class="">Fitzhugh</option>
                                @endif
                                @if (old('print_publication_'.$i) == "Kamloops This Week")
                                    <option value="Kamloops This Week" class="" selected>Kamloops This Week</option>
                                @else
                                    <option value="Kamloops This Week" class="">Kamloops This Week</option>
                                @endif
                                @if (old('print_publication_'.$i) == "Merritt Herald")
                                    <option value="Merritt Herald" class="" selected>Merritt Herald</option>
                                @else
                                    <option value="Merritt Herald" class="">Merritt Herald</option>   
                                @endif
                                @if (old('print_publication_'.$i) == "Peachland View")
                                    <option value="Peachland View" class="" selected>Peachland View</option>
                                @else
                                    <option value="Peachland View" class="">Peachland View</option>  
                                @endif
                                @if (old('print_publication_'.$i) == "Times Chronicle")
                                    <option value="Times Chronicle" class="" selected>Times Chronicle</option>
                                @else
                                    <option value="Times Chronicle" class="">Times Chronicle</option>
                                @endif
                            @else
                                @if ($user->publication == "Columbia Valley Pioneer")
                                    <option value="Columbia Valley Pioneer" class="" selected> Columbia Valley Pioneer</option>
                                @else
                                    <option value="Columbia Valley Pioneer" class="">Columbia Valley Pioneer</option>
                                @endif
                                @if ($user->publication == "Fitzhugh")
                                    <option value="Fitzhugh" class="" selected>Fitzhugh</option>    
                                @else
                                    <option value="Fitzhugh" class="">Fitzhugh</option>
                                @endif
                                @if ($user->publication == "Kamloops This Week")
                                    <option value="Kamloops This Week" class="" selected>Kamloops This Week</option>
                                @else
                                    <option value="Kamloops This Week" class="">Kamloops This Week</option>
                                @endif
                                @if ($user->publication == "Merritt Herald")
                                    <option value="Merritt Herald" class="" selected>Merritt Herald</option>
                                @else
                                    <option value="Merritt Herald" class="">Merritt Herald</option>   
                                @endif
                                @if ($user->publication == "Peachland View")
                                    <option value="Peachland View" class="" selected>Peachland View</option>
                                @else
                                    <option value="Peachland View" class="">Peachland View</option>  
                                @endif
                                @if ($user->publication == "Times Chronicle")
                                    <option value="Times Chronicle" class="" selected>Times Chronicle</option>
                                @else
                                    <option value="Times Chronicle" class="">Times Chronicle</option>
                                @endif
                            @endif
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="print_no_insertions_{{ $i }}">No. Insertions</label><br>
                        <input  type="text" 
                                name="print_no_insertions_{{ $i }}" 
                                id="print_no_insertions_{{ $i }}"  
                                value="{{ old('print_no_insertions_'.$i) }}"
                                class="form-control form-control-sm {{ $errors->has('print_no_insertions_'.$i) ? 'is-invalid' : '' }}">
                    </div>
                    <div class="col-md-1">
                        @if ($i == 0)
                            &nbsp;
                        @else
                            <a href="javascript:" class="text-danger v-bottom" onclick="remove_row(this,{{ $i }});">(X) Remove</a>
                        @endif
                    </div>
                    <div class="form-row" id="row_{{ $i }}" syle="display:flex">
                        <div class="col-md-12">
                            @if ($i == 12)
                                &nbsp;
                            @else
                                <a href="javascript:;" class="text-primary" onclick="show_row(this,{{ $i+1 }});">(+) Add Row</a>
                            @endif     
                        </div>
                    </div>
                    
                </div> <!-- closes add_row_$i -->
                <div class="form-row spacer"></div> 
                
                <?php $i++  ?>
            @endwhile
           <div class="form-row">
                <div class="col-md-12">
                    <label for="notes">Other Notes / Special Instructions:</label>
                    <textarea name="notes" rows="5"  class="form-control" >{{ old('notes') }}</textarea>
                </div>
           </div>
            <div class="form-row">
            <!--spacer--> <p>&nbsp;</p>
             </div>
            <div class="form-row">
                <button type="submit" class="btn btn-primary">Submit IO</button>
            </div>
        </form>
    </div>
   <p>&nbsp;</p><p>&nbsp;</p>
   <p>&nbsp;</p><p>&nbsp;</p>
@stop



@section('js')

<script type="text/javascript">
    function ShowHideDiv(field_id_to_change, property_value) {
        var change_property = document.getElementById(field_id_to_change);
        var incoming_value = document.getElementById(property_value);
        change_property.style.display = incoming_value.checked == true ? "flex" : "none";
    // console.log(incoming_value.checked);
    }
   
    function CalcSubtotal(){
        var flyer_dollars_per_thousand = document.getElementById('flyer_dollars_per_thousand');
        var flyer_quanity = document.getElementById('flyer_quanity');
        var spansubtotal = document.getElementById('span_sub_total');
        var inputsubtotal = document.getElementById('sub_total');
        var flyer_discount = document.getElementById('flyer_discount');
        var spangst = document.getElementById('span_gst');
        var inputgst = document.getElementById("gst");
        var spantotal = document.getElementById('span_total');
        var inputtotal = document.getElementById("total");
        var overridesubtotal = document.getElementById('override_subtotal'); 
        var spandiscount = document.getElementById('span_discount');
        var inputdiscount = document.getElementById('input_discount');
        
        var subtotal = (parseFloat(flyer_dollars_per_thousand.value) * (parseInt(flyer_quanity.value)/1000));
        if (!isNaN(parseFloat(flyer_discount.value))) {
            discount = subtotal * flyer_discount.value;
            subtotal = subtotal - discount;
        }
        var gst = 0;
        var total = 0;
        if (!isNaN(parseFloat(overridesubtotal.value))) {
            oldsubtotal = subtotal;
            spansubtotal.innerText = overridesubtotal.value;
            inputsubtotal.value = overridesubtotal.value; 
            subtotal = parseFloat(overridesubtotal.value);
            if (oldsubtotal > subtotal) {
                spandiscount.innerText = parseFloat(oldsubtotal) - parseFloat(subtotal);
                inputdiscount.value =  parseFloat(oldsubtotal) - parseFloat(subtotal);
            } else {
                spandiscount.innerText = 0;
                inputdiscount.value =  0;
            }

        }
        spansubtotal.innerText = subtotal;
        inputsubtotal.value = subtotal;
        gst = subtotal * 0.05;
        spangst.innerText = gst;
        inputgst.value = gst;
        total = subtotal+gst;
        spantotal.innerText = total;
        inputtotal.value = total;

        
        console.log(subtotal);

    }
    function insertion(number){
        var start_date = 'print_start_date_'+number;
        var end_date = 'print_end_date_'+number;
        var change_field = "print_no_insertions_"+number;
        var date1 = new Date (document.getElementById(start_date).value);
        var date2 = new Date (document.getElementById(end_date).value);
        var print_frequency_0 = parseInt(document.getElementById('print_frequency_0').value);
        var no_insertions = 0;

        if ((date1 != null) && (date2 != null) && (print_frequency_0 != null)) {
            // To calculate the time difference of two dates
            var Difference_In_Time = date2.getTime() - date1.getTime();
            // To calculate the no. of days between two dates
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            print_frequency_0 = print_frequency_0 * 7;
            no_insertions = Math.round(Difference_In_Days / print_frequency_0);

            document.getElementById(change_field).value = no_insertions;
        }

    }
   
    function show_row(placeholder, number) {
        $.ajax({
            url: $(placeholder).attr('rel'),
            type: "GET",
            success:function(){
               // alert("done");
                var row = number;
                var prev = parseInt(number) - 1;
                var make_visible = "add_row_" + row;
                var hide_prev_btn = "row_" + prev;
                console.log (make_visible);

                document.getElementById(make_visible).style.display = "flex";
                document.getElementById(hide_prev_btn).style.display = "none";
            },
            error:function (){
                alert("testing error");
            }
        });
        return false;
    }
    function remove_row(placeholder, number) {
        $.ajax({
            url: $(placeholder).attr('rel'),
            type: "GET",
            success:function(){
               // alert("done");
                var row = number;
                var prev = parseInt(number) - 1;
                var make_visible = "row_" + prev;
                var hide_prev_btn = "add_row_" + row;   
                var print_start_date_clear = "print_start_date_" + row;
                var print_end_date_clear = "print_end_date_" + row;
                var print_no_insertions_clear = "print_no_insertions_" + row;
                // clear out data if there is any if they are removing the row
                document.getElementById(print_start_date_clear).value = "";
                document.getElementById(print_end_date_clear).value = "";
                document.getElementById(print_no_insertions_clear).value = "";

                document.getElementById(make_visible).style.display = "flex";
                document.getElementById(hide_prev_btn).style.display = "none";
            },
            error:function (){
                alert("testing error");
            }
        });
        return false;
    }      

    function UpdateRepNumber() {
        // sales_rep contains: "userid, repnumber"
        var sales_rep = document.getElementById('sales_rep').value;
        var values = sales_rep.split(',');
        var user_id = values[0];
        var rep_number = values[1];

        document.getElementById('rep_number').value = rep_number.trim();
        document.getElementById('sales_id').value = user_id.trim();
    }
   
  
</script>
    <script> console.log('Hi!'); </script>
   
@stop