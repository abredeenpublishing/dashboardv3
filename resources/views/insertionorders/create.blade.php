@extends('layouts.dashboard')

@section('content')
@if($error != "none")
    <div class="alert alert-danger">
        {{ $error }}
    </div>
@endif
@section('title', 'Create Client :: Dashboard')
    <div >
        <h1>Create IO</h1>    
        <p>&nbsp;</p>
    </div> 

    <div class="container">
        
      
       
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="ad_type">Choose Ad Type:</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="ad_type" id="ad_type_1" value="Print">
                        <label class="form-check-label" for="ad_type">Print</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="ad_type" id="ad_type_2" value="Web">
                        <label class="form-check-label" for="ad_type">Web</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="ad_type" id="ad_type_3" value="Flyer">
                        <label class="form-check-label" for="ad_type">Flyer</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="ad_type" id="ad_type_4" value="Classified">
                        <label class="form-check-label" for="ad_type">Classified</label>
                      </div>
                </div>
            </div>
            <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for='client_search'>Find Client</label>
                        <input class="form-control" name="client_search" id="search" value="" placeholder="Search for Client">
                    </div>
            </div>
           <table class="table table-bordered table-hover">
                <thead>
                <tr>
                <th>ID</th>
                <th>Client Name</th>
                <th>City</th> 
                <th>DTI ID</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
           
             
              
            <!--<div class="form-row">
                <button type="submit" class="btn btn-primary">Submit IO</button>
            </div>-->
     
    </div>
   <p>&nbsp;</p><p>&nbsp;</p>
   <p>&nbsp;</p><p>&nbsp;</p>
@stop



@section('js')
    <script type="text/javascript">
    $('#search').on('keyup',function(){
        $value=$(this).val();
        $value2=$('input:radio[name="ad_type"]:checked').val();
        $.ajax({
            type : 'get',
            url : '{{ route('insertionorders.search') }}',
            data:{'search':$value, 'ad_type':$value2}, 
            success:function(data){
                $('tbody').html(data);
            }
        });
    });
    $('#ad_type_1').click(function(e) {
        $value=$('#search').val();
        $value2=$('input:radio[name="ad_type"]:checked').val();
        if ($value != "") {
            $.ajax({
                type : 'get',
                url : '{{ route('insertionorders.search') }}',
                data:{'search':$value, 'ad_type':$value2}, 
                success:function(data){
                    $('tbody').html(data);
                }
            });
        }
    });
    $('#ad_type_2').click(function(e) {
        $value=$('#search').val();
        $value2=$('input:radio[name="ad_type"]:checked').val();
        if ($value != "") {
            $.ajax({
                type : 'get',
                url : '{{ route('insertionorders.search') }}',
                data:{'search':$value, 'ad_type':$value2}, 
                success:function(data){
                    $('tbody').html(data);
                }
            });
        }
    });
    $('#ad_type_3').click(function(e) {
        $value=$('#search').val();
        $value2=$('input:radio[name="ad_type"]:checked').val();
        if ($value != "") {
            $.ajax({
                type : 'get',
                url : '{{ route('insertionorders.search') }}',
                data:{'search':$value, 'ad_type':$value2}, 
                success:function(data){
                    $('tbody').html(data);
                }
            });
        }
    });
    $('#ad_type_4').click(function(e) {
        $value=$('#search').val();
        $value2=$('input:radio[name="ad_type"]:checked').val();
        if ($value != "") {
            $.ajax({
                type : 'get',
                url : '{{ route('insertionorders.search') }}',
                data:{'search':$value, 'ad_type':$value2}, 
                success:function(data){
                    $('tbody').html(data);
                }
            });
        }
    });
    </script>
    <script type="text/javascript">
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    </script>
    <script> console.log('Hi!'); </script>
@stop