@extends('layouts.dashboard')

@section('content')
@section('title', 'Update Order Number :: Dashboard')
<div >
    <h1>Update Order Number</h1>    
    <p>&nbsp;</p>
</div> 

<div class="container">
    <div class="row">
        <div class="col">
                Insertion Order #: <strong>{{ $insertionorder->id }}</strong>
        </div>
        <div class="col">
                Client: <strong>{{ $insertionorder->client->client_name }}</strong>
        </div>
        <div class="col">
                DTI ID: 
                @if ($insertionorder->client->dti_id == "") 
                    Please update DTI ID below.
                @else
                    <strong>{{ $insertionorder->client->dti_id }}</strong>
                @endif
        </div>
    </div>
    <form action="/insertionorders/edit-order-number/{{ $insertionorder->id }}" method="POST">
        <div class="form-row">&nbsp;</div>
        <div class="form-row">
            @csrf
            @method("PUT")
            <input type="hidden" name="chk_dti_id" value="{{ $insertionorder->client->dti_id }}">
            <input type="hidden" name="client_id" value="{{ $insertionorder->client_id }}">
            <label for="order_number">Order Number:</label>
            <input type="text" name="order_number" class="form-control form-control-lg" value="" placeholder="Enter Order # here...">
        </div>
            @if ($insertionorder->client->dti_id == "") 
                <div class="form-orw">&nbsp;</div>
                <div class="form-row">
                    <label for="dti_id">DTI ID</label>
                    <input type="text" name="dti_id" class="form-control form-control-lg" value="" placeholder="Enter DTI ID here...">
                </div>
            
            @endif
            
       
        <div clss="form-row">&nbsp;</div>
        <div class="form-row">
            <button type="submit" class="btn btn-primary">Update</button> 
        </div>
    </form>



</div>
    




@stop

@section('js')
    <script> 

   console.log('Hi!'); </script>
  
@stop