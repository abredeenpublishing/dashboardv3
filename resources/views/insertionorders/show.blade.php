@extends('layouts.dashboard')

@section('content')
@section('title', 'View Insertion Order :: Dashboard')
<div >
    <h1>View Insertion Order</h1>    
    <p>&nbsp;</p>
</div> 
<?php $user_id = auth()->id(); ?>
<div class="container">
    @if ($insertionorder->cancel == true) 
        <p class="text-danger">This order has been canceled:<br>
            {{ $insertionorder->cancel_message }}
        </p>
    @endif
    <div class="row">
       <div class="col-6">
        <button onclick="window.history.back();" class="btn btn-secondary"><i class="fas fa-backward"></i> Back</button>
        
       </div>
       <div class="col-6">
            <div class="row">
                <div class="col-3">
                </div>
                @if (Gate::allows('front-only', auth()->user()))
                    @if ($insertionorder->cancel == true)
                        <div class="col-6">
                            <a href="{{ route('insertionorders.editordernumber', $insertionorder->id) }}">
                                <button class="btn btn-primary" disabled><i class="fas fa-plus-circle"></i> Update Order #</button>
                            </a>
                        </div>
                    @else
                        <div class="col-6">
                            <a href="{{ route('insertionorders.editordernumber', $insertionorder->id) }}">
                                <button class="btn btn-primary"><i class="fas fa-plus-circle"></i> Update Order #</button>
                            </a>
                        </div>
                    @endif
                @else
                    @if (($insertionorder->cancel == true) || ($insertionorder->order_number == ""))
                        <div class="col-3">
                            <a href="{{ route('insertionorders.edit', $insertionorder->id) }}">
                                <button class="btn btn-primary" disabled><i class="fas fa-edit"></i> Edit IO</button>
                            </a>
                        </div>
                    @else
                        @if($user_id == $insertionorder->user_id)
                            <div class="col-3">
                                <a href="{{ route('insertionorders.edit', $insertionorder->id) }}">
                                    <button class="btn btn-primary"><i class="fas fa-edit"></i> Edit IO</button>
                                </a>
                            </div>
                        @else
                            <div class="col-3">
                                <a href="{{ route('insertionorders.edit', $insertionorder->id) }}">
                                    <button class="btn btn-primary" disabled><i class="fas fa-edit"></i> Edit IO</button>
                                </a>
                            </div>
                        @endif
                    @endif
                <div class="col-3">
                    <a href="/insertionorders/{{ $insertionorder->id }}/edit?clone=y">
                        <button class="btn btn-warning"><i class="far fa-copy"></i> Clone IO</button>
                    </a>
                </div>
                @endif
                <div class="col-3">
                    
                        <button onclick="window.print();" class="btn btn-info"><i class="fas fa-print"></i> Print</button>
                </div>      
            </div> 
       </div>
   </div>

   <br>
    <div class="card">
        <div class="card-body">
            <div class="row bg-dark rounded pt-2">
                <div class="col-10">
                    <div class="card-title">
                       <h4>{{ Helper::getioType($insertionorder->order_type) }} IO #: {{ $insertionorder->id }} </h4>
                    </div>
                </div>
                <div class="col-2">
                    <p><small>Date Created: {{ Helper::cleanDate($insertionorder->insertion_date) }}</small></p>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">
                <div class="col-2 bg-secondary rounded">
                    <label class="m-0">Client Information:</label>
                </div>
                <div class="col-1"></div>
                <div class="col-2 bg-secondary rounded">
                    <label class="m-0">Order Information:</label>
                </div>
                <div class="col-1"></div>
                <div class="col-2 bg-secondary rounded">
                    <label class="m-0">Ad Specs:</label>
                </div>
                <div class="col-1"></div>
                <div class="col-2 bg-secondary rounded">
                    <label class="m-0">Order / Totals:</label>
                </div>
                
            </div>
            <div class="row">
                <div class="col-3 ">
                    <p><a href="{{ route('clients.show', $insertionorder->client->id) }}">{{ $insertionorder->client->client_name }}</a><br>
                    {{ $insertionorder->client->address }}<br>
                    {{ $insertionorder->client->city }}, {{ $insertionorder->client->province }}<br>
                    {{ $insertionorder->client->postal_code }}</p>
                    <label>Billing Contact:</label>
                    <p>{{ $insertionorder->client->billing_contact_name }}<br> 
                        Email: {{ $insertionorder->client->billing_contact_email }}<br>
                        Tel: {{ $insertionorder->client->billing_contact_phone }}
                    </p>
                    <p><a href="/clients/{{ $insertionorder->client->id }}/edit" class="text-primary">Edit</a></p>
                </div>
                <div class="col-3">
                   @if ((empty($insertionorder->sales_id)) || ($insertionorder->sales_id == "0" ))
                         <p> Sales Rep: {{ $insertionorder->user->name }}</p>
                         <p>Rep Number: {{ $insertionorder->user->rep_number }}</p> 
                   @else
                       {!! htmlspecialchars_decode(Helper::getSalesRep($insertionorder->sales_id)) !!}
                   @endif
                   
                    <p>Email:<br> <small>{{ $insertionorder->user->email }}</small></p>
                    <label>Account / DTI ID:</label>
                    {{ $insertionorder->client->dti_id }}<br>
                    <p>PO Number: {{ $insertionorder->po_number }}</p>
                    
                    @if ($insertionorder->order_number != "")
                    <div class="row">
                        <div class="col-8 bg-warning rounded">
                     <strong>Order Number:   {{ $insertionorder->order_number }}</strong>
                        </div>
                    </div>
                    @endif

                </div>
                <div class="col-3"><!--Production-->
                    @if ($insertionorder->order_type == "2") <!-- Web Ad info -->
                    <p>Start Date: {{ Helper::cleanDate($insertionorder->web_start_date) }}<br>
                    End Date: {{ Helper::cleanDate($insertionorder->web_end_date) }}</p>
                    <p>Web url: {{ $insertionorder->web_url }}</p>
                    <p>% Rotation: {{ $insertionorder->rotation_percentage }}</p>
                    <p>Impressions: {{ $insertionorder->impressions }}</p>
                    
                    @elseif ($insertionorder->order_type == "3") <!-- Flyer info -->
                    <p><strong>Flying Identification:</strong><br> {{ $insertionorder->flyer_identification }}</p> 
                    <p><strong>Distribution Instructions:</strong><br> {{ $insertionorder->flyer_distribution }}</p>

                    @elseif ($insertionorder->order_type == "4") <!-- Classified info -->
                        <p>Classified Word Count: {{ $insertionorder->classified_word_count }}</p>
                        <p>Payment Method: {{ $insertionorder->payment_method }}</p>
                        <p>How Many Weeks: {{ $insertionorder->classified_number_of_weeks }}</p>
                        <p>Start Date: {{ Helper::cleanDate($insertionorder->classified_start_date) }}</p>    
                        <p>End Date: {{ Helper::cleanDate($insertionorder->classified_end_date) }}</p>

                    @else
                    <p>Position Requested: {{ $insertionorder->position_requested }}</p>
                    <p>Feature: {{ $insertionorder->feature }}</p>
                    <p>Colour: {{ $insertionorder->color }}</p>
                    <p>Copy:
                        @if ($insertionorder->copy != "")
                        <li>{{ $insertionorder->copy }}</li>                               
                        @endif    
                        @if ($insertionorder->copy2 != "")
                        <li>{{ $insertionorder->copy2 }}</li>                               
                        @endif    
                        @if ($insertionorder->copy3 != "")
                        <li>{{ $insertionorder->copy3 }}</li>                               
                        @endif    
                        @if ($insertionorder->copy4 != "")
                        <li>{{ $insertionorder->copy4 }}</li>                               
                        @endif    
                        @if ($insertionorder->copy5 != "")
                        <li>{{ $insertionorder->copy5 }}</li>                               
                        @endif   
                    </p>
                    <p>
                        Ad Type: {{ $insertionorder->ad_type }}<br>
                        Freq.: {{ Helper::getFrequency($insertionorder->print_frequency_0) }}<br>
                        TFN: {{ $insertionorder->tfn }}
                    </p>
                    @endif
                </div>
                <div class="col-3"><!-- Ad Specs -->
                        @if ($insertionorder->order_type == "2")
                        <p>Online Asset: {{ $insertionorder->web_online_asset }}</p>
                        <p>Ad Space: {{ $insertionorder->web_ad_space }}</p>

                        @elseif ($insertionorder->order_type == "3") <!-- Flyer info -->
                        <p>Dollars Per Thousand: @money($insertionorder->flyer_dollars_per_thousand)</p>
                        <p>Quantity: {{ $insertionorder->flyer_quanity }}</p>
                        <p>Discount:  {{ $insertionorder->flyer_discount * 100 }}%  </p>
                        @elseif ($insertionorder->order_type == "4") <!-- Classified info -->
                        
                        <p>Extra Word Cost: {{ $insertionorder->classified_extra_word_cost }}</p>
                        @else
                            @if($insertionorder->modular_size != "") 
                                <p>Modular Size: <strong>{{ $insertionorder->modular_size }}</strong></p>
                            @else 
                                <p>Columns Wide: <strong>{{ $insertionorder->columns_wide }}</strong> 
                                x Inches High: <strong>{{ $insertionorder->inches_high }}</strong>
                                Column Inches: {{ $column_inches = $insertionorder->columns_wide * $insertionorder->inches_high }}<br>
                                Column Rate: ${{ $insertionorder->column_rate }}<br>
                                </p>
                            @endif
                        <p>
                            
                            Colour Charge: ${{ $insertionorder->colour_charge }}
                        </p>
                        <?php
                            $output = 0;
                            $discount = $insertionorder->print_discount; 
                            if ($discount != null) {
                                $subtotal = $insertionorder->subtotal;
                                // need to find inverse of the original pertage then divide the subtotal by the inverse to get the original subtotal. This is neseccesy to figure out the dollar amount of the total discount given. 
                                $inverse_amount = (100 - $discount)/100;
                                $old_total = $subtotal / $inverse_amount;
                                $discount_amount = $old_total - $subtotal;
                                $output = $discount_amount . " (". $discount ."% off)";
                            }
                        ?>
                        <p> Discount: ${{ $output }}</p>
                        @endif
                        <p>
                            Subtotal: ${{ $insertionorder->subtotal }}<br>
                            GST (5%): ${{ $insertionorder->gst }}
                        </p>
                        <div class="row">
                            <div class="col-10 bg-dark rounded p-3">
                                <h2>Total: @money($insertionorder->total)</h2>
                            </div>
                        </div>
                </div>
            </div>
            <div class="row">&nbsp;</div>
            @if ($insertionorder->order_type == "2") <!-- if Web Ad -->
                <div class="row">
                   
                        <label>Web Ads</label>
                         
                        @foreach (explode(', ', $insertionorder->web_ad) as $filename)
                           <div class="row"><p>&nbsp;</p></div>
                           <div class="row ad_space"> <li> <img src="{{ asset('/storage/images/'.$filename) }}"></li></div>
                           <div class="row"><p>&nbsp;</p></div>
                        @endforeach
                    
                </div>
            @elseif ($insertionorder->order_type == "4") <!-- Display classifed word ad --->
                <div class="row">
                    <label>Classified Word Ad:</label>
                    <p>{{ $insertionorder->classified_word_ad }}</p>
                </div>
            
            @else <!-- Display this for Print and Flyer IO's -->
                <div class="row border-top border-bottom pt-2 bg-light">
                    <div class="col"><label>START DATE</label></div>
                    <div class="col"><label>END DATE</label></div>
                    <div class="col"><label>PUBLICATION</label></div>
                    <div class="col"><label>No. OF INSERTIONS</label></div>
                </div>
            
                <?php $i = 0; ?>
                @while ($i <= 12)
                    <?php 
                    $start_date = 'print_start_date_' . $i;
                    $end_date = 'print_end_date_' . $i;
                    $publication = 'print_publication_' . $i;
                    $insertions = 'print_no_insertions_' .$i;
                    ?>
                    @if ($insertionorder->$start_date != "")
                    <div class="row pt-2 pb-2 border-bottom">
                        <div class="col">{{ Helper::cleanDate($insertionorder->$start_date) }}</div>
                        <div class="col">{{ Helper::cleanDate($insertionorder->$end_date) }}</div>
                        <div class="col">{{ $insertionorder->$publication }}</div>
                        <div class="col">{{ $insertionorder->$insertions }}</div>
                    </div>
                    @endif
                    <?php $i++ ?>    
                @endwhile
            @endif
            <div class="row"><p>&nbsp;</p></div>
            <div class="row">
                
                <label>Special Instructions: </label>
                <p>{{ $insertionorder->notes }}</p>
            </div>
        </div>
        
    </div> 
    @if ($insertionorder->cancel == true) 
        <p class="text-danger">This order has been canceled:<br>
            {{ $insertionorder->cancel_message }}
        </p>
    @else
        @if (Gate::allows('sales-only', auth()->user()))
            <div class="row">
                <a href="{{ route('insertionorders.cancel', $insertionorder->id) }}"><button class="btn btn-danger">Cancel IO</button></a>
            </div>
        @endif
    @endif
    
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>


    
@stop

@section('js')
    <script> 
 
   
   console.log('Hi!'); </script>
  
@stop