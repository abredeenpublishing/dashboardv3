@extends('layouts.dashboard')

@section('content')

@section('title', 'Create Classified IO :: Dashboard')
    <div >
        <h1>Create Classified IO</h1>    
        <p>&nbsp;</p>
    </div> 
    
    <div class="container">
        
        <form action="/insertionorders" method="POST">
            @csrf
            <input type="hidden" name="client_id" value="{{ $client->id }}">
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <input type="hidden" name="sales_id" id="sales_id" value="{{ old('sales_id') }}">
            <input type="hidden" name="order_type" value="Classified">
            <input type="hidden" name="insertion_date" value="{{ now() }}">
            <div class="form-row">
                <div class="col md-6">
                    <label>Client Information:</label>
                    <p>{{ $client->client_name }}<br>
                    {{ $client->address }}<br>
                    {{ $client->city }}, {{ $client->province }}<br>
                    {{ $client->postal_code }}</p>
                    <label>Billing Contact:</label>
                    <p>{{ $client->billing_contact_name }}<br> 
                        Email: {{ $client->billing_contact_email }}<br>
                        Tel: {{ $client->billing_contact_phone }}
                    </p>
                    <p><a href="/clients/{{ $client->id }}/edit" class="text-primary">Edit</a></p>
                </div>
                <div class="col md-6">
                    <label>Order Information:</label>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">Sales Rep:</span>
                            </div>
                           <select name="sales_rep" id="sales_rep" class='form-control form-control-sm' onchange="UpdateRepNumber()">
                            @foreach ($sales_reps as $rep)
                                @if (old('sales_rep') == "")
                                    @if ($rep->id == $user->id)
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                    @else
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                    @endif
                                @else
                                    @if ($rep->id == old('sales_rep'))
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}" selected>{{ $rep->name }}</option>
                                    @else
                                        <option value="{{ $rep->id . ", " . $rep->rep_number }}">{{ $rep->name }}</option>
                                    @endif  

                                @endif
                                
                            @endforeach  
                            
                           </select>
                    </div>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Rep Number:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="rep_number"
                                id="rep_number"
                                readonly 
                                value="{{ old('rep_number') == "" ? $user->rep_number : old('rep_number') }}">
                    </div>
                 
                    <p>Email: {{ $user->email }}</p>
                    <label>Account / DTI ID:</label>
                    {{ $client->dti_id }}<br>
                    <label for="po_number">PO Number</label>
                    <input type="text" class="form-control form-control-sm" name="po_number" value="{{ old('po_number') }}">
                </div>
            </div>
            <div class="form-row">
                <div class="col md-6"><!-- First Column of form -->
                    <label>Ad Specs:</label><br>
                        
                        <label for="classfied_category">Category</label>
                        <input  type="text"
                                name="classified_category"
                                value="{{ old('classified_category') }}"
                                placeholder="For Rent, For Sales, Services For Hire, etc..."
                                class="form-control form-control-sm {{ $errors->has('calssified_category') ? 'is-invalid' : '' }}">
                        <br>
                        <label for="classified_word_ad">Classified Word Ad</label>
                        <textarea   rows="5" 
                                    id="classified_word_ad"
                                    class="form-control {{ $errors->has('classified_word_ad') ? 'is-invalid' : '' }}"
                                    onchange="wordcount();"
                                    name="classified_word_ad">{{ old('classified_word_ad') }}</textarea>
                        
                        
                        
                        <br>
                        <label>Payment Method</label>
                        <select name="payment_method" class="form-control form-control-sm">
                            @if(old('payment_method')=="Prepaid")
                            <option value="Prepaid" selected>Prepaid</option>
                            @else
                                <option value="Prepaid">Prepaid</option>
                            @endif
                            @if(old('payment_method')=="Invoice")
                                <option value="Invoice" selected>Invoice</option>
                            @else
                                <option value="Invoice">Invoice</option>
                            @endif
                        </select>
                        <br>
                        <label for="classified_number_of_weeks">How Many Weeks?</label>
                        <select     name="classified_number_of_weeks" 
                                    id="classified_how_many_weeks"
                                    onchange="wordcount();"
                                    class="form-control form-control-sm {{ $errors->has('classified_number_of_weeks') ? 'is-invalid' : '' }}">
                            <option value=""></option>
                            @for ($i=1; $i <= 52; $i++)
                                @if (old('classified_number_of_weeks') == $i)
                                    <option value="{{ $i }}" selected>{{ $i }} Weeks</option>
                                @else
                                    <option value="{{ $i }}">{{ $i }} Weeks</option>
                                @endif
                            @endfor
                        </select>
                        
                        <br>
                        <div class="form-row">
                            <div class="col">
                                <label for="classified_start_date">Start Date</label>
                                <input  type="date"
                                        name="classified_start_date"
                                        value="{{ old('classified_start_date') }}" 
                                        class="form-control form-control-sm {{ $errors->has('classified_start_date') ? 'is-invalid' : '' }}">

                            </div>
                            <div class="col">
                                <label for="classified_end_date">End Date</label>
                                <input  type="date"
                                        name="classified_end_date"
                                        value="{{ old('classified_end_date') }}"
                                        class="form-control form-control-sm {{ $errors->has('classified_end_date') ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                        <div class="form-row">
                                    <!--spacer--> <p>&nbsp;</p>
                        </div>  
                        <div class="col-md-12 bg-dark rounded p-2">
                            @if (old('override_subtotal_yes_or_no') == "Y" )
                                <div class="form-row" id="show_ors" style="display:flex;">
                            @else
                                <div class="form-row" id="show_ors" style="display:none;">
                            @endif
                            <div class="col-md-7">
                                    <label for="subtotal_overide">Override Subtotal</label>
                                    <input  type="text"
                                            name="subtotal_overide"
                                            id="override_subtotal"
                                            value="{{ old('subtotal_overide') }}"
                                            class="form-control form-control-sm {{ $errors->has('subtotal_overide') ? 'is-invalid' : '' }}"
                                            onchange="wordcount();">
                                </div>
                                <div class="col-md-5">
                                <label>Discount:</label>&nbsp;
                                <span id="span_discount" class="text-success fw-bold">{{ old('discount') }}</span>
                                <input type="hidden" name="discount" id="input_discount" value="{{ old('discount') }}" class="form-control form-control-sm">
                                
                                </div>
                            </div>
                            <div class="form-row">
                                    <div calss="col-md-12">
                                        <label for="override_subtotal_yes_or_no">Override Subtotal?</label>
                                        @if (old('override_subtotal_yes_or_no') == "Y")
                                            <input  type="checkbox" 
                                                    class="form-check form-check-inline" 
                                                    name="override_subtotal_yes_or_no" 
                                                    id="override_subtotal_yes_or_no" 
                                                    value="Y" 
                                                    checked 
                                                    onchange="ShowHideDiv('show_ors', 'override_subtotal_yes_or_no');">

                                        @else
                                            <input  type="checkbox" 
                                                    class="form-check form-check-inline" 
                                                    name="override_subtotal_yes_or_no" 
                                                    id="override_subtotal_yes_or_no" 
                                                    value="Y" 
                                                    onchange="ShowHideDiv('show_ors', 'override_subtotal_yes_or_no');">
                                        @endif       
                                    </div>
                            </div>
                            <div class="form-row ">
                                <label>Extra Word Cost:</label>
                                <div class="col-md-3">
                                    <span   id="span_extra_word_ad_cost" 
                                            class="text-success fw-bold">{{ old('classified_extra_word_cost') }}
                                    </span>
                                    <input  type="hidden" 
                                            name="classified_extra_word_cost" 
                                            id="classified_extra_word_cost" 
                                            value="{{ old('classified_extra_word_cost') }}" 
                                            class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                
                                
                                </div>
                            </div>
                            <div class="form-row ">
                                <label>Subtotal:</label>
                                <div class="col-md-3">
                                    <span id="span_sub_total" class="text-success fw-bold">{{ old('subtotal') }}</span>
                                    <input type="hidden" name="subtotal" id="sub_total" value="{{ old('subtotal') }}" class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                
                                
                                </div>
                            </div>
                            <div class="form-row">
                                <label>GST (5%):</label>
                                <div class="col-md-2">
                                    <span id="span_gst" class="text-success">{{ old('gst') }}</span>
                                    <input type="hidden" name="gst" id="gst" value="{{ old('gst') }}" class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <div class="form-row ">
                                <label>Total:</label>
                                <div class="col-md-2">
                                    <span id="span_total" class="text-success fw-bold fs-4">{{ old('total') }}</span>
                                    <input type="hidden" name="total" id="total" value="{{ old('total') }}" class="form-control form-control-sm">
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                           
                        </div>
                </div>
                <?php
                    // set defualt values if there is nothing entered for the user
                    if ($user->classified_word_count == null) {
                        $user->classified_word_count = 25;
                    }
                    if ($user->classified_extra_word_cost == null) {
                        $user->classified_extra_word_cost = 0.1;
                    }
                    if ($user->classified_base == null) {
                        $user->classified_base = 10;
                    }
                ?>
                <div class="col md-6"><!-- -start of second column in form-->
                   <label>Classified Cost Controls:</label>
                    <div class="form-row">
                        <div class="col-md-4 pl-5">
                            <small>Classified Word Count:</small>
                            <input  type="text"
                                    name="ctrl_classified_word_count"
                                    id="ctrl_classified_word_count"
                                    value="{{ old('ctrl_classified_word_count', $user->classified_word_count)}}"
                                    class="form-control form-control-sm">
                            <small>This is the amount of words that included in the base cost of the ad</small>
                        </div>
                        <div class="col-md-4 pl-5">
                            <small>Extra Word Cost:</small>
                            <input  type="text"
                                    name="ctrl_classified_extra_word_cost"
                                    id="ctrl_classified_extra_word_cost"
                                    value="{{ old('ctrl_classified_extra_word_cost', $user->classified_extra_word_cost) }}"
                                    class="form-control form-control-sm">
                            <small>$ dollar amount to charge if ad goes above Classified Word Count.</small>
                        </div>
                        <div class="col-md-4 pl-5">
                            <small>Classified Base Cost:</small>
                            <input  type="text"
                                    name="ctrl_classified_base"
                                    id="ctrl_classified_base"
                                    value="{{ old('ctrl_classified_base', $user->classified_base) }}"
                                    class="form-control form-control-sm">
                            <small>$ dollar amount for 1 week for Classified Word Count.</small>
                        </div>
                    </div> 
                   <br><br>
                   <div class="form-row">
                        <div class="col">
                            <label>Word Count</label>
                            <input  type="text" 
                                    name="classified_word_count"
                                    id="classified_ad_count"
                                    readonly
                                    value="{{ old('classified_word_count') }}"
                                    class="form-control form-control-sm">
                        </div>
                    </div>
                    <br><br>
                   
                </div>
            </div>
            <div class="form-row">
                <!--spacer--> <p>&nbsp;</p>
            </div>
          
           <div class="form-row">
                <div class="col-md-12">
                    <label for="notes">Other Notes / Special Instructions:</label>
                    <textarea name="notes" rows="5"  class="form-control" >{{ old('notes') }}</textarea>
                </div>
           </div>
            <div class="form-row">
            <!--spacer--> <p>&nbsp;</p>
             </div>
            <div class="form-row">
                <button type="submit" class="btn btn-primary">Submit IO</button>
            </div>
        </form>
    </div>
   <p>&nbsp;</p><p>&nbsp;</p>
   <p>&nbsp;</p><p>&nbsp;</p>
@stop



@section('js')

<script type="text/javascript">
    function ShowHideDiv(field_id_to_change, property_value) {
        var change_property = document.getElementById(field_id_to_change);
        var incoming_value = document.getElementById(property_value);
        change_property.style.display = incoming_value.checked == true ? "flex" : "none";
    // console.log(incoming_value.checked);
    }
    
    function CalcSubtotal(){
        var spansubtotal = document.getElementById('span_sub_total');
        var inputsubtotal = document.getElementById('sub_total');
        var spangst = document.getElementById('span_gst');
        var inputgst = document.getElementById("gst");
        var spantotal = document.getElementById('span_total');
        var inputtotal = document.getElementById("total");
        var overridesubtotal = document.getElementById('override_subtotal'); 
        var spandiscount = document.getElementById('span_discount');
        var inputdiscount = document.getElementById('input_discount');
        var extra_word_ad_cost = document.getElementById('classified_extra_word_cost').value;
        var classified_how_many_weeks = document.getElementById('classified_how_many_weeks').value;
        var subtotal = parseFloat(document.getElementById('sub_total').value) + parseFloat(extra_word_ad_cost);

        if (classified_how_many_weeks != "") {
            subtotal = subtotal * parseInt(classified_how_many_weeks);
        }

        var gst = 0;
        var total = 0;
        if (!isNaN(parseFloat(overridesubtotal.value))) {
            oldsubtotal = subtotal;
            spansubtotal.innerText = overridesubtotal.value;
            inputsubtotal.value = overridesubtotal.value; 
            subtotal = parseFloat(overridesubtotal.value);
            if (oldsubtotal > subtotal) {
                spandiscount.innerText = parseFloat(oldsubtotal) - parseFloat(subtotal);
                inputdiscount.value =  parseFloat(oldsubtotal) - parseFloat(subtotal);
            } else {
                spandiscount.innerText = 0;
                inputdiscount.value =  0;
            }

        }
        spansubtotal.innerText = subtotal;
        inputsubtotal.value = subtotal;
        gst = subtotal * 0.05;
        spangst.innerText = gst;
        inputgst.value = gst;
        total = subtotal+gst;
        spantotal.innerText = total;
        inputtotal.value = total;

        
        console.log(subtotal);

    }
   
   
    function wordcount() {
        var classified_word_ad = document.getElementById('classified_word_ad').value;
        var count = classified_word_ad.split(/\s/).length;
        document.getElementById('classified_ad_count').value = count; // Word count
        var word_base = document.getElementById('ctrl_classified_word_count').value;
        var word_cost = document.getElementById('ctrl_classified_extra_word_cost').value;
        document.getElementById('sub_total').value = document.getElementById('ctrl_classified_base').value;
        var diff = count - word_base;
        if (diff > 0) {
            document.getElementById('span_extra_word_ad_cost').innerText = (diff * word_cost).toFixed(2);
            document.getElementById('classified_extra_word_cost').value = (diff * word_cost).toFixed(2);
        } else {                     
            document.getElementById('span_extra_word_ad_cost').innerText = 0;
            document.getElementById('classified_extra_word_cost').value = 0;
        }
        CalcSubtotal();

	}

    function UpdateRepNumber() {
        // sales_rep contains: "userid, repnumber"
        var sales_rep = document.getElementById('sales_rep').value;
        var values = sales_rep.split(',');
        var user_id = values[0];
        var rep_number = values[1];

        document.getElementById('rep_number').value = rep_number.trim();
        document.getElementById('sales_id').value = user_id.trim();
    }
  
    
  
</script>
    <script> console.log('Hi!'); </script>
   
@stop