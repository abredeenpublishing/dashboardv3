@extends('layouts.dashboard')

@section('content')
@section('title', 'View Client :: Dashboard')
<div >
    <h1>View Client</h1>    
    <p>&nbsp;</p>
</div> 

<div class="container">
    <div class="row">
        <div class="col">
        <h3>{{ $client->client_name }} </h3>
        <label>Account #: {{ $client->dti_id }}</label><br>
        {{ $client->address }}<br>
        {{ $client->city }}, 
        {{ $client->province }}<br>
        {{ $client->postal_code }}
            <br><br>

        </div>
        <div class="col">
            <h3>&nbsp;</h3>
            <label>Billing Contact Details:</label><br>
            {{ $client->billing_contact_name }}<br>
            {{ $client->billing_contact_phone }}<br>
            {{ $client->billing_contact_email }}<br>
            <label>Client Contact Details:</label><br>
            {{ $client->client_contact_name }}<br>
            {{ $client->client_contact_phone }}<br>
            {{ $client->client_contact_email }}<br>
        </div>
        <div class="col">
           <a href="{{ route('clients.edit', $client->id) }}" title="Edit Client"><button class="btn btn-primary"><i class="fas fa-edit"></i>Edit</button></a>
           @if (Gate::allows('sales-only', auth()->user()))
           <a href="{{ route('insertionorders.printio', $client->id) }}" title="Create Print IO"><button class="btn btn-info"><i class="fas fa-fw fa-plus-circle "></i> <strong>P</strong></button></a>
           <a href="{{ route('insertionorders.webio', $client->id) }}" title="Create Web IO"><button class="btn btn-info"><i class="fas fa-fw fa-plus-circle "></i> <strong>W</strong></button></a>
           <a href="{{ route('insertionorders.flyerio', $client->id) }}" title="Create Flyer IO"><button class="btn btn-info"><i class="fas fa-fw fa-plus-circle "></i> <strong>F</strong></button></a>
           <a href="{{ route('insertionorders.classifiedio', $client->id) }}"  title="Create Classified IO"><button class="btn btn-info"><i class="fas fa-fw fa-plus-circle "></i> <strong>C</strong></button></a>
           @endif
        </div>
    </div>
    <div class="row">
        <div class="col">
        <label>Insertion Orders: </label><br>    
        <table id="clients-table" class="table table-condensed dataTable no-footer" role="grid" aria-describedby="clients-table_info">
            <thead>
                <tr role="row">
                    <th>ID</th>
                    <th>Client Name</th>
                    <th>Order Number</th>
                    <th>IO Type</th>
                    <th>Total</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Action</th>
     
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
    
</div>
<div class="row">
    <div class="col">
        <br><br><br>
        <!--<button class="btn btn-danger">Delete Client</button>   This button is not functional-->
    </div>
</div>
</div>

    
@stop

@section('js')
<script> 
    jQuery(function($) {
       //initiate dataTables plugin
       var myTable = 
       $('#clients-table').DataTable( {
           processing: true,
           serverSide: true,
           bFilter: false,
           responsive: true,
           "order": [[ 0, "desc" ]],
           ajax: {
                url: '{{ route('clients.showinsertions', $client->id) }}',
                    data: function (d) {
                    d.client_name = $('input[name=client_name]').val();
                    d.dti_id = $('input[name=dti_id]').val();
                }
            },
            "createdRow": function( row, data, dataIndex){
                if( data['cancel'] ==  `1`){
                    $(row).addClass('cancel');
                }
           },
           columns: [
            {data: 'id', name: 'id',
                    fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if(oData.client_name) {
                        $(nTd).html("<a href='/insertionorders/"+oData.id+"'>"+oData.id+"</a>");
                    }
                }
            },
            {data: 'client_name', name: 'Client Name'},
            {data: 'order_number', name: 'Order Number'},
            {data: 'io_type', name: 'Orde Type', 
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if(oData.client_name) {
                        $(nTd).html("<a href='/insertionorders/"+oData.id+"'>"+oData.io_type+"</a>");
                    }
                }
            },
            {data: 'total',
                render: $.fn.dataTable.render.number( ',', '.', 2 )},
            {data: 'start_date', name: 'Start Date'},
            {data: 'end_date', name: 'End Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false, width: '17%'},
           ],
       
       
        });
    });
   
   console.log('Hi!'); </script>
  
@stop