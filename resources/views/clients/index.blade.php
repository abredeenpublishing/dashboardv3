@extends('layouts.dashboard')

@section('content')
@section('title', 'Client Database :: Dashboard')
@section('plugins.Datatables', true)
<div class="float-left">
    <h1>Client Database</h1>    
    <p>&nbsp;</p>
</div> 
<div class="float-right">
    <a href="/clients/create"><button type="button" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add Client</button></a>
</div>   
<div style="clear:both"></div>
<div class="card">
    <div class="card-body">
        <div class="card-title">
            Advance Searching
        </div>
        <form method="GET" id="search-form">
            <div class="row">
                <div class="col-lg-3">
                    <input type="text" class="form-control adv-search-input" placeholder="Client Name" name="client_name" value="{{ (isset($_GET['client_name']) && $_GET['client_name'] != '')?$_GET['client_name']:'' }}">
                </div>
                <div class="col-lg-3">
                    <input type="text" class="form-control adv-search-input" placeholder="DTI ID" name="dti_id" value="{{ (isset($_GET['dti_id']) && $_GET['dti_id'] != '')?$_GET['dti_id']:'' }}">
                </div>
                <div class="col-lg-12">
                    <hr>
                    <button class="btn btn-success" type="submit" id="extraSearch">Search</button>
                    <a class="btn btn-danger" href="/clients">Reset</a>
                </div>
            </div>
        </form>
    </div>
</div>
    
   <table id="clients-table" class="table table-condensed dataTable no-footer" role="grid" aria-describedby="clients-table_info">
       <thead>
           <tr role="row">
               <th>ID</th>
               <th>DTI ID</th>
               <th>Client Name</th>
               <th>City</th>
               <th>Action</th>

           </tr>
       </thead>
       <tbody>
           
       </tbody>
   </table>

   <p>&nbsp;</p><p>&nbsp;</p>
   <p>&nbsp;</p><p>&nbsp;</p>
@stop

@section('js')
    <script> 
    jQuery(function($) {
       //initiate dataTables plugin
       var myTable = 
       $('#clients-table').DataTable( {
           processing: true,
           serverSide: true,
           bFilter: false,
           responsive: true,
           ordering: false,
           ajax: {
                url: '{{ route('clients.getdata') }}',
                    data: function (d) {
                    d.client_name = $('input[name=client_name]').val();
                    d.dti_id = $('input[name=dti_id]').val();
                }
            },
           columns: [
           {data: 'id', name: 'id'},
           {data: 'dti_id', name: 'DTI ID'},
           {data: 'client_name', name: 'Client Name', width: '30%',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    if(oData.client_name) {
                        $(nTd).html("<a href='/clients/"+oData.id+"'>"+oData.client_name+"</a>");
                    }
                }
           },
           {data: 'city', name: 'City'},
           {data: 'action', name: 'action', orderable: false, searchable: false, width: '25%'},
           ],
       
       
        });
    });
   
   console.log('Hi!'); </script>
  
@stop