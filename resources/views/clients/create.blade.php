@extends('layouts.dashboard')

@section('content')
@section('title', 'Create Client :: Dashboard')
    <div >
        <h1>Add Client</h1>    
        <p>&nbsp;</p>
    </div> 

    <div class="container">
        <form action="/clients" method="POST">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="dti_id">DTI ID</label>
                    <input type="text" class="form-control" name="dti_id" value="{{ old('dti_id') }}" placeholder="DTI ID">
                    <small id="dti_idHelp" class="form-text text-muted">You can leave this blank if this is a brand new client</small>
                    @if ($errors->has('dti_id'))
                    <div class="text-danger">
                        {{ $errors->first('dti_id') }}
                      </div>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="client_name">Company/Client Name</label>
                    <input type="text" class="form-control" name="client_name" value="{{ old('client_name') }}" placeholder="Company/Client Name">
                    @if ($errors->has('client_name'))
                    <div class="text-danger">
                        {{ $errors->first('client_name') }}
                      </div>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Address">
                    @if ($errors->has('address'))
                    <div class="text-danger">
                        {{ $errors->first('address') }}
                      </div>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="city">City</label>
                    <input type="city" class="form-control" name="city" :error="form.errors.city" value="{{ old('city') }}" placeholder="City">
                    @if ($errors->has('city'))
                    <div class="text-danger">
                        {{ $errors->first('city') }}
                      </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="province">Province</label>
                    <input type="text" class="form-control" name="province" value="{{ old('province') }}" placeholder="Province">
                    @if ($errors->has('province'))
                    <div class="text-danger">
                        {{ $errors->first('province') }}
                      </div>
                    @endif
                </div>
                <div class="form-group col-md-2">
                    <label for="postal_code">Postal Code</label>
                    <input type="text" class="form-control" name="postal_code" value="{{ old('postal_code') }}" placeholder="Postal Code">
                    @if ($errors->has('city'))
                    <div class="text-danger">
                        {{ $errors->first('postal_code') }}
                      </div>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <label>Billing Contact Information:</label>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="billing_contact_name">Name</label>
                    <input type="text" class="form-control" name="billing_contact_name" value="{{ old('billing_contact_name') }}" placeholder="Billing Contact Name">
                    @if ($errors->has('billing_contact_name'))
                    <div class="text-danger">
                        {{ $errors->first('billing_contact_name') }}
                      </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="billing_contact_email">Email</label>
                    <input type="email" class="form-control" name="billing_contact_email" value="{{ old('billing_contact_email') }}" placeholder="Billing Contact Email">
                    @if ($errors->has('billing_contact_email'))
                    <div class="text-danger">
                        {{ $errors->first('billing_contact_email') }}
                      </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="billing_contact_phone">Phone</label>
                    <input type="text" class="form-control" name="billing_contact_phone" value="{{ old('billing_contact_phone') }}" placeholder="Billing Contact phone">
                    @if ($errors->has('billing_contact_phone'))
                    <div class="text-danger">
                        {{ $errors->first('billing_contact_phone') }}
                      </div>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <label>Client Contact Information:</label>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="client_contact_name">Name</label>
                    <input type="text" class="form-control" name="client_contact_name" value="{{ old('client_contact_name') }}" placeholder="Client Contact Name">
                    @if ($errors->has('client_contact_name'))
                    <div class="text-danger">
                        {{ $errors->first('client_contact_name') }}
                      </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="client_contact_email">Email</label>
                    <input type="email" class="form-control" name="client_contact_email" value="{{ old('client_contact_email') }}" placeholder="Client Contact Email">
                    @if ($errors->has('client_contact_email'))
                    <div class="text-danger">
                        {{ $errors->first('client_contact_email') }}
                      </div>
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label for="client_contact_phone">Phone</label>
                    <input type="text" class="form-control" name="client_contact_phone" value="{{ old('client_contact_phone') }}" placeholder="Client Contact phone">
                    @if ($errors->has('client_contact_phone'))
                    <div class="text-danger">
                        {{ $errors->first('client_contact_phone') }}
                      </div>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <button type="submit" class="btn btn-primary">Add Client</button>
            </div>
        </form>
    </div>
   <p>&nbsp;</p><p>&nbsp;</p>
   <p>&nbsp;</p><p>&nbsp;</p>
@stop



@section('js')
    <script> console.log('Hi!'); </script>
@stop