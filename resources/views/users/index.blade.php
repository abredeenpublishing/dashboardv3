@extends('layouts.dashboard')

@section('content')
@section('plugins.Datatables', true)
@section('title', 'Manage Users - Dashboard')
<div style="float:left;">
    <h1>Manage Users</h1>    
    <p>This is where you can manage users.</p>
</div> 
<div style="float:right;">
    <a href="/users/create"><button type="button" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Create User</button></a>
</div>   
<div style="clear:both"></div>
    
   <table id="users-table" class="table table-condensed dataTable no-footer" role="grid" aria-describedby="users-table_info">
       <thead>
           <tr role="row">
               <th>ID</th>
               <th>Name</th>
               <th>Email</th>
               <th>Updated Ad</th>
               <th>Action</th>

           </tr>
       </thead>
       <tbody>
           @foreach ($users as $user )
               <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->updated_at }}</td>
                    <td>
                        <div class="row">
                            <div class="col-xs-6 px-2">
                                <a href="/users/{{ $user->id }}/edit"><button class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</button></a>
                            </div>
                            <div class="col-xs-6">
                                <form action="/users/{{ $user->id  }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Delete</button>
                                </form>
                            </div>
                        </div>
                    </td>

               </tr>
           @endforeach
       </tbody>
   </table>

   
@stop


@section('js')
    <script> 
     jQuery(function($) {
        //initiate dataTables plugin
        var myTable = 
        $('#users-table')
        
        .DataTable( {
            columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'updated_at', name: 'updated_at'},
            {data: '', name: 'Action'}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            });
        }
            });
        });
    
    console.log('Hi!'); </script>
@stop