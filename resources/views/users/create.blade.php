@extends('layouts.dashboard')


@section('content') 
@section('title', 'Create User :: Dashboard')
<h1>Create User</h1>   
<div class="container"> 
       
        <form action="/users" method="POST">
            @csrf
            <div class="form-row">
                <div class="form-group md-col-6">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Full Name:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="name" 
                                value="{{ old('name') }}">
                    </div> 
                </div>
                <div class="form-group md-col-6">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Email:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="email" 
                                value="{{ old('email') }}">
                    </div> 
                </div>
                <div class="w-100"></div>
            </div>
            <div class="form-row">
                <div class="form-group md-col-6">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Password:</span>
                        </div>
                        <input  type="password" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="password" 
                                value="{{ old('password') }}">
                    </div> 
                </div>
                <div class="form-group md-col-6">
                    
                </div>
            </div>
            <div class="form-row">
                <hr>
            </div>
            <div class="form-row">
                <div class="form-group md-col-6">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Office Admin Email:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="office_admin_email" 
                                value="{{ old('office_admin_email') }}">
                    </div>     
                </div>
                <div class="form-group md-col-6">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Production Email:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="production_email" 
                                value="{{ old('production_email') }}">
                    </div> 
                </div>
            </div>
            <div class="form-row">
                <div class="form-group md-col-6">
                    <div class="input-group input-group-sm">
                        <select     name="publication" 
                        class='form-control form-control-sm {{ $errors->has('publicaiton') ? 'is-invalid' : '' }}'>
                <option value="">Select One</option>
              
                    @if (old('publication') == "Columbia Valley Pioneer")
                        <option value="Columbia Valley Pioneer" class="" selected>Columbia Valley Pioneer</option>
                    @else
                        <option value="Columbia Valley Pioneer" class="">Columbia Valley Pioneer</option>
                    @endif
                    @if (old('publication') == "Fitzhugh")
                        <option value="Fitzhugh" class="" selected>Fitzhugh</option>    
                    @else
                        <option value="Fitzhugh" class="">Fitzhugh</option>
                    @endif
                    @if (old('publication') == "Kamloops This Week")
                        <option value="Kamloops This Week" class="" selected>Kamloops This Week</option>
                    @else
                        <option value="Kamloops This Week" class="">Kamloops This Week</option>
                    @endif
                    @if (old('publication') == "Merritt Herald")
                        <option value="Merritt Herald" class="" selected>Merritt Herald</option>
                    @else
                        <option value="Merritt Herald" class="">Merritt Herald</option>   
                    @endif
                    @if (old('publication') == "Peachland View")
                        <option value="Peachland View" class="" selected>Peachland View</option>
                    @else
                        <option value="Peachland View" class="">Peachland View</option>  
                    @endif
                    @if (old('publication') == "Times Chronicle")
                        <option value="Times Chronicle" class="" selected>Times Chronicle</option>
                    @else
                        <option value="Times Chronicle" class="">Times Chronicle</option>
                    @endif
               
            </select>
                    </div> 
                </div>
                <div class="form-group md-col-6">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg">Rep Number:</span>
                        </div>
                            <input  type="text" 
                                    class="form-control" 
                                    aria-label="Large" 
                                    aria-describedby="inputGroup-sizing-sm" 
                                    name="rep_number" 
                                    value="{{ old('rep_number') }}">
                    </div> 
                </div>
            </div>
            <div class="form-row">
                <div class="form-group md-col-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Classified Word Count:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="classified_word_count" 
                                value="{{ old('classified_word_count') }}">
                    </div> 
                </div>
                <div class="form-group md-col-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Extra Word Cost:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="classified_extra_word_cost" 
                                value="{{ old('classified_extra_word_cost') }}">
                    </div> 
                </div>
                <div class="form-group md-col-4">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroup-sizing-lg">Classified Base Cost:</span>
                        </div>
                        <input  type="text" 
                                class="form-control" 
                                aria-label="Large" 
                                aria-describedby="inputGroup-sizing-sm" 
                                name="classified_base" 
                                value="{{ old('classified_base') }}">
                    </div> 
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3">
                    <label>Is Admin?:</label><br>
                    <div class="form-check form-check-inline">
                        @if (old('isAdmin') == true)
                            <input class="form-check-input" type="checkbox" name="isAdmin" checked value="1">
                        @else
                            <input class="form-check-input" type="checkbox" name="isAdmin" value="1">
                        @endif
                        <label class="form-check-label" for="inlineCheckbox1">Yes</label>
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Is Front End?:</label><br>
                    <div class="form-check form-check-inline">
                        @if (old('isFront') == true)
                            <input class="form-check-input" type="checkbox" name="isFront" checked value="1">
                        @else
                            <input class="form-check-input" type="checkbox" name="isFront" value="1">
                        @endif
                        <label class="form-check-label" for="inlineCheckbox1">Yes</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <br><br>
            </div>
            <div class="form-row">
                <div class="form-group md-col-6">
                    <button type="submit" class="btn btn-primary">Create User</button>
                </div>
            </div>
         </form>
         
    <div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop